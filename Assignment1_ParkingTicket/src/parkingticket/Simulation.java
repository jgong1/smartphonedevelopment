/*
 * JSPHDEV Assignment1 Part B
 * By Jian Gong 
 * AndrewID: jgong1
 */
package parkingticket;

public class Simulation {
	public static void main (String[] args) {
		ParkingMeter meter1 = new ParkingMeter(100);
		ParkingMeter meter2 = new ParkingMeter(100);
		ParkingMeter meter3 = new ParkingMeter(100);
		ParkingMeter meter4 = new ParkingMeter(100);
		
		ParkedCar car1 = new ParkedCar("Jeep", "Cherokee", "Black", "PIT1234", 60);
		ParkedCar car2 = new ParkedCar("VW", "CC", "White", "PIT5678", 100);
		ParkedCar car3 = new ParkedCar("Ford", "Focus", "Red", "PIT0987", 120);
		ParkedCar car4 = new ParkedCar("Benz", "C300", "Black", "PIT1357", 230);
		
		PoliceOfficer officer = new PoliceOfficer("Jackson", "CMU2345");
		
		ParkingTicket ticket1 = null;
		ParkingTicket ticket2 = null;
		ParkingTicket ticket3 = null;
		ParkingTicket ticket4 = null;
		
		System.out.println("Test Case 1");
		int overtime1 = officer.examineOvertime(car1, meter1);
		if (overtime1 == 1) {
			ticket1 = officer.issueTicket(car1, meter1);
		}
		if (ticket1 != null) {
			ticket1.reportCar();
			ticket1.reportFine();
			ticket1.reportPolice();
		}
		
		System.out.println("Test Case 2");
		int overtime2 = officer.examineOvertime(car2, meter2);
		if (overtime2 == 1) {
			ticket2 = officer.issueTicket(car2, meter2);
		}
		if (ticket2 != null) {
			ticket2.reportCar();
			ticket2.reportFine();
			ticket2.reportPolice();
		}
		
		System.out.println("Test Case 3");
		int overtime3 = officer.examineOvertime(car3, meter3);
		if (overtime3 == 1) {
			ticket3 = officer.issueTicket(car3, meter3);
		}
		if (ticket3 != null) {
			ticket3.reportCar();
			ticket3.reportFine();
			ticket3.reportPolice();
		}
		
		System.out.println("Test Case 4");
		int overtime4 = officer.examineOvertime(car4, meter4);
		if (overtime4 == 1) {
			ticket4 = officer.issueTicket(car4, meter4);
		}
		if (ticket4 != null) {
			ticket4.reportCar();
			ticket4.reportFine();
			ticket4.reportPolice();
		}
		
		
	}
}
