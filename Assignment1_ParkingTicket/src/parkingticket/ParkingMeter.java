/*
 * JSPHDEV Assignment1 Part B
 * By Jian Gong 
 * AndrewID: jgong1
 */
package parkingticket;

public class ParkingMeter {
	// ---- Fields ----
	private int minutesPurchased;
	
	// ---- Constructor ----
	public ParkingMeter (int minutes) {
		this.minutesPurchased = minutes;
	}
	
	// ---- Methods ----
	public int getMinutesPurchased () {
		return this.minutesPurchased;
	}
}
