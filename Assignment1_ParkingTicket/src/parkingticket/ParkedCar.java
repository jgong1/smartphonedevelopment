/*
 * JSPHDEV Assignment1 Part B
 * By Jian Gong 
 * AndrewID: jgong1
 */

package parkingticket;

public class ParkedCar {
	// ---- Fields ----
	private String make;
	private String model;
	private String color;
	private String licenseNumber;
	private int numberOfMinutes;
	
	// ---- Constructor ----
	public ParkedCar (String make, String model, String color, 
			String licenseNumber, int numberOfMinutes) {
		if (make == null || model == null || color == null || licenseNumber == null) {
			System.out.println("Error: Make, Model, Color, LicenseNumber cannot be null");
			return;
		}
		this.make = make;
		this.model = model;
		this.color = color;
		this.licenseNumber = licenseNumber;
		this.numberOfMinutes = numberOfMinutes;
	}
	
	// ---- Methods ----
	public String getMake () {
		return this.make;
	}
	
	public String getModel () {
		return this.model;
	}
	
	public String getColor () {
		return this.color;
	}
	
	public String getLicenseNumber() {
		return this.licenseNumber;
	}
	
	public int getNumberOfMinutes() {
		return this.numberOfMinutes;
	}
	
	public void setMake (String newMake) {
		if (newMake == null ) {
			System.out.println("Error: Make of the car cannot be null");
			return;
		}
		this.make = newMake;
	}
	
	public void setModel (String newModel) {
		if (newModel == null) {
			System.out.println("Error: Model of the car cannot be null");
			return;
		}
		this.model = newModel;
	}
	
	public void setColor (String newColor) {
		if (newColor == null) {
			System.out.println("Error: Color of the car cannot be null");
			return;
		}
		this.color = newColor;
	}
	
	public void setLicenseNumber (String newLicenseNumber) {
		if (newLicenseNumber == null) {
			System.out.println("Error: License number of the car cannot be null");
			return;
		}
		this.licenseNumber = newLicenseNumber;
	}
	
	public void setNumberOfMinutes (int newNumberOfMinutes) {		
		this.numberOfMinutes = newNumberOfMinutes;
	}
}
