/*
 * JSPHDEV Assignment1 Part B
 * By Jian Gong 
 * AndrewID: jgong1
 */
package parkingticket;

public class ParkingTicket {
	// ---- Fields ----
	private ParkedCar car;
	private ParkingMeter meter;
	private PoliceOfficer officer;
	
	// ---- Constructor ----
	public ParkingTicket (ParkedCar car, ParkingMeter meter, PoliceOfficer officer) {
		if (car == null || meter == null || officer == null) {
			System.out.println("Error: Car, Meter nor Officer cannot be null");
			return;
		}
		this.car = car;
		this.meter = meter;
		this.officer = officer;
	}
	
	// ---- Methods ----
	public void reportCar () {		
		System.out.println("------------------------");
		System.out.println("Report Illegally Parked Car");
		System.out.println("Make: " + this.car.getMake());
		System.out.println("Model: " + this.car.getModel());
		System.out.println("Color: " + this.car.getColor());
		System.out.println("License Number: " + this.car.getLicenseNumber());
//		System.out.println("------------------------");
	}
	
	public void reportFine () {	
		int minutesParked = this.car.getNumberOfMinutes();
		int minutesPurchased = this.meter.getMinutesPurchased();
		int minutesExceeded = minutesParked - minutesPurchased;
//		System.out.println(minutesExceeded);
		int fine = 0;
		if (minutesExceeded <= 60) {
			fine = 25;
		} else {
			int hours = (minutesExceeded - 60) / 60;
			if ((minutesExceeded - 60) % 60 == 0) {
				fine = 25 + 10 * hours;
			} else {
				fine = 25 + 10 * (hours + 1);
			}	
		}
//		System.out.println("------------------------");
		System.out.println();
		System.out.println("Report Fine");
		System.out.println("Minutes purchased: " + this.meter.getMinutesPurchased());
		System.out.println("Minutes parked: " + this.car.getNumberOfMinutes());
		System.out.println("Fine: $" + fine);
//		System.out.println("------------------------");
	}
	
	public void reportPolice () {
//		System.out.println("------------------------");
		System.out.println();
		System.out.println("Report Police Officer");
		System.out.println("Name: " + this.officer.getName());
		System.out.println("Badge: " + this.officer.getBadgeNumber());
		System.out.println("------------------------");
	}
	
}
