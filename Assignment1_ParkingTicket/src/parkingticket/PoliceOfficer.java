/*
 * JSPHDEV Assignment1 Part B
 * By Jian Gong 
 * AndrewID: jgong1
 */
package parkingticket;

public class PoliceOfficer {
	// ---- Fields ----
	private String name;
	private String badgeNumber;	
		
	// ---- Constructor ----
	public PoliceOfficer (String name, String badgeNumber) {
		this.name = name;
		this.badgeNumber = badgeNumber;
	}
	
	// ---- Methods ----
	public String getName() {
		return this.name;
	}
	
	public String getBadgeNumber() {
		return this.badgeNumber;
	}
	
	public int examineOvertime(ParkedCar car, ParkingMeter meter) {
		if (car == null || meter == null) {
			System.out.println("Error: Car nor meter cannot be null");
			return -1;
		}
		int minutesParked = car.getNumberOfMinutes();
		int minutesPurchased = meter.getMinutesPurchased();
		if (minutesParked > minutesPurchased) {
			return 1;
		} else if (minutesParked < minutesPurchased) {
			System.out.println("------------------------");
			System.out.println("Clear. This car still has " + 
					(meter.getMinutesPurchased()-car.getNumberOfMinutes()) 
					+ " minutes left.");
			System.out.println("Make: " + car.getMake());
			System.out.println("Model: " + car.getModel());
			System.out.println("Color: " + car.getColor());
			System.out.println("License Number: " + car.getLicenseNumber());
			System.out.println("------------------------");
			return 2;
		} else {
			System.out.println("------------------------");
			System.out.println("Notice: This is the last minute purchased for this car");
			System.out.println("Make: " + car.getMake());
			System.out.println("Model: " + car.getModel());
			System.out.println("Color: " + car.getColor());
			System.out.println("License Number: " + car.getLicenseNumber());
			System.out.println("------------------------");
			return 0;
		}
	}
	
	public ParkingTicket issueTicket (ParkedCar car, ParkingMeter meter) {
		if (car == null || meter == null) {
			System.out.println("Error: Car nor Meter cannot be null");
			return null;
		}
		ParkingTicket newTicket = new ParkingTicket(car, meter, this);
		return newTicket;
	}
}
