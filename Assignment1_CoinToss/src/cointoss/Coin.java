/*
 * JSPHDEV Assignment1 Part B
 * By Jian Gong 
 * AndrewID: jgong1
 */
package cointoss;

public class Coin {
	
	private String sideUp;
	
	public Coin () {
		double rand = Math.random();
		if (rand >= 0.5) {
			sideUp = "heads";
		} else {
			sideUp = "tails";
		}
	}
	
	public void toss () {
		double rand = Math.random();
		if (rand >= 0.5) {
			this.sideUp = "heads";
		} else {
			this.sideUp = "tails";
		}
	}
	
	public String getSideUp () {
		return sideUp;
	}
	
	
}
