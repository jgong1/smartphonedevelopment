/*
 * JSPHDEV Assignment1 Part B
 * By Jian Gong 
 * AndrewID: jgong1
 */
package cointoss;

public class Test {
	public static void main (String[] args) {
		 Simulation sim = new Simulation();
		 System.out.println("Test Case 1");
		 sim.simulation(0);
		 System.out.println("Test Case 2");
		 sim.simulation(-1);
		 System.out.println("Test Case 3");
		 sim.simulation(20);
		 System.out.println("Test Case 4");
		 sim.simulation(40);
	}
}
