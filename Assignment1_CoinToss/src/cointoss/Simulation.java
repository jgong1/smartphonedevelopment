/*
 * JSPHDEV Assignment1 Part B
 * By Jian Gong 
 * AndrewID: jgong1
 */
package cointoss;

public class Simulation {
	public void simulation (int numberOfTosses) {
		if (numberOfTosses < 0) {
			System.out.println("Error: number of tosses cannot be negative.");
			return;
		}
		Coin coin = new Coin();
		System.out.println("------------------------");
		System.out.println("New Coin created, and the side facing up is " + coin.getSideUp());
		System.out.println();
		int headCount = 0;
		int tailCount = 0;
		for (int i = 0; i < numberOfTosses; i ++) {
			coin.toss();
			System.out.println("In the " + (i + 1) + " time, the side facing up is " 
					+ coin.getSideUp());
			if (coin.getSideUp() == "heads") {
				headCount ++;
			} else {
				tailCount ++;
			}
		}
		System.out.println();
		System.out.println("After " + numberOfTosses + " times toss, the total times of head facing up is " + headCount 
				+ ", and times of tail facing up is " + tailCount);
		System.out.println("------------------------");
	}
}
