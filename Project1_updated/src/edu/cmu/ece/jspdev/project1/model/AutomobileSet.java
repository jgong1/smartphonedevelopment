package edu.cmu.ece.jspdev.project1.model;

import java.util.Iterator;
import java.util.LinkedHashMap;

public class AutomobileSet {
	private LinkedHashMap<String, Automobile> lhm;
	
	/*
	 * Constructor which initialize the LinkedHashMap
	 */
	public AutomobileSet () { 
		this.lhm = new LinkedHashMap<String, Automobile>();
	}
	
	/*
	 * Getter for LinkedHashMap
	 */
	public LinkedHashMap<String, Automobile> getLHM() {
		return this.lhm;
	}
	
	/*
	 * Add a Automobile to AutomobileSet
	 * @param: Automobile automobile -- automobile to be added
	 */
	public void addAutomobile(Automobile automobile) {
		if (automobile == null) {
			System.out.println("Error when adding automobile to LinkedHashMap, automobile cannot be null");
			return;
		}
		this.lhm.put(automobile.getName(), automobile);
	}
	
	
	/*
	 * Delete a automobile from LinkedHashMap by its name and return the deleted Automobile
	 * @param: String name -- name of the Automobile to be deleted.
	 * @return: Automobile -- the Automobile deleted
	 */
	public Automobile deleteAutomobile(String name) {		
		Automobile automobile = this.lhm.remove(name);
		return automobile;
	}
	
	/*
	 * Find a automobile from LinkedHashmMap by its name and return it
	 * @param: String name -- name of the Automobile
	 * @return: Automobile 
	 */
	public Automobile findAutomobile(String name) {
		Iterator<Automobile> iterator = this.lhm.values().iterator();
		while (iterator.hasNext()) {
			Automobile automobileTemp = iterator.next();
			if (automobileTemp.getName().equals(name)) {
				return automobileTemp;
			}
		}
		System.out.println("No Automobile with name: " + name + " in AutomobileSet");
		return null;
	}
	
	/*
	 * Update Automobile Set with particular Automobile name
	 * @param: String automobile name, Automobile automobile 
	 */
	public void updateAutomobileSet(String name, Automobile automobile) {		
		this.lhm.put(name, automobile);
	}
	
}
