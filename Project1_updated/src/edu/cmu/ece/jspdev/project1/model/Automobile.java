package edu.cmu.ece.jspdev.project1.model;

import java.io.Serializable;
import java.util.ArrayList;

import edu.cmu.ece.jspdev.project1.model.OptionSet.Option;

public class Automobile implements Serializable{
	private static final long serialVersionUID = 1L;
	private String name;
	private String make;
	private String model;	
	private float basePrice;
	private ArrayList<OptionSet> optionSets; 
	private ArrayList<Option> choice;
	
	/*
	 * Constructors
	 */
	public Automobile() { 
		optionSets = new ArrayList<OptionSet>();
		choice = new ArrayList<Option>();
	}
	
	public Automobile(String newName) {
		this.name = newName;
	}
	
	public Automobile(float newBasePrice) {
		this.basePrice = newBasePrice;
	}
	
	public Automobile(ArrayList<OptionSet> newOptionSets) {
		this.optionSets = newOptionSets;
	}
	
	public Automobile(String newName, int size) {
		this.name = newName;
		this.optionSets = new ArrayList<OptionSet>(size);
		for (int i = 0; i < size; i ++) {
			optionSets.add(new OptionSet());
		}
	}
	
	public Automobile(String newName, float newBasePrice, int size) {
		this.name = newName;
		this.basePrice = newBasePrice;
		this.optionSets = new ArrayList<OptionSet>(size);
		for (int i = 0; i < size; i ++) {
			optionSets.add(new OptionSet());
		}
	}
	
	public Automobile(String newName, float newBasePrice, ArrayList<OptionSet> newOptionSets) {
		this.name = newName;
		this.basePrice = newBasePrice;
		this.optionSets = newOptionSets;
	}
	
	/*
	 * Getters and setters
	 */
	public String getName() {
		return this.name;
	}
	
	public String getMake() {
		return this.make;
	}
	
	public String getModel() {
		return this.model;
	}
	
	public float getBasePrice() {
		return this.basePrice;
	}
	
	public ArrayList<OptionSet> getOptionSets() {
		return this.optionSets;
	}
	
	public OptionSet getOptionSet(int index) {
		return this.optionSets.get(index);
	}
	
	/*
	 * Return the choice of the specific OptionSet with name setName
	 * @param: String setName -- name of the OptionSet
	 * @return: String -- choice of that OptionSet
	 */
	public String getOptionChoice(String setName) {
		int setIndex = findOptionSet(setName);
		if (setIndex == -1) {
			System.out.println("Could not find OptionSet with name " + setName);
			return "";
		}
		OptionSet setTemp = this.optionSets.get(setIndex);
		return setTemp.getOptionChoice().getName();
	}
	
	/*
	 * Return the price of the specific OptionSet with name setName
	 * @param: String setName -- name of the OptionSet
	 * @returnL int -- price of the Option
	 */
	public int getOptionChoicePrice(String setName) {
		int setIndex = findOptionSet(setName);
		if (setIndex == -1) {
			System.out.println("Could not find OptionSet with name " + setName);
			return 0;
		}
		OptionSet setTemp = this.optionSets.get(setIndex);
		return Math.round(setTemp.getOptionChoice().getPrice());
	}
	
	public void setName(String newName) {
		this.name = newName;
	}
	
	public void setMake(String newMake) {
		this.make = newMake;
	}
	
	public void setModel(String newModel) {
		this.model = newModel;
	}
	
	public void setBasePrice(float newBasePrice) {
		this.basePrice = newBasePrice;
	}
	
	public void setOptionSets(int size) {
		this.optionSets = new ArrayList<OptionSet>(size);	
		for (int i = 0; i < size; i ++) {
			optionSets.add(new OptionSet());
		}
	}
	
	public void setOptionSets(ArrayList<OptionSet> newOptionSets) {
		this.optionSets = newOptionSets;
	}
	
	public void setOptionSet(int index, OptionSet newOptionSet) {
		this.optionSets.set(index, newOptionSet);		
	}
	
	public void setOptionSet(int index, String optionSetName, int size) {
		OptionSet optionSetTemp = new OptionSet(optionSetName, size);
		this.optionSets.set(index, optionSetTemp);			
	}
	
	public void setOption(int optionSetIndex, int optionIndex, String optionName, float optionPrice) {
		OptionSet optionSetTemp = this.getOptionSet(optionSetIndex);
		optionSetTemp.setOptionName(optionIndex, optionName);
		optionSetTemp.setOptionPrice(optionIndex, optionPrice);		
	}
	
	/*
	 * Add OptionSet to this Automobile
	 * @param: OptionSet optionSet -- OptionSet to be added
	 */
	public void addOptionSet(String optionSetName, int size) {
		OptionSet optionSet = new OptionSet(optionSetName, size);
		this.optionSets.add(optionSet);
	}
	
	/*
	 * Set the choice of a specific OptionSet with name setName
	 * @param: String setName -- name of OptionSet
	 * 			String optionName -- name of choice
	 */
	public void setOptionChoice(String setName, String optionName) {
		int setIndex = this.findOptionSet(setName);
		if (setIndex ==  -1) {
			System.out.println("Could not find OptionSet with name " + setName);
			return;
		}
		OptionSet optionSetTemp = this.optionSets.get(setIndex);
		optionSetTemp.setOptionChoice(optionName);
		this.choice.add(optionSetTemp.getOptionChoice());		
	}
	
	/*
	 * Get the total price of all choices
	 * @return: int -- total price
	 */
	public int getTotalPrice() {
		int totalPrice = 0;
		totalPrice += this.basePrice;
		for (int i = 0; i < this.choice.size(); i ++) {
			totalPrice += this.choice.get(i).getPrice();
		}
		return Math.round(totalPrice);
	}
	
	
	/*
	 * Find OptionSet with name
	 * @param: String name -- name of the OptionSet
	 * @return: int optionSetIndex -- the index of the found OptionSet 
	 * 									and -1 if not found
	 */
	public int findOptionSet(String name) {
		if (name == null) {
			System.out.println("Error: In order to find the OptionSet, name for OptionSet cannot be null!");
			return -1;
		}				
		for (int i = 0; i < this.optionSets.size(); i ++) {
			if (this.optionSets.get(i) == null) {
				continue;
			}
			String nameTemp = this.optionSets.get(i).getName();
			if (nameTemp.equals(name)) {
				return i;
			}
		}
		return -1;
	}
	
	/*
	 * Find Option with name
	 * @param: String optionSetName, String optionName 
	 * @return: int optionIndex
	 * 			-1 if not found
	 */
	public int findOption(String optionSetName, String optionName) {
		int optionSetIndex = this.findOptionSet(optionSetName);
		int optionIndex = this.optionSets.get(optionSetIndex).findOption(optionName);
		return optionIndex;
	}
	
	/*
	 * Delete an Option by index
	 * @param: int optionIndex -- index of the to be deleted Option
	 * 		   int optionSetIndex -- index of the OptionSet which the to be deleted Option stays in
	 * 
	 */
	public void deleteOption(int optionIndex, int optionSetIndex) {
		OptionSet optionSetTemp = this.getOptionSet(optionSetIndex);
		optionSetTemp.setOption(optionIndex, null);
	}
	
	/*
	 * Delete an OptionSet by index
	 * @param: int optionSetIndex -- index of the OptionSet to be deleted
	 */
	public void deleteOptionSet(int optionSetIndex) {
		this.setOptionSet(optionSetIndex, null);
	}
	
	/*
	 * Update name of OptionSet
	 * @param: String oldName, String newName -- old and new name of the updated OptionSet
	 */
	public void updateOptionSetName(String oldName, String newName) {
		if (oldName == null || newName == null) {
			System.out.println("Error: In order to update name of the OptionSet, "
					+ "neither old name nor the new name could be null");			
			return;
		}
		int index = this.findOptionSet(oldName);
		this.getOptionSet(index).setName(newName);
	}
	
	/*
	 * Update a whole OptionSet
	 * @param: String name, OptionSet newOptionSet
	 */
	public void updateOptionSet(String name, OptionSet newOptionSet) {
		int index = this.findOptionSet(name);
		this.setOptionSet(index, newOptionSet);
	}
	
	/*
	 * Update name of Option
	 * @param: String oldName, String newName
	 */
	public void updateOptionName(String optionSetName, String oldName, String newName) {
		if (optionSetName == null || oldName == null || newName == null) {
			System.out.println("Error: In order to update name of Option, "
					+ "neither old name nor the new name could be null");
			return;
		}
		int optionSetIndex = this.findOptionSet(optionSetName);
		OptionSet optionSetTemp = this.getOptionSet(optionSetIndex);
		int optionIndex = optionSetTemp.findOption(oldName);
		if (optionIndex == -1) {
			System.out.println("Error: there is no Option with name " + oldName);
			return;
		}
		optionSetTemp.setOptionName(optionIndex, newName);		
	}
	
	/*
	 * Update price of Option
	 * @param: String optionSetName, String optionName, float newPrice
	 */
	public void updateOptionPrice(String optionSetName, String optionName, float newPrice) {
		if (optionSetName == null || optionName == null) {
			System.out.println("Error: OptionSet name nor the Option name cannot be null");
			return;
		}
		int optionSetIndex = this.findOptionSet(optionSetName);
		OptionSet optionSetTemp = this.getOptionSet(optionSetIndex);
		int optionIdnex = optionSetTemp.findOption(optionName);
		if (optionIdnex == -1) {
			System.out.println("Error: there is no Option with name " + optionName);
			return;
		}
		optionSetTemp.setOptionPrice(optionIdnex, newPrice);
	}
	
	/*
	 * Print method
	 */
	public void print() {
		StringBuilder sb = new StringBuilder();
		sb.append("Automotive name: ").append(this.name).append("\n");
		sb.append("Base price: ").append(this.basePrice);
		System.out.println(sb.toString());
		for (int i = 0; i < this.optionSets.size(); i ++) {
			OptionSet optionSetTemp = this.optionSets.get(i);
			if (optionSetTemp == null) {
				System.out.println("NULL");
				continue;
			}
			optionSetTemp.printOptionSet();
		}
	}
	
	/*
	 * Print selected choice and total price for now
	 */
	public void printChoiceAndPrice() {
		StringBuilder sb = new StringBuilder();
		sb.append("Automotive name: ").append(this.name).append("\n");
		sb.append("Choices made:").append("\n");
		for (int i = 0; i < this.choice.size(); i ++) {
			sb.append("\t").append(this.choice.get(i).getName()).append(": ")
				.append(this.choice.get(i).getPrice()).append("\n");
		}
		sb.append("Total Price is ").append(this.getTotalPrice());
		System.out.println(sb.toString());
	}
	
}
