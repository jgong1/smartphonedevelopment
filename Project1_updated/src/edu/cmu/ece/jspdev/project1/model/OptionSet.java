/*
 * JSPDEV Project Unit 1
 * @Author Jian Gong, AndrewID: jgong1
 */

package edu.cmu.ece.jspdev.project1.model;

import java.io.Serializable;
import java.util.ArrayList;

public class OptionSet implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private String name;
	private ArrayList<Option> options;
	private String choice;
	
	/*
	 * Constructors
	 */
	protected OptionSet() { }
	
	protected OptionSet(String newName) {
		this.name = newName;
	}
	
	protected OptionSet(String newName, int size) {
		this.name = newName;
		this.options = new ArrayList<Option>(size);
		for (int i = 0; i < size; i ++) {
			this.options.add(new Option());
		}		
	}
	
	protected OptionSet(ArrayList<Option> newOptions) {
		this.options = newOptions;
	}
	
	protected OptionSet(String newName, ArrayList<Option> newOptions) {
		this.name = newName;
		this.options = newOptions;
	}
	
	/*
	 * Getters and setters
	 */
	protected void setName(String newName) {
		this.name = newName;
	}
	
	protected void setOptionName(int index, String newName) {
		if (newName == null) {
			System.out.println("Error: new name of option cannot be null");
			return;
		}
		this.options.get(index).setName(newName);
	}
	
	protected void setOptionPrice(int index, float newPrice) {
		this.options.get(index).setPrice(newPrice);
	}
	
	protected void setOptions(int size) {
		this.options = new ArrayList<Option>(size);
		
		for (int i = 0; i < size; i ++) {
			this.options.add(new Option());
		}	
	}
	
	protected void setOptions(ArrayList<Option> newOptions) {
		this.options = newOptions;
	}
	
	protected void setOption(int index, Option newOption) {
		if (index < 0 || index >= this.options.size()) {
			System.out.println("Error: Index out of range!");
			return;
		}
		this.options.set(index, newOption);
	}
	
	protected void setOptionChoice(String optionName) {
		this.choice = optionName;
	}
	
	protected String getName() {
		return this.name;
	}
	
	protected ArrayList<Option> getOptions() {
		return this.options;
	}
	
	protected Option getOption(int index) {
		if (index < 0 || index >= this.options.size()) {
			System.out.println("Error: Index out of range!");
			return null;
		}
		return this.options.get(index);
	}
	
	protected Option getOptionChoice() {
		int index = findOption(this.choice);
		if (index == -1) {
			return null;
		}
		return this.options.get(index);
	}
	
	
	/*
	 * Find Option with name
	 * @param: String name -- Name of the Option
	 * @return: int index -- index of the found Option 
	 * 						return -1 if Option is not found
	 */
	protected int findOption(String name) {
		if (name == null) {
			System.out.println("Error: In order to find Option, name of the Option cannot be null!");
		}
		for (int i = 0; i < this.options.size(); i ++) {
			if (this.options.get(i) == null) {
				continue;
			}
			String nameTemp = this.options.get(i).getName();
			if (nameTemp.equals(name)) {
				return i;
			}
		}
		return -1;
	}
	
	/*
	 * Print method
	 */
	protected void printOptionSet() {
		StringBuilder sb = new StringBuilder();
		sb.append("OptionSet name: ").append(this.name).append("\n");
		System.out.println(sb.toString());
		for (int i = 0; i < this.options.size(); i ++) {
			Option optionTemp = this.options.get(i);	
			if (optionTemp == null) {
				System.out.println("NULL");
				continue;
			}
			optionTemp.printOption();			
		}
		
	}
	
	/*
	 * Inner class -- Option
	 */
	protected class Option implements Serializable {
		
		private static final long serialVersionUID = 1L;
		private String name;
		private float price;
		
		/*
		 * Constructors
		 */
		protected Option () { }
		
		protected Option (String newName) {
			this.name = newName;			
		}
		
		protected Option (float newPrice) {
			this.price = newPrice;
		}
		
		protected Option (String newName, float newPrice) {
			this.name = newName;
			this.price = newPrice;
		}
		
		/*
		 * Getters and setters
		 */
		protected void setName(String newName) {
			this.name = newName;
		}
		
		protected void setPrice(float newPrice) {
			this.price = newPrice;
		}
		
		protected String getName () {
			return this.name;
		}
		
		protected float getPrice () {
			return this.price;
		}
		
		/*
		 * Print method
		 */
		protected void printOption() {
			StringBuilder sb = new StringBuilder();
			sb.append("\t");
			sb.append("Option Name: ").append(this.name).append("\t");
			sb.append("Option Price: ").append(this.price).append("\n");
			System.out.println(sb.toString());
			
		}
		
	}
}
