/*
 * JSPDEV Project Unit 1
 * @Author Jian Gong, AndrewID: jgong1
 */

package edu.cmu.ece.jspdev.project1.util;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import edu.cmu.ece.jspdev.project1.exception.AutoException;
import edu.cmu.ece.jspdev.project1.exception.ErrorMessageEnum;
import edu.cmu.ece.jspdev.project1.exception.ErrorNumberEnum;
import edu.cmu.ece.jspdev.project1.model.Automobile;

public class Util {
	public Automobile buildAutoObject(String fileName, Automobile automobile) throws AutoException{	
		FileReader file = null;
		BufferedReader br = null;
		try {			
			file = new FileReader(fileName);
			br = new BufferedReader(file);
			String line = "";
			int count = 0;
			automobile = new Automobile();
			while((line = br.readLine()) != null) {			
				if (count == 0) {
					String[] elements = line.split("\t");
					String make = elements[0];
					String model = elements[1];					
					String automobileName = make + " " + model;
					float automobilePrice = (float)0;					
					try {
						automobilePrice = Float.parseFloat(elements[2]);
					} catch (NumberFormatException nfe) {
						throw new AutoException(ErrorNumberEnum.MissingAutomobilePrice.getErrorNo(), 
								ErrorMessageEnum.MissingAutomobilePrice.getErrorMSG()); // Throws exception 3 -- Price for automobile is missing/illegal or not
					}					 
//					System.out.println(automobile);
					automobile.setMake(make);
					automobile.setModel(model);
					automobile.setName(automobileName);
					automobile.setBasePrice(automobilePrice);
				} else {
					String[] elements = line.split("\t");
					if (elements[0] == "" || elements[0].contains(":")) {
						throw new AutoException(ErrorNumberEnum.MissingOptionSetName.getErrorNo(),
								ErrorMessageEnum.MissingOptionSetName.getErrorMSG()); // Throw exception 4 -- OptionSet name missing
					}
					automobile.addOptionSet(elements[0], elements.length - 1);
					for (int i = 0; i < elements.length - 1; i ++) {
						String optionStrTemp = elements[i + 1];						
						String[] optionElements = optionStrTemp.split(":");
						if (optionElements[0] == "") {
							throw new AutoException(ErrorNumberEnum.MissingOptionName.getErrorNo(),
									ErrorMessageEnum.MissingOptionName.getErrorMSG()); // Throw exception 5 -- Option name missing
						}
						float optionPrice = (float)0;
						try {
							optionPrice = Float.parseFloat(optionElements[1]);
						} catch (NumberFormatException nfe) {
							throw new AutoException(ErrorNumberEnum.MissingOptionPrice.getErrorNo(),
									ErrorMessageEnum.MissingOptionPrice.getErrorMSG()); // Throw exception 6 -- Option price missing/illegal
						}						
						automobile.setOption(count - 1, i, optionElements[0], optionPrice);
					}
				}
				count ++; 			
			}			
			
			if (count == 0) {
				throw new AutoException(ErrorNumberEnum.EmptyFile.getErrorNo(),
						ErrorMessageEnum.EmptyFile.getErrorMSG()); // Throw exception 1 -- file is empty
			}			
		} catch (IOException f) {
			System.out.println("Error message: " + f.toString());
		} finally {
			try {
				br.close();
			} catch (IOException g) {
				System.out.println("Error message: " + g.toString());
			}
		}
		return automobile;
	}
	
	public void writeSerializeFile(Automobile automobile, String fileName) {
		try {
			FileOutputStream fos = new FileOutputStream(fileName);
			ObjectOutputStream out = new ObjectOutputStream(fos);
			out.writeObject(automobile);
			out.close();
		} catch (Exception e) {
			System.out.println("Error: " + e.toString());
			System.exit(1);
		}
	}
	
	public Automobile readSerializeFile(String fileName) {
		Automobile newAuto = null;
		try {
			FileInputStream fis = new FileInputStream(fileName);
			ObjectInputStream in = new ObjectInputStream(fis);
			newAuto = (Automobile)in.readObject();
			newAuto.print();
			in.close();			
		} catch (Exception e) {
			System.out.println("Error: " + e.toString());
			System.exit(1);
		}
		return newAuto;
	}
	
}
