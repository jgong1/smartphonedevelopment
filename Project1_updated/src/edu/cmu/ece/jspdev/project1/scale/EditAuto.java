package edu.cmu.ece.jspdev.project1.scale;

public interface EditAuto {
	/*
	 * Given a Model name, an OptionSet name, an Option name, new Option name
	 * 		edit this particular Option name
	 * @param: String model, String optionSetName, 
	 * 		String oldOptionName, String newOptionName
	 */
	public void editOptionName(String model, String optionSetName, String oldOptionName, String newOptionName);
	
	/*
	 * Given a Model name, an OptionSet name, an Option name, new Option price
	 * 		edit this particular Option price
	 * @param: String model, String optionSetName, String optionName, float newPrice
	 */
	public void editOptionPrice(String model, String optionSetName, String optionName, float price);
}
