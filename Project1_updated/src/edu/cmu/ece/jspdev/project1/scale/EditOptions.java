package edu.cmu.ece.jspdev.project1.scale;

import edu.cmu.ece.jspdev.project1.adapter.*;
import edu.cmu.ece.jspdev.project1.model.*;

public class EditOptions extends Thread implements EditAuto {
	private String model;
	private String optionSetName;
	private String optionName;
	private String newOptionName;
	private float newOptionPrice;
	private int flag;
	
	/*
	 * Constructor of EditOptions, create a new thread for the particular Option
	 * @param: String model -- model name
	 * 		   String optionSetName -- OptionSet name
	 * 	       String optionName -- Option name
	 * 		   int flag -- flag for edit name '1' or edit price '2'
	 */
	public EditOptions (String model, String optionSetName, String optionName, String newOptionName, int flag) {
		this.model = model;
		this.optionSetName = optionSetName;
		this.optionName = optionName;
		this.newOptionName = newOptionName;
		this.flag = flag;
	}
	
	public EditOptions (String model, String optionSetName, String optionName, float newOptionPrice, int flag) {
		this.model = model;
		this.optionSetName = optionSetName;
		this.optionName = optionName;
		this.newOptionPrice = newOptionPrice;
		this.flag = flag;
	}
	
	/*
	 * Function for editing name of a particular Option
	 * @param: String newOptionName -- new name of Option
	 */
	public synchronized void editOptionName(String model, String optionSetName, 
			String oldOptionName, String newOptionName) {
		Automobile auto = ProxyAutoMobile.automobileSet.findAutomobile(model);
		if (auto == null) {
			System.out.println("Model " + model + " does not exist! Cannot edit Option name...");
		} else {
			auto.updateOptionName(optionSetName, oldOptionName, newOptionName);			
		}
	}
	
	/*
	 * Function for editing price of a particular Option
	 * @param: int newOptionPrice
	 */
	public synchronized void editOptionPrice(String model, String optionSetName, 
			String optionName, float newOptionPrice) {
		Automobile auto = ProxyAutoMobile.automobileSet.findAutomobile(model);
		if (auto == null) {
			System.out.println("Model "+ model + " does not exist! Cannot edit Option price...");
		} else {
			auto.updateOptionPrice(optionSetName, optionName, newOptionPrice);
		}
	}
	
	/*
	 * Helper function for wait random time
	 */
	@SuppressWarnings("static-access")
	private void randomWait() {
        try {
            Thread.currentThread().sleep((long)(3000*Math.random()));
        } catch(InterruptedException e) {
            System.out.println("Interrupted!");
        }
    }    
	
	/*
	 * java.lang.runnable
	 */
	public void run() {
		synchronized(System.out) {
			switch (flag) {
			case 1:
				randomWait();
				editOptionName(this.model, this.optionSetName, this.optionName, this.newOptionName);
				break;
			case 2:
				randomWait();
				editOptionPrice(this.model, this.optionSetName, this.optionName, this.newOptionPrice);
				break;
			default:
				System.out.println("Wrong flag input, flag can only be 1 or 2...");
				break;
			}
		}
	}
}
