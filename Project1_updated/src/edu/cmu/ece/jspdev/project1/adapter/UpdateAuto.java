package edu.cmu.ece.jspdev.project1.adapter;

public interface UpdateAuto {
	/*
	 * This function searches the Model for a given OptionSet and sets the name of the OptionSet to newName
	 */
	public void updateOptionSetName(String modelName, String optionSetName, String newName);
	
	/*
	 * This function searches the Model for a given OptionSet and Option name, and sets the price to newPrice.
	 */
	public void updateOptionPrice(String modelName, String optionName, String option, float newPrice);
	
}
