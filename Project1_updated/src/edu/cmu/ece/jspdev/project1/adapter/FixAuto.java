package edu.cmu.ece.jspdev.project1.adapter;

public interface FixAuto {
	/*
	 * Given an error no, try to fix the error.
	 */
	public String fix(int errorNo);
}
