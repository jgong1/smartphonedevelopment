package edu.cmu.ece.jspdev.project1.adapter;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.sql.Timestamp;

import edu.cmu.ece.jspdev.project1.exception.AutoException;
import edu.cmu.ece.jspdev.project1.model.Automobile;
import edu.cmu.ece.jspdev.project1.model.AutomobileSet;
import edu.cmu.ece.jspdev.project1.scale.EditOptions;
import edu.cmu.ece.jspdev.project1.util.Util;

public abstract class ProxyAutoMobile {
	
	public static AutomobileSet automobileSet = new AutomobileSet(); 	
	
	/*
	 * Given a text file name this method can be written to build an instance of Automobile.
	 * @param: String fileName -- Input file for building the automobile
	 */
	public void buildAuto(String fileName) {
		Util util = new Util();
		boolean flag = false;
		do {
			Automobile automobile = new Automobile();
			try {				
				automobile = util.buildAutoObject(fileName, automobile);
				addAutoToSet(automobile);
				flag = true;				
			} catch (AutoException e) {
				System.out.println("Exception catched!");
				StringBuilder sb = new StringBuilder();
				sb.append("Exception catched at ");
				java.util.Date date = new java.util.Date();
				sb.append(new Timestamp(date.getTime())).append("\n");
				sb.append("\t").append("Error No:").append(e.getErrorNo());
				sb.append("\t").append("Error Message:").append(e.getErrorMSG()).append("\n");
				try {									// Log exceptions into log file
					FileOutputStream fos = new FileOutputStream("log.txt", true);
					BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
					bw.write(sb.toString());
					bw.close();
				} catch (IOException ioe) {
					System.out.println("Error when writing to log file");
				}
				
				String fixResult = "";
				switch (e.getErrorNo()) {
				case 1:									// Fix empty file exception
					fixResult = e.fix(e.getErrorNo());
					fileName = fixResult;
					break;				
				default:
					break;
				}
			}
		} while(flag == false);			
	}
	
	/*
	 * Add Automobile to LinkedHashMap
	 * @param: Automobile automobile 
	 */
	public void addAutoToSet(Automobile automobile) {
		automobileSet.addAutomobile(automobile);
	}
	
	/*
	 * Delete Automobile from LinkedHashMap
	 * @param: String name -- name of the Automobile to be deleted
	 * @return: Automobile -- the Automobile just deleted
	 */
	public Automobile deleteAutoFromSet(String name) {
		Automobile auto = automobileSet.deleteAutomobile(name);
		return auto;
	}
	
	
	/*
	 * This function searches and prints the properties of a given Automobile
	 * @param: In part A, there is no need for this input fileName
	 */
	public void printAuto(String modelName) {
		// As there is only one model in part A
		// Just pass the name of the Automobile this class is working with
		Automobile auto = automobileSet.findAutomobile(modelName);
		auto.print();
	}

	/*
	 * This function searches the Model for a given OptionSet and sets the name of the OptionSet to newName
	 * @param: In Part A, there is no need for the input String modelName
	 */
	public void updateOptionSetName(String modelName, String optionSetName,
			String newName) {
		// As there is only one model in part A
		// Just ignore the input String modelName
		Automobile auto = automobileSet.findAutomobile(modelName);
		auto.updateOptionSetName(optionSetName, newName);		
	}
	
	/*
	 * This function searches the Model for a given OptionSet and Option name, and sets the price to newPrice.
	 * @param: In Part A, there is no need for the input String modelName
	 */
	public void updateOptionPrice(String modelName, String optionSetName,
			String optionName, float newPrice) {
		// As there is only one model in part A
		// Just ignore the input String modelName
		Automobile auto = automobileSet.findAutomobile(modelName);
		auto.updateOptionPrice(optionSetName, optionName, newPrice);
	}
	
	/*
	 * Implements function editOptionName in interface editAuto
	 */
	public void editOptionName(String model, String optionSetName, String oldOptionName, String newOptionName) {
		EditOptions editOption = new EditOptions(model, optionSetName, oldOptionName, newOptionName, 1);
		editOption.start();
	}
	
	/*
	 * Implements function editOptionPrice in interface editAuto
	 */
	public void editOptionPrice(String model, String optionSetName, String optionName, float newOptionPrice) {
		EditOptions editOption = new EditOptions(model, optionSetName, optionName, newOptionPrice, 2);
		editOption.start();
	}
	
	/*
	 * Given an error no, try to fix the error.
	 * @param: int errorNo, the number of the error.
	 */
	public String fix(int errorNo) {
		AutoException autoException = new AutoException(errorNo);
		String fixResult = autoException.fix(errorNo);
		return fixResult;
	}

}
