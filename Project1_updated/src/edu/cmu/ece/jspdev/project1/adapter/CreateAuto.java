package edu.cmu.ece.jspdev.project1.adapter;

public interface CreateAuto {
	/*
	 * Given a text file name this method can be written to build an instance of Automobile.
	 */
	public void buildAuto(String fileName);
	
	/*
	 * This function searches and prints the properties of a given Automobile
	 */
	public void printAuto(String modelName);
	
}
