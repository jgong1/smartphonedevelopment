package edu.cmu.ece.jspdev.project1.exception;

public enum ErrorNumberEnum {
	EmptyFile(1),
	MissingAutomobilePrice(2),
	MissingOptionSetName(3),
	MissingOptionName(4),
	MissingOptionPrice(5);
	
	private int errorNo;
	
	private ErrorNumberEnum(int errorNo) {
		this.errorNo = errorNo;
	}
	
	public int getErrorNo() {
		return this.errorNo;
	}

}
