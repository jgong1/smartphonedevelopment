package edu.cmu.ece.jspdev.project1.exception;

import java.util.Scanner;

public class FixHelper {
	private Scanner scanner;
	/*
	 * Fix Helper for error (empty file exception)
	 */
	public String fixEmptyFile(int errorNo) {
		scanner = new Scanner(System.in);
		System.out.println("Error [" + ErrorMessageEnum.EmptyFile + "]. Trying to fix error [" + errorNo + "].");
		System.out.print("Please input path of a valid file: ");		
		String path = scanner.next();
		return path;
	}
	
	/*
	 * Fix helper for error (Missing or illegal automobile base price)
	 */
	public String fixMissingAutomobilePrice(int errorNo) {
		scanner = new Scanner(System.in);
		System.out.println("Error [" + ErrorMessageEnum.MissingAutomobilePrice + "]. Trying to fix error [" + errorNo + "].");
		System.out.print("Please input a valid base price: ");
		String price = scanner.next();
		return price;
	}
	
	/*
	 * Fix helper for error (Missing OptionSet name)
	 */
	public String fixMissingOptionSetName(int errorNo) {
		scanner = new Scanner(System.in);
		System.out.println("Error [" + ErrorMessageEnum.MissingOptionSetName + "]. Trying to fix error [" + errorNo + "].");
		System.out.print("Please input a valid OptionSet name: ");
		String name = scanner.next();
		return name;
	}
	
	/*
	 * Fix helper for error (Missing Option name)
	 */
	public String fixMissingOptionName(int errorNo) {
		scanner = new Scanner(System.in);
		System.out.println("Error [" + ErrorMessageEnum.MissingOptionName + "]. Trying to fix error [" + errorNo + "].");
		System.out.print("Please input a valid Option name: ");
		return scanner.next();
	}
	
	/*
	 * Fix helper for error (Missing or illegal Option price)
	 */
	public String fixMissingOptionPrice(int errorNo) {
		scanner = new Scanner(System.in);
		System.out.println("Error [" + ErrorMessageEnum.MissingOptionPrice + "]. Trying to fix error [" + errorNo + "].");
		System.out.print("Please input a valid Option price: ");
		String price = scanner.next();
		return price;
	}
	
	
}
