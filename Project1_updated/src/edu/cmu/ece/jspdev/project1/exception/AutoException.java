package edu.cmu.ece.jspdev.project1.exception;

import edu.cmu.ece.jspdev.project1.adapter.FixAuto;

public class AutoException extends Exception implements FixAuto{
	private static final long serialVersionUID = 1L;
	private int errorNo;
	private String errorMSG;
	
	public AutoException(int errorNo) {
		super();
		this.errorNo = errorNo;
		printError();
	}
	
	public AutoException(String errorMSG) {
		super();
		this.errorMSG = errorMSG;
		printError();
	}
	
	public AutoException(int errorNo, String errorMSG) {
		super();
		this.errorNo = errorNo;
		this.errorMSG = errorMSG;
		printError();
	}
	
	public int getErrorNo() {
		return this.errorNo;
	}
	
	public String getErrorMSG() {
		return this.errorMSG;
	}
	
	public void setErrorNo(int newErrorNo) {
		this.errorNo = newErrorNo;
	}
	
	public void setErrorMSG(String newErrorMSG) {
		this.errorMSG = newErrorMSG;
	}
	
	public void printError() {
		System.out.println("AutoException: Error Message=" + this.errorMSG);
	}
	
	public String fix(int errorNo) {		
		FixHelper fixHelper = new FixHelper();
		String fixResult = "";		
		switch(errorNo) {
		case 1:
			fixResult = fixHelper.fixEmptyFile(errorNo);
			break;
		default:
			break;
		}
		return fixResult;
	}
	
}
