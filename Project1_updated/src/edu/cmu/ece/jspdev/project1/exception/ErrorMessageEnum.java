package edu.cmu.ece.jspdev.project1.exception;

public enum ErrorMessageEnum {
	EmptyFile("Input file is an empty file!"),
	MissingAutomobilePrice("Price for automobile is missing or illegal!"),
	MissingOptionSetName("Name of OptionSet is missing or illegal!."),
	MissingOptionName("Name of Option is missing."),
	MissingOptionPrice("Price of Option is missing or illegal!");
	
	private String errorMSG;
	
	private ErrorMessageEnum (String errorMSG) {
		this.errorMSG = errorMSG;
	}
	
	public String getErrorMSG() {
		return this.errorMSG;
	}
}
