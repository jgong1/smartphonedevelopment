package edu.cmu.ece.jspdev.project1.driver;

import edu.cmu.ece.jspdev.project1.adapter.*;
import edu.cmu.ece.jspdev.project1.scale.*;


public class Driver_Unit3 {
	public static void main(String[] args) {
		/*
		 * Create Automobile and print the original version
		 */
		CreateAuto auto = new BuildAuto();
		auto.buildAuto("FordZTW.txt");
		System.out.println("-----------------");
		System.out.println("Result of intact input file.");
		auto.printAuto("Ford Focus Wagon ZTW");
		System.out.println("-----------------");
		System.out.println("-----------------");
		
		boolean t1IsAlive = true;
		boolean t2IsAlive = true;
		
		/*
		 * Two threads trying to edit option name
		 */
		
		EditOptions edit = new EditOptions("Ford Focus Wagon ZTW", "Color", "Liquid Grey Clearcoat Metallic", "Just Grey", 1);
		Thread t1 = new Thread(edit);
		Thread t2 = new Thread(edit);
		t1.start();
		t2.start();
		do {
			if (t1IsAlive && !t1.isAlive()) {
				t1IsAlive  = false;
				System.out.println("t1 is dead.");
			}
			if (t2IsAlive && ! t2.isAlive()) {
				t2IsAlive = false;
				System.out.println("t2 is dead.");
			}
		} while(t1IsAlive || t2IsAlive);
		System.out.println("Result after two threads executed.");
		auto.printAuto("Ford Focus Wagon ZTW");
		
		
		System.out.println("-----------------");
		System.out.println("-----------------");
		
		boolean t3IsAlive = true;
		boolean t4IsAlive = true;
		
		/*
		 * Two threads trying to edit option price
		 */
		
		EditOptions edit1 = new EditOptions("Ford Focus Wagon ZTW", "Color", "Liquid Grey Clearcoat Metallic",
				(float)1000.0, 2);
		Thread t3 = new Thread(edit1);
		Thread t4 = new Thread(edit1);
		t3.start();
		t4.start();
		do {
			if (t3IsAlive && !t3.isAlive()) {
				t3IsAlive  = false;
				System.out.println("t3 is dead.");
			}
			if (t4IsAlive && ! t4.isAlive()) {
				t4IsAlive = false;
				System.out.println("t4 is dead.");
			}
		} while(t3IsAlive || t4IsAlive);
		System.out.println("Result after two threads executed.");
		auto.printAuto("Ford Focus Wagon ZTW");
		
		
	}
}
