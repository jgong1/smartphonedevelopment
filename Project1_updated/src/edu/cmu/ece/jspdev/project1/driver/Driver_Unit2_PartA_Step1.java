package edu.cmu.ece.jspdev.project1.driver;

import edu.cmu.ece.jspdev.project1.adapter.BuildAuto;
import edu.cmu.ece.jspdev.project1.adapter.CreateAuto;

public class Driver_Unit2_PartA_Step1 {
	public static void main (String[] args) {
		/*
		 * Test for Unit2 Part A
		 * As there is only one model, there is no need to pass 
		 * String modelName into functions.
		 */
		
		/*
		 * Create and print Auto
		 */
		System.out.println("Create and Print Auto Test.");
		CreateAuto auto = new BuildAuto();
		auto.buildAuto("FordZTW.txt");
		auto.printAuto("Ford Focus Wagon ZTW");
		
		/*
		 * Update OptionSet's name or Option price
		 */
		System.out.println("----------------------------");
		System.out.println("Update OptionSet Name or Update Option price.");
		System.out.println("Cannot update!");
	}
}
