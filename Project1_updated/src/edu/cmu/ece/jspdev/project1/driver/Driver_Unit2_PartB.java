package edu.cmu.ece.jspdev.project1.driver;

import edu.cmu.ece.jspdev.project1.exception.AutoException;
import edu.cmu.ece.jspdev.project1.model.Automobile;
import edu.cmu.ece.jspdev.project1.util.Util;

public class Driver_Unit2_PartB {
	public static void main(String[] args) {
		/*
		 * Test for new intact input file 
		 */
		Automobile automobile = new Automobile();
		Util util = new Util();
		try {
			automobile = util.buildAutoObject("FordZTW.txt", automobile);
		} catch (AutoException e) {
			System.out.println("Exception catched!");
			System.out.println("Error message: " + e.getErrorMSG() + " Error number: " + e.getErrorNo());
		}
		System.out.println("-------Results from reading text file-------");
		automobile.print();
		
		/*
		 * Test for set/get choice and get total price
		 */
		System.out.println("--------------");
		System.out.println("--------------");
		System.out.println("-------Results of testing setchoices--------");
		automobile.setOptionChoice("Color", "French Blue Clearcoat Metallic");
		automobile.setOptionChoice("Transmission", "Automatic");
		automobile.setOptionChoice("Brakes", "ABS");
		automobile.setOptionChoice("Side Impact Air Bags", "Selected");
		automobile.setOptionChoice("Power Moonroof", "None");
		automobile.printChoiceAndPrice();
	}
}
