package edu.cmu.ece.jspdev.project1.driver;

import edu.cmu.ece.jspdev.project1.adapter.*;

public class Driver_Unit2_PartA_Step2 {
	public static void main (String[] args) {
		System.out.println("Different test cases for testing exception handling.");
		
		/*
		 * Test case for intact input file
		 */
		System.out.println("----------------");
		System.out.println("----------------");
		System.out.println("Result of intact input file:");
		CreateAuto auto = new BuildAuto();
		auto.buildAuto("FordZTW.txt");
		auto.printAuto("Ford Focus Wagon ZTW");
		
		
		/*
		 * Test case for empty input file
		 */
		System.out.println("----------------");
		System.out.println("----------------");
		System.out.println("Result of empty file:");
		CreateAuto auto2 = new BuildAuto();
		auto2.buildAuto("empty.txt");
		auto2.printAuto("Ford Focus Wagon ZTW");
		
		
	}
}
