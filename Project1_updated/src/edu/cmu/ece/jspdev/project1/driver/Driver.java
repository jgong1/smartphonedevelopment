package edu.cmu.ece.jspdev.project1.driver;

import edu.cmu.ece.jspdev.project1.exception.AutoException;
import edu.cmu.ece.jspdev.project1.model.Automobile;
import edu.cmu.ece.jspdev.project1.util.Util;


public class Driver {
	public static void main(String[] args) {
		/*
		 * Test for input file and serialization
		 */
		Automobile FordZTW = new Automobile();
		Util util = new Util();
		try {
			FordZTW = util.buildAutoObject("FordZTW.txt", FordZTW);
		} catch (AutoException e) {
			e.printStackTrace();
		}
		System.out.println("-------Results from reading text file-------");
		FordZTW.print();
		util.writeSerializeFile(FordZTW, "serializedFile.ser");
		Automobile newAutomobile = util.readSerializeFile("serializedFile.ser");
		System.out.println();
		System.out.println("-------Results from reading serialized file-------");
		newAutomobile.print();
		
		/*
		 * Test for set OptionSet name 
		 */
		System.out.println("--------------------");
		System.out.println("--------------------");
		System.out.println("Results after set the second OptionSet size");
		Automobile setAuto = FordZTW;
		setAuto.setOptionSet(1, "New OptionSet Name", 4);
		setAuto.print();
		
		/*
		 * Test for update OptionSet name
		 */
		System.out.println("--------------------");
		System.out.println("--------------------");
		System.out.println("Results after update the first OptionSet name");
		Automobile updateAuto = FordZTW;
		updateAuto.updateOptionSetName("Color", "New Color");
		updateAuto.print();
		
		/*
		 * Test for update Option name
		 */
		System.out.println("--------------------");
		System.out.println("--------------------");
		System.out.println("Results after update the perticular Option name");
		Automobile updateAuto2 = FordZTW;
		updateAuto2.updateOptionName("Color", "Fort Knox Gold Clearcoat Metallic", "New Color Name");
		updateAuto2.print();
		
		/*
		 * Test for update Option price
		 */
		System.out.println("--------------------");
		System.out.println("--------------------");
		System.out.println("Results after update the perticular Option price");
		Automobile updateAuto3 = FordZTW;
		updateAuto3.updateOptionPrice("Color", "Fort Knox Gold Clearcoat Metallic", 10000);
		updateAuto3.print();
		
		/*
		 * Test for set Option
		 */
		System.out.println("--------------------");
		System.out.println("--------------------");
		System.out.println("Result after set the third Option in first OptionSet");
		Automobile setAuto2 = FordZTW;
		setAuto2.setOption(0, 2, "New Color", 10000);
		setAuto2.print();
		
		/*
		 * Test for deleting Option
		 */
		System.out.println("--------------------");
		System.out.println("--------------------");
		System.out.println("Results after delete the first Option in first OptionSet");
		Automobile delAuto = FordZTW;
		delAuto.deleteOption(0, 0);
		delAuto.print();
		
		/*
		 * Test for deleting OptionSet
		 */
		System.out.println("--------------------");
		System.out.println("--------------------");
		System.out.println("Results after delete the first OptionSet");
		Automobile delAuto2 = FordZTW;
		delAuto2.deleteOptionSet(0);
		delAuto2.print();
		
		
		
	}
	
}
