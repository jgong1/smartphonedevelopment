package database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Trip;
import model.User;

public class UserDBAction extends AbstractDBAction {
	
	public UserDBAction() {
		super();
	}
	
	/**
	 * Create a new User in the database 
	 */
	public void createUser(String username, String password, String profilePicture) {
		StringBuilder sb = new StringBuilder();
		sb.append(props.get("create_user"));
		sb.append(username);
		sb.append("', '");
		sb.append(password);
		sb.append("', '");
		sb.append(profilePicture);
		sb.append("')");
		executeUpdate(sb.toString());
	}
	
	public int getUserID(String username) {
		StringBuilder sb = new StringBuilder();
		sb.append(props.get("get_user"));
		sb.append(username);
		sb.append("' LIMIT 1");
		System.out.println(sb.toString());
		ResultSet rs = executeQuery(sb.toString());
		int id = -1;
		try {
			while (rs.next()) {
				id = rs.getInt("id");
				break;
			}
		} catch (SQLException e) {
			System.out.println("Error getting User id");
			e.printStackTrace();
		}
		return id;
	}
	
	/**
	 * Get a instance of User from the database for the specified username.
	 * @param username
	 * @return
	 */
	public User getUser(String username) {
		StringBuilder sb = new StringBuilder();
		sb.append(props.get("get_user"));
		sb.append(username);
		sb.append("' LIMIT 1");
		System.out.println(sb.toString());
		User user = new User(username);
		ResultSet rs = executeQuery(sb.toString());
		String profilePicture = "";
		int id = -1;
		try {
			while (rs.next()) {
				profilePicture = rs.getString("profilePicture");
				id = rs.getInt("id");
				break;
			}
		} catch (SQLException e) {
			System.out.println("Error getting User");
			e.printStackTrace();
		}
		List<Trip> trips = new TripDBAction().getTrips(id);
		List<String> friends = new FriendDBAction().getFriends(id);
		user.setProfilePicture(profilePicture);
		user.setTrips(trips);
		user.setFriends(friends);
		return user;
	}
	
	/**
	 * Get a list of all usernames.
	 * @return
	 */
	public List<String> getUserList() {
		String sql = props.getProperty("get_user_list");
		List<String> users = new ArrayList<String>();
		try {
			ResultSet rs = executeQuery(sql);
			while (rs.next()) {
				String username = rs.getString("Username");
				users.add(username);
			}
		} catch (SQLException e) {
			System.out.println("Error getting User");
			e.printStackTrace();
		}
		return users;
	}
	
	/** 
	 * Validate a login attempt. Returns true if the username/password
	 * is correct. Returns false otherwise.
	 */
	public boolean checkUserCredentials(String username, String password) {
		boolean result = false;
		StringBuilder sb = new StringBuilder();
		sb.append(props.getProperty("check_credentials"));
		sb.append(username);
		sb.append("'");
		System.out.println("Checking username=" + username + ", password=" + password);
		ResultSet rs = executeQuery(sb.toString());
		try {
			while (rs.next()) {
				String storedPassword = rs.getString("password");
				if (storedPassword == null) {
					System.out.println("NULL");
				}
				if (storedPassword.equals(password)) {
					System.out.println("MATCHING PASSWORD");
					result = true;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * Check if username has already been taken. Returns true if there already
	 * exists a row in the database with the specified username. 
	 * Otherwise, return false.
	 */
	public boolean duplicateUsername(String username) {
		StringBuilder sb = new StringBuilder();
		sb.append(props.getProperty("get_user"));
		sb.append(username);
		sb.append("'");
		ResultSet rs = executeQuery(sb.toString());
		try {
			while (rs.next()) {
				return true;
			}
		} catch (SQLException e) {
			System.out.println("Error checking duplicate username");
			e.printStackTrace();
		}
		return false;
	}
}
