package model;

/**
 * Model for entries of photos.
 * @author elliotttan
 *
 */
public class Photo {
	private String photo;

	public Photo(String photo) {
		this.photo = photo;
	}
}
