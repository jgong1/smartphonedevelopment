package model;

import java.util.ArrayList;
import java.util.List;

public class Place {
	private double latitude;
	private double longitude;
	private List<Photo> photos;
	private List<Note> notes;
	
	public Place() {
		
	}
	
	public Place(double latitude, double longitude) {
		this.latitude = latitude;
		this.longitude = longitude;
		this.photos = new ArrayList<Photo>();
		this.notes = new ArrayList<Note>();
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public List<Photo> getPhotos() {
		return photos;
	}

	public void setPhotos(List<Photo> photos) {
		this.photos = photos;
	}

	public List<Note> getNotes() {
		return notes;
	}

	public void setNotes(List<Note> notes) {
		this.notes = notes;
	}
	
	
}
