package model;

import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;
import model.Note;

public class Trip {
    private String name;
    private int state;
    private Map<String, Place> places;
    private List<Note> comments;
    
    public Trip() {
    	
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public Map<String, Place> getPlaces() {
		return places;
	}

	public void setPlaces(Map<String, Place> places) {
		this.places = places;
	}

	public List<Note> getComments() {
		return comments;
	}

	public void setComments(List<Note> comments) {
		this.comments = comments;
	}

    
}