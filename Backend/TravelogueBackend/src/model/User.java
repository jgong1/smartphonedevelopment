package model;

import java.util.ArrayList;
import java.util.List;

public class User {
    private String username;
    private String profilePicture;
    private List<Trip> trips;
    private List<String> friends;

    /**
     * @param username The username of this user.
     */
    public User(String username) {
    	this.username = username;
    	this.trips = new ArrayList<Trip>();
    	this.friends = new ArrayList<String>();
    }
    
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getProfilePicture() {
		return profilePicture;
	}

	public void setProfilePicture(String profilePicture) {
		this.profilePicture = profilePicture;
	}

	public List<Trip> getTrips() {
		return trips;
	}

	public void setTrips(List<Trip> trips) {
		this.trips = trips;
	}

	public List<String> getFriends() {
		return friends;
	}

	public void setFriends(List<String> friends) {
		this.friends = friends;
	}
}