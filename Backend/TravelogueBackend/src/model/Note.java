package model;


/**
 * Note object used to hold user notes and friend comments
 */
public class Note {
    private String author;
    private String content;

    /**
     * @param author The username of the author
     * @param content The note/comment content
     */
    public Note(String author, String content) {
        this.author = author;
        this.content = content;
    }

    /**
     * @return The username of the author
     */
    public String getAuthor() {
        return this.author;
    }

    /**
     * @param author The user name of the author
     */
    public void setAuthor(String author) {
        this.author = author;
    }

    /**
     * @return The contents of the Note
     */
    public String getContent() {
        return this.content;
    }

    /**
     * @param content The new content of the note
     */
    public void setContent(String content) {
        this.content = content;
    }
}