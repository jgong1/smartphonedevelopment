package database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Note;

public class CommentDBAction extends AbstractDBAction {
	
	public CommentDBAction() {
		super();
	}
	
	/**
	 * Get a list of comments for a given tripID 
	 */
	public List<Note> getComments(int tripID) {
		establishConnection();
		StringBuilder sb = new StringBuilder();
		sb.append(props.getProperty("get_comments"));
		sb.append(tripID);
		ResultSet rs = executeQuery(sb.toString());
		List<Note> comments = new ArrayList<Note>();
		try {
			while (rs.next()) {
				String author = rs.getString("author");
				String content = rs.getString("content");
				comments.add(new Note(author, content));
			}
		} catch (SQLException e) {
			System.out.println("Error getting comments.");
			e.printStackTrace();
		}
		closeConnection();
		return comments;
	}
	
	/**
	 * Create a new entry in the database for a comment.
	 */
	public void addComment(String author, String content, 
			String tripName, String username) {
		establishConnection();
		int tripID = new TripDBAction().getTripID(username, tripName);
		StringBuilder sb = new StringBuilder();
		sb.append(props.getProperty("create_comment"));
		sb.append(content);
		sb.append("', '");
		sb.append(author);
		sb.append("', ");
		sb.append(tripID);
		executeUpdate(sb.toString());
		closeConnection();
	}

}
