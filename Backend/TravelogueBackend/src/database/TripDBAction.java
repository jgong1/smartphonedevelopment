package database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import model.User;
import model.Note;
import model.Place;
import model.Trip;

public class TripDBAction extends AbstractDBAction {
	
	public TripDBAction() {
		super();
	}
	
	/**
	 * Get a list of trips for a specified user.
	 * @param ownerID
	 * @return
	 */
	public List<Trip> getTrips(int ownerID) {
		establishConnection();
		StringBuilder sb = new StringBuilder();
		sb.append(props.getProperty("get_trips_of_user"));
		sb.append(ownerID);
		ResultSet rs = executeQuery(sb.toString());
		List<Trip> trips = new ArrayList<Trip>();
		try {
			while (rs.next()) {
				Trip trip = new Trip();
				String tripName = rs.getString("tripName");
				int state = rs.getInt("state");
				int tripID = rs.getInt("id");
				List<Note> comments = new CommentDBAction().getComments(tripID);
				Map<String, Place> places = new PlaceDBAction().getPlacesForTrip(tripID);
				trip.setName(tripName);
				trip.setState(state);
				trip.setComments(comments);
				trip.setPlaces(places);
			}
		} catch (Exception e) {
			System.out.println("Error getting trips");
			e.printStackTrace();
		}
		closeConnection();
		return trips;
	}
	
	/**
	 * Get a map of all tripID -> tripName for a specified user.
	 */
	public Map<Integer, String> getTripsMap(int ownerID) {
		establishConnection();
		StringBuilder sb = new StringBuilder();
		sb.append(props.getProperty("get_trips_of_user"));
		sb.append(ownerID);
		sb.append(" ORDER BY id ASC");
		ResultSet rs = executeQuery(sb.toString());
		Map<Integer, String> trips = new LinkedHashMap<Integer, String>();
		try {
			while (rs.next()) {
				trips.put(rs.getInt("id"), rs.getString("tripName"));
			}
		} catch (Exception e) {
			System.out.println("Error getting trips");
			e.printStackTrace();
		}
		closeConnection();
		return trips;
	}
	
	/**
	 * Create a new trip.
	 */
	public void newTrip(String username, Trip trip) {
		establishConnection();
		StringBuilder sb = new StringBuilder();
		int ownerID = new UserDBAction().getUserID(username);
		sb.append(props.getProperty("create_trip"));
		sb.append(username);
		sb.append("', ");
		sb.append(trip.getState());
		sb.append(", ");
		sb.append(ownerID);
		sb.append(")");
		executeUpdate(sb.toString());
		closeConnection();
	}
	
	/**
	 * Given a username and a trip name, get the unique tripID.
	 */
	public int getTripID(String username, String tripName) {
		establishConnection();
		int tripID = -1;
		StringBuilder sb = new StringBuilder();
		int ownerID = new UserDBAction().getUserID(username);
		sb.append(props.getProperty("get_trip_id_1"));
		sb.append(ownerID);
		sb.append(props.getProperty("get_trip_id_2"));
		sb.append(tripName);
		sb.append("' LIMIT 1");
		ResultSet rs = executeQuery(sb.toString());
		try {
			while (rs.next()) {
				tripID = rs.getInt("id");
			}
		} catch (SQLException e) {
			System.out.println("Error getting tripID");
			e.printStackTrace();
		}
		closeConnection();
		return tripID;
	}
	
}
