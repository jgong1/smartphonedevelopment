package database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class FriendDBAction extends AbstractDBAction {
	public FriendDBAction() {
		super();
	}
	
	/**
	 * Get a list of friends for the specified user.
	 */
	public List<String> getFriends(int ownerID) {
		establishConnection();
		StringBuilder sb = new StringBuilder();
		sb.append(props.getProperty("get_friends"));
		sb.append(ownerID);
		List<String> friends = new ArrayList<String>();
		ResultSet rs = executeQuery(sb.toString());
		try {
			while (rs.next()) {
				String username = rs.getString("username");
				friends.add(username);
			}
		} catch (SQLException e) {
			System.out.println("Error getting friends");
			e.printStackTrace();
		}
		closeConnection();
		return friends;
	}

	/**
	 * Add a new one-way friendship.
	 */
	public void addFriend(int ownerID, String friend) {
		establishConnection();
		StringBuilder sb = new StringBuilder();
		sb.append(props.getProperty("create_friend"));
		sb.append(friend);
		sb.append("', ");
		sb.append(ownerID);
		sb.append(")");
		executeUpdate(sb.toString());
		closeConnection();
	}
}
