package database;

/**
 * Initialize the database by creating the tables.
 * @author elliotttan
 *
 */
public class InitDBAction extends AbstractDBAction {
	public InitDBAction() {
		super();
	}
	
	public void initTables() {
		establishConnection();
		executeUpdate(props.getProperty("create_user_table"));
		executeUpdate(props.getProperty("create_friend_table"));
		executeUpdate(props.getProperty("create_trip_table"));
		executeUpdate(props.getProperty("create_place_table"));
		executeUpdate(props.getProperty("create_comment_table"));
		executeUpdate(props.getProperty("create_note_table"));
		executeUpdate(props.getProperty("create_photo_table"));
		closeConnection();
	}
}
