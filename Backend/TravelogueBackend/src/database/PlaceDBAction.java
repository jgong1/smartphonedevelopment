package database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import model.Note;
import model.Photo;
import model.Place;

public class PlaceDBAction extends AbstractDBAction {
	
	public PlaceDBAction() {
		super();
	}

	/**
	 * Get the map of places associated with a trip. 
	 */
	public Map<String, Place> getPlacesForTrip(int tripID) {
		establishConnection();
		StringBuilder sb = new StringBuilder();
		sb.append(props.get("get_places_for_trip"));
		sb.append(tripID);
		ResultSet rs = executeQuery(sb.toString());
		Map<String, Place> places = new HashMap<String, Place>();
		try {
			while (rs.next()) {
				double latitude = rs.getDouble("latitude");
				double longitude = rs.getDouble("longitude");
				Place place = new Place(latitude, longitude);
				int placeID = rs.getInt("id");
				NoteDBAction noteAction = new NoteDBAction();
				List<Note> notes = noteAction.getNotes(placeID);
				place.setNotes(notes);
				PhotoDBAction photoAction = new PhotoDBAction();
				List<Photo> photos = photoAction.getPhotos(placeID);
				place.setPhotos(photos);
				String latLng = Double.toString(latitude) + "," 
						+ Double.toString(longitude);
				places.put(latLng, place);
			}
		} catch (SQLException e) {
			System.out.println("Error getting places");
			e.printStackTrace();
		}
		closeConnection();
		return places;
	}
	
	
	
}
