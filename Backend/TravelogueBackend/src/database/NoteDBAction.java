package database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Note;
import model.User;

public class NoteDBAction extends AbstractDBAction {
	public NoteDBAction() {
		super();
	}
	
	/* Create a new Note object in the database */
	public void createNote(Note note) {
		establishConnection();
		StringBuilder sb = new StringBuilder();
		sb.append(props.get("create_note"));
		sb.append(note.getAuthor());
		sb.append("', '");
		sb.append(note.getContent());
		sb.append("')");
		executeUpdate(sb.toString());
		closeConnection();
	}
	
	/**
	 * Get a list of Notes for a specified placeID.
	 */
	public List<Note> getNotes(int placeID) {
		establishConnection();
		StringBuilder sb = new StringBuilder();
		sb.append(props.getProperty("notes"));
		sb.append(placeID);
		ResultSet rs = executeQuery(sb.toString());
		List<Note> notes = new ArrayList<Note>();
		try {
			while (rs.next()) {
				String author = rs.getString("author");
				String content = rs.getString("content");
				notes.add(new Note(author, content));
			}
		} catch (SQLException e) {
			System.out.println("Error getting comments.");
			e.printStackTrace();
		}
		closeConnection();
		return notes;
	}
}
