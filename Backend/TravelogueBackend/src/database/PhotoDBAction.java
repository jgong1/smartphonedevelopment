package database;

import java.sql.Blob;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.tomcat.util.codec.binary.Base64;

import model.Photo;

public class PhotoDBAction extends AbstractDBAction {
	public PhotoDBAction() {
		super();
	}
	
	/**
	 * Get a list of photos for a specified placeID.
	 */
	public List<Photo> getPhotos(int placeID) {
		establishConnection();
		StringBuilder sb = new StringBuilder();
		sb.append(props.getProperty("get_photos"));
		sb.append(placeID);
		ResultSet rs = executeQuery(sb.toString());
		List<Photo> photos = new ArrayList<Photo>();
		try {
			while (rs.next()) {
				Blob blob = rs.getBlob("photo");
				byte[] photo = blob.getBytes(1, (int)blob.length());
				photos.add(new Photo(Base64.encodeBase64String(photo)));
			}
		} catch (SQLException e) {
			System.out.println("Error getting photos");
			e.printStackTrace();
		}
		closeConnection();
		return photos;
	}
}
