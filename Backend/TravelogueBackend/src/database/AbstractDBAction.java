package database;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public abstract class AbstractDBAction {

	private static final String driver = "org.sqlite.JDBC";
	private static final String url = "jdbc:sqlite:/home/ec2-user/sqlite.db";
	
	protected Connection connection;
	protected Properties props;
	
	/* Loads the properties file containaing the SQL statments. */
	public AbstractDBAction() {
		try {
			Class.forName(driver);
		} catch (ClassNotFoundException e) {
			System.err.println("Cannot find database driver class.");
			System.err.println(e);
		}
		props = new Properties();
		FileInputStream in = null;
		try {
			in = new FileInputStream("/home/ec2-user/sql.properties");
			props.load(in);
			in.close();
		} catch (FileNotFoundException e) {
			System.err.println("sql.properties file not found.");
		} catch (IOException e) {
			System.err.println("Caught IOException: " + e.getStackTrace());
		}
	}
	
	/* Establishes a connection with the specified MySQL database. */
	public void establishConnection() {
		try {
			connection = DriverManager.getConnection(url);
		} catch (SQLException e) {
            System.err.println("Cannot connect to this database.");
            System.err.println(e);
        }
	}
	
	/* Executes a SQL query from the given String */
	public ResultSet executeQuery(String query) {
		System.out.println(query);
		ResultSet result = null;
		try {
			Statement statement = connection.createStatement();
			result = statement.executeQuery(query);
		} catch (SQLException e) {
			System.err.println("Error executing SQL query.");
			System.err.println(e);
		}
		return result;
	}
	
	/* Executes a SQL update from the given String */
	public void executeUpdate(String query) {
		System.out.println(query);
		try {
			Statement statement = connection.createStatement();
			statement.executeUpdate(query);
		} catch (SQLException e) {
			System.err.println("Error executing SQL update.");
			System.err.println(e);
		}
	}
	
	/* Closes the connection to the database. */
	public void closeConnection() {
		try {
			connection.close();
		} catch (SQLException e) {
			System.err.println("Error closing database connection.");
			System.err.println(e);
		}
	}
}
