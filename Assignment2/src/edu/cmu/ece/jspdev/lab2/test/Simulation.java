package edu.cmu.ece.jspdev.lab2.test;

import edu.cmu.ece.jspdev.lab2.main.exception.EmptyFileException;
import edu.cmu.ece.jspdev.lab2.main.exception.FileOutOfSizeException;
import edu.cmu.ece.jspdev.lab2.main.model.Statistics;
import edu.cmu.ece.jspdev.lab2.main.model.Student;
import edu.cmu.ece.jspdev.lab2.main.util.Util;

public class Simulation {
	public static void main(String[] args) {

		final int NUMBER_STUDENTS = 40;
		final String fileName1 = "./src/data_test1.txt";
		final String fileName2 = "./src/data_test2.txt";
		final String fileName3 = "./src/data_test3.txt";
		final String fileName4 = "./src/data_test4.txt";
		

		Student[] students = new Student[NUMBER_STUDENTS];
		Util util = new Util();
		Statistics stat = new Statistics();
		
		/*
		 * Test case 1, number of students is 40
		 */
		System.out.println();
		System.out.println("-------------------");
		System.out.println("-------------------");
		System.out.println("Test case 1: number of students is 40");
		try {
			students = util.readFile(fileName1, students);
		} catch (FileOutOfSizeException e) {
			System.out
					.println("Exception catched, there are more than 40 students in this file.");
			e.fixError();
		} catch (EmptyFileException f) {
			System.out.println("Exception catched, empty file.");
			f.fixError();
			System.exit(1);
		}

		util.printGrades(students);

		stat.findlow(students);
		stat.findhigh(students);
		stat.findavg(students);

		stat.printLowScores();
		stat.printHighScores();
		stat.printAvgScores();
		
		/*
		 * Test case 2, number of students is less than 40
		 */
		System.out.println();
		System.out.println("-------------------");
		System.out.println("-------------------");
		System.out.println("Test case 2, number of students is less than 40");
		try {
			students = util.readFile(fileName2, students);
		} catch (FileOutOfSizeException e) {
			System.out
					.println("Exception catched, there are more than 40 students in this file.");
			e.fixError();
		} catch (EmptyFileException f) {
			System.out.println("Exception catched, empty file.");
			f.fixError();
			System.exit(1);
		}

		util.printGrades(students);

		stat.findlow(students);
		stat.findhigh(students);
		stat.findavg(students);

		stat.printLowScores();
		stat.printHighScores();
		stat.printAvgScores();
		
		/*
		 * Test case 3, number of students is more than 40
		 */
		System.out.println();
		System.out.println("-------------------");
		System.out.println("-------------------");
		System.out.println("Test case 3, number of students is more than 40");
		try {
			students = util.readFile(fileName3, students);
		} catch (FileOutOfSizeException e) {
			System.out
					.println("Exception catched, there are more than 40 students in this file.");
			e.fixError();
		} catch (EmptyFileException f) {
			System.out.println("Exception catched, empty file.");
			f.fixError();
			System.exit(1);
		}

		util.printGrades(students);

		stat.findlow(students);
		stat.findhigh(students);
		stat.findavg(students);

		stat.printLowScores();
		stat.printHighScores();
		stat.printAvgScores();
		
		/*
		 * Test case 4, number of students is 0
		 */
		System.out.println();		
		System.out.println("-------------------");
		System.out.println("-------------------");
		System.out.println("Test case 4: number of student is 0");
		try {
			students = util.readFile(fileName4, students);
		} catch (FileOutOfSizeException e) {
			System.out
					.println("Exception catched, there are more than 40 students in this file.");
			e.fixError();
		} catch (EmptyFileException f) {
			System.out.println("Exception catched, empty file.");
			f.fixError();
			System.exit(1);
		}

		util.printGrades(students);

		stat.findlow(students);
		stat.findhigh(students);
		stat.findavg(students);

		stat.printLowScores();
		stat.printHighScores();
		stat.printAvgScores();

	}
}
