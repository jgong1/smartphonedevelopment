package edu.cmu.ece.jspdev.lab2.main.model;


public class Student {
	private static final int NUMBER_QUIZES = 5;
	
	private int SID;
	private int[] scores = new int[NUMBER_QUIZES];
	
	public int getSID () {
		return this.SID;
	}
	
	public int[] getScores () {
		return this.scores;
	}
	
	public void setSID (int newSID) {
		if (newSID < 1000 || newSID > 9999) {
			System.out.println("Error: Student ID out of range.");
			return;
		}
		this.SID = newSID;
	}
	
	public void setScores (int[] newScores) {
		if (newScores.length != NUMBER_QUIZES) {
			System.out.println("Error: Number of scores out of range");
			return;
		}
		this.scores = newScores;
	}
	
	public void printSID () {
		System.out.println("Student ID is " + this.SID);
	}
	
	public void printScores () {
		System.out.println("Scores of students of the five quizes:");
		for (int i = 0; i < NUMBER_QUIZES; i ++) {
			String temp = "Quiz " + String.valueOf(i + 1) + ": ";
			temp += String.valueOf(this.scores[i]);
			System.out.println(temp);
		}
	}
	
	public void printStudent () {
		printSID();
		printScores();
	}
}
