package edu.cmu.ece.jspdev.lab2.main.util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import edu.cmu.ece.jspdev.lab2.main.exception.EmptyFileException;
import edu.cmu.ece.jspdev.lab2.main.exception.FileOutOfSizeException;
import edu.cmu.ece.jspdev.lab2.main.model.Student;

public class Util implements UtilInterface{
	private static final int NUMBER_QUIZES = 5;
	private static final int NUMBER_STUDENTS = 40;
	private int count = 0;
	
	public Student[] readFile(String fileName, Student[] stu) throws FileOutOfSizeException, EmptyFileException {
		FileReader file = null;
		BufferedReader br = null;
		try {
			file = new FileReader(fileName);
			br = new BufferedReader(file);		
			br.readLine();
			String line = "";
			count = 0;				
			while ((line = br.readLine()) != null) {
				if (count >= NUMBER_STUDENTS) {
					throw new FileOutOfSizeException("Error: There are more than 40 students in file.");
				}
				String[] elements = line.split(" ");
				Student studentTemp = new Student();						 											
				int[] scores = new int[NUMBER_QUIZES];
				for (int i = 0; i < NUMBER_QUIZES + 1; i ++) {					
					int temp = Integer.parseInt(elements[i]);
					if (i == 0) {
						studentTemp.setSID(temp);
					} else {
						scores[i - 1] = temp;
					}												
				}
				studentTemp.setScores(scores);
				stu[count] = studentTemp;					
				count ++;
				
			}
			if (count == 0) {
				throw new EmptyFileException("Error: This is an empty file!");
			}
		} catch (FileNotFoundException e) {
			System.out.print("FileNotFoundException: File not found.");
			System.exit(1);
		} catch (IOException e) {
			System.out.println("IOException: Cannot read file.");
			System.exit(1);
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				System.out.println("IOException: Cannot close file.");
				System.exit(1);
			}
		}
		return stu;
	}
		
	public void printGrades (Student[] stu) {
		System.out.println();
		System.out.println("Stu Qu1 Qu2 Qu3 Qu4 Qu5");
		int length = Math.min(count, NUMBER_STUDENTS);
		for (int i = 0; i < length; i ++) {
			StringBuilder sb = new StringBuilder();
			int sID = stu[i].getSID();
			sb.append(sID);
			int[] scores = stu[i].getScores();
			for (int scoreTemp : scores) {
				sb.append(" ");
				sb.append(scoreTemp);
			}
			System.out.println(sb.toString());
		}
	}
}
