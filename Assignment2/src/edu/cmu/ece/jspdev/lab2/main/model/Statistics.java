package edu.cmu.ece.jspdev.lab2.main.model;


public class Statistics extends AbstractStatics {
	private static final int NUMBER_QUIZES = 5;
	
	private int[] lowscores = new int[NUMBER_QUIZES];
	private int[] highscores = new int[NUMBER_QUIZES];
	private float[] avgscores = new float[NUMBER_QUIZES];
	
	@Override
	public void findlow(Student[] a) {
		if (a == null || a.length == 0) {
			System.out.println("Error: There should be more than one student!");
			return;
		}
		for (int i = 0; i < NUMBER_QUIZES; i ++) {
			int lowscore = 100;
			for (int j = 0; j < a.length; j ++) {
				if (a[j] != null) {
					int[] scoresTemp = a[j].getScores();
					int scoreTemp = scoresTemp[i];
					lowscore = Math.min(scoreTemp, lowscore);
				}				
			}
			this.lowscores[i] = lowscore;
		}
		return;
	}
	
	@Override
	public void findhigh(Student[] a) {
		if (a == null || a.length == 0) {
			System.out.println("Error: There should be more than one student!");
			return;
		}
		for (int i = 0; i < NUMBER_QUIZES; i ++) {
			int highscore = 0;
			for (int j = 0; j < a.length; j ++) {
				if (a[j] != null) {
					int[] scoresTemp = a[j].getScores();
					int scoreTemp = scoresTemp[i];
					highscore = Math.max(scoreTemp, highscore);
				}				
			}
			this.highscores[i] = highscore;
		}
		return;
	}
	
	@Override
	public void findavg(Student[] a) {
		if (a == null || a.length == 0) {
			System.out.println("Error: There should be more than one student!");
			return;
		}	
		int numberStudents = 0;
		for (int i = 0; i < NUMBER_QUIZES; i ++) {
			
			int sumTemp = 0;
			for (int j = 0; j < a.length; j ++) {
				if (a[j] != null) {
					int[] scoresTemp = a[j].getScores();
					int scoreTemp = scoresTemp[i];
					sumTemp += scoreTemp;
					if (i == 0) {
						numberStudents ++;
					}					
				}				
			}
			this.avgscores[i] = (float)sumTemp / (float)numberStudents;
		}
		return;
	}
	
	@Override
	public void printLowScores() {
		System.out.println();
		System.out.println("Lowest scores for five quizes are:");
		for (int i = 0; i < NUMBER_QUIZES; i ++) {
			String temp = "Quiz " + String.valueOf(i + 1) + ": ";
			temp += String.valueOf(this.lowscores[i]);
			System.out.println(temp);
		}
	}
	
	@Override
	public void printHighScores() {
		System.out.println();
		System.out.println("Highest scores for five quizes are:");
		for (int i = 0; i < NUMBER_QUIZES; i ++) {
			String temp = "Quiz " + String.valueOf(i + 1) + ": ";
			temp += String.valueOf(this.highscores[i]);
			System.out.println(temp);
		}
	}
	
	@Override
	public void printAvgScores() {
		System.out.println();
		System.out.println("Average scores for five quizes are:");
		for (int i = 0; i < NUMBER_QUIZES; i ++) {
			String temp = "Quiz " + String.valueOf(i + 1) + ": ";
			temp += String.valueOf(this.avgscores[i]);
			System.out.println(temp);
		}
	}
	
}
