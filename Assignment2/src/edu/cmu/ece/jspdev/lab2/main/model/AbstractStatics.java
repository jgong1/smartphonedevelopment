package edu.cmu.ece.jspdev.lab2.main.model;

public abstract class AbstractStatics {	
	
	public abstract void findlow(Student[] a);
	
	public abstract void findhigh(Student[] a);
	
	public abstract void findavg(Student[] a);
	
	public abstract void printLowScores();
	
	public abstract void printHighScores();
	
	public abstract void printAvgScores();
	
}
