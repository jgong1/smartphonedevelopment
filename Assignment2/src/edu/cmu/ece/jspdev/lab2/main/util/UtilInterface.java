package edu.cmu.ece.jspdev.lab2.main.util;

import edu.cmu.ece.jspdev.lab2.main.exception.EmptyFileException;
import edu.cmu.ece.jspdev.lab2.main.exception.FileOutOfSizeException;
import edu.cmu.ece.jspdev.lab2.main.model.Student;

public interface UtilInterface {
	
	public Student[] readFile(String fileName, Student[] student) throws FileOutOfSizeException, EmptyFileException;
	
	public void printGrades(Student[] stu);		
}
