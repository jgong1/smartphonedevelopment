package edu.cmu.ece.jspdev.lab2.main.exception;

public class FileOutOfSizeException extends Exception {
	
	private static final long serialVersionUID = 1L;
	private int errorNo;
	private String errorMSG;
	
	public FileOutOfSizeException(int errorNo) {
		super();
		this.errorNo = errorNo;
		printError();
	}
	
	public FileOutOfSizeException(String errorMSG) {
		super();
		this.errorMSG = errorMSG;
		printError();
	}
	
	public FileOutOfSizeException(int errorNo, String errorMSG) {
		super();
		this.errorNo = errorNo;
		this.errorMSG = errorMSG;
		printError();
	}
	
	public int getErrorNo() {
		return this.errorNo;
	}
	
	public String getErrorMSG() {
		return this.errorMSG;
	}
	
	public void setErrorNo(int newErrorNo) {
		this.errorNo = newErrorNo;
	}
	
	public void setErrorMSG(String newErrorMSG) {
		this.errorMSG = newErrorMSG;
	}
	
	public void printError() {
		System.out.println("FileOutOfSizeException: Error Message=" + this.errorMSG);
	}
	
	public void fixError() {
		System.out.println("Only the first 40 students records are recorded.");
	}
	
}
