package edu.cmu.ece.jspdev.lab2.main.exception;

public class EmptyFileException extends Exception{
	private static final long serialVersionUID = 1L;
	private int errorNo;
	private String errorMSG;
	
	public EmptyFileException(int errorNo) {
		super();
		this.errorNo = errorNo;
		printError();
	}
	
	public EmptyFileException(String errorMSG) {
		super();
		this.errorMSG = errorMSG;
		printError();
	}
	
	public EmptyFileException(int errorNo, String errorMSG) {
		super();
		this.errorNo = errorNo;
		this.errorMSG = errorMSG;
		printError();
	}
	
	public int getErrorNo() {
		return this.errorNo;
	}
	
	public String getErrorMSG() {
		return this.errorMSG;
	}
	
	public void setErrorNo(int newErrorNo) {
		this.errorNo = newErrorNo;
	}
	
	public void setErrorMSG(String newErrorMSG) {
		this.errorMSG = newErrorMSG;
	}
	
	public void printError() {
		System.out.println("EmptyFileException: Error Message=" + this.errorMSG);
	}
	
	public void fixError() {
		System.out.println("Please try another file.");
	}
}
