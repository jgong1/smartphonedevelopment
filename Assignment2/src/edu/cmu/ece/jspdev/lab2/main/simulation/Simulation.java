package edu.cmu.ece.jspdev.lab2.main.simulation;

import edu.cmu.ece.jspdev.lab2.main.exception.EmptyFileException;
import edu.cmu.ece.jspdev.lab2.main.exception.FileOutOfSizeException;
import edu.cmu.ece.jspdev.lab2.main.model.Statistics;
import edu.cmu.ece.jspdev.lab2.main.model.Student;
import edu.cmu.ece.jspdev.lab2.main.util.Util;

public class Simulation {
	public static void main(String[] args) {
		
		final int NUMBER_STUDENTS = 40;		
		final String fileName = "./src/data.txt";
		
		Student[] students = new Student[NUMBER_STUDENTS];
		Util util = new Util();
		
		try {
			students = util.readFile(fileName, students);
		} catch (FileOutOfSizeException e) {
			System.out.println("Exception catched, there are more than 40 students in this file.");
			e.fixError();
		} catch (EmptyFileException f) {
			System.out.println("Exception catched, empty file.");
			f.fixError();
			System.exit(1);
		}
		
		util.printGrades(students);
		
		Statistics stat = new Statistics();
		stat.findlow(students);
		stat.findhigh(students);
		stat.findavg(students);
		
		stat.printLowScores();
		stat.printHighScores();
		stat.printAvgScores();
		
	}
}
