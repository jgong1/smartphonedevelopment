package com.travelogue.travelogue;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.travelogue.travelogue.DBLayout.LocalDatabaseConnector;

import java.util.ArrayList;

public class Friends extends Activity {
    private Button searchButton;
    private EditText searchET;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friends);

        searchButton = (Button) findViewById(R.id.search_button);
        searchButton.setOnClickListener(searchButtonClicked);

        searchET = (EditText) findViewById(R.id.search_bar);

        if (findViewById(R.id.fragment) != null) {
            // Get the friends list from the database
            LocalDatabaseConnector db = new LocalDatabaseConnector(Friends.this);
            TravelogueApp appState = (TravelogueApp) getApplicationContext();
            ArrayList<String> friends = db.getAllFriends(appState.getCurrentUser());

            // Add the friends list to the fragment
            FriendSearchFragment newFragment = new FriendSearchFragment();
            Bundle args = new Bundle();
            args.putStringArrayList(newFragment.getParam1Name(), friends);
            newFragment.setArguments(args);

            // Add the fragment to the placeholder
            FragmentManager manager = getFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.add(R.id.fragment, newFragment);
            transaction.commit();
        }
    }

    View.OnClickListener searchButtonClicked = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String searchVal = searchET.getText().toString();
            if (searchVal.isEmpty()) {
                return;
            } else {
                // TODO Get a list of users and get all the users that contain the query
                LocalDatabaseConnector db = new LocalDatabaseConnector(Friends.this);
                ArrayList<String> users = db.getAllUsers();
                ArrayList<String> results = matchingResults(users, searchVal);

                // Set up the array list of matches as an argument to the fragment
                FriendSearchFragment newFragment = new FriendSearchFragment();
                Bundle args = new Bundle();
                args.putStringArrayList(newFragment.getParam1Name(), results);
                newFragment.setArguments(args);

                // Replace current fragment with new one
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment, newFragment);
                transaction.commit();
            }
        }
    };

    private ArrayList<String> matchingResults(ArrayList<String> users, String query) {
        TravelogueApp appState = (TravelogueApp) getApplicationContext();
        String myName = appState.getCurrentUser();
        ArrayList<String> matches = new ArrayList<>();
        for (String s : users) {
            if (s.toLowerCase().contains(query.toLowerCase())) {
                matches.add(s);
            }
        }
        matches.remove(myName);
        return matches;
    }
}