package com.travelogue.travelogue;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.travelogue.travelogue.DBLayout.LocalDatabaseConnector;
import com.travelogue.travelogue.entities.Note;
import com.travelogue.travelogue.entities.Place;
import com.travelogue.travelogue.entities.Trip;
import com.travelogue.travelogue.exceptions.EntityException;

import java.util.ArrayList;

public class ViewPlaceContent extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_content);

        // Get the username and the trip name
        Intent intent = getIntent();
        String username = intent.getStringExtra("username");
        String tripName = intent.getStringExtra("tripName");
        double latitude = intent.getDoubleExtra("latitude", 0);
        double longitude = intent.getDoubleExtra("longitude", 0);

        // TODO query remote DB again
        LocalDatabaseConnector db = new LocalDatabaseConnector(ViewPlaceContent.this);
        Trip t = db.getTrip(tripName, username);
        Place p = t.getPlace(new LatLng(latitude, longitude));
        ArrayList<Bitmap> bmp = p.getAllPhotos();
        ArrayList<String> notes = new ArrayList<>();
        for (Note n : p.getAllNotes()) {
            notes.add(n.getContent());
        }

        if (findViewById(R.id.fragment1) != null) {
            ViewPlacePhotosFragment newFragment = new ViewPlacePhotosFragment();
            Bundle args = new Bundle();
            args.putParcelableArrayList(newFragment.getParam1Name(), bmp);
            newFragment.setArguments(args);

            // Add the images fragment to the placeholder
            FragmentManager manager = getFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.add(R.id.fragment1, newFragment);
            transaction.commit();
        }

        if (findViewById(R.id.fragment2) != null) {
            ViewPlaceNotesFragment newFragment = new ViewPlaceNotesFragment();
            Bundle args = new Bundle();
            args.putStringArrayList(newFragment.getParam1Name(), notes);
            newFragment.setArguments(args);

            // Add the notes fragment to the placeholder
            FragmentManager manager = getFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.add(R.id.fragment2, newFragment);
            transaction.commit();
        }
    }
}