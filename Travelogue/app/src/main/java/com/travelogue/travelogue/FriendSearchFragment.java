package com.travelogue.travelogue;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;

import com.travelogue.travelogue.DBLayout.LocalDatabaseConnector;
import com.travelogue.travelogue.ui.DisplayUtil;

import java.util.ArrayList;

public class FriendSearchFragment extends Fragment implements AbsListView.OnItemClickListener {
    private static final String ARG_PARAM1 = "Items Array";
    private AbsListView mListView;
    private ListAdapter mAdapter;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public FriendSearchFragment() {
    }

    public static FriendSearchFragment newInstance(ArrayList<String> param1) {
        FriendSearchFragment fragment = new FriendSearchFragment();
        Bundle args = new Bundle();
        args.putStringArrayList(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    public String getParam1Name() {
        return ARG_PARAM1;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ArrayList<String> mItems;
        if (getArguments() != null) {
            mItems = getArguments().getStringArrayList(ARG_PARAM1);
        } else {
            return;
        }

        mAdapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_list_item_1, android.R.id.text1, mItems);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_friends, container, false);

        // Set the adapter
        mListView = (AbsListView) view.findViewById(android.R.id.list);
        mListView.setAdapter(mAdapter);

        // Set OnItemClickListener so we can be notified on item clicks
        mListView.setOnItemClickListener(this);

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        TravelogueApp app = (TravelogueApp) getActivity().getApplicationContext();
        final LocalDatabaseConnector db = new LocalDatabaseConnector(getActivity().getApplicationContext());
        final String username = app.getCurrentUser();
        final String friend = (String) mAdapter.getItem(position);

        if (!db.isFriend(friend, username)) {
            DisplayUtil.showYesNoAlertDialog(getActivity(), R.string.not_friend,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            db.insertFriend(username, friend);
                            Intent myIntent = new Intent(getActivity(), OthersMain.class);
                            myIntent.putExtra("username", friend);
                            getActivity().startActivity(myIntent);
                        }
                    },
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
        } else {
            Intent myIntent = new Intent(getActivity(), OthersMain.class);
            myIntent.putExtra("username", (String) mAdapter.getItem(position));
            getActivity().startActivity(myIntent);
        }
    }
}
