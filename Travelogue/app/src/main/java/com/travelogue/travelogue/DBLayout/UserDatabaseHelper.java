package com.travelogue.travelogue.DBLayout;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;

import java.util.ArrayList;

import com.travelogue.travelogue.entities.Trip;
import com.travelogue.travelogue.entities.User;
import com.travelogue.travelogue.utils.BitmapUtil;

/**
 * Class for working with user objects in the database.
 */
public class UserDatabaseHelper implements DatabaseVariables {
    /* Private field */
    private SQLiteDatabase database; // open, writable database object

    /* Public constructor */
    public UserDatabaseHelper(SQLiteDatabase database) {
        this.database = database;
    }

    /* Create a new user item in the database */
    public long insertUser(String username, Bitmap newPic) {
        if (getUserID(username.toLowerCase()) != ERROR_ID) {
            return ERROR_ID;
        }

        ContentValues newUser = new ContentValues();
        newUser.put(COLUMN_NAME_USERNAME, username);
        newUser.put(COLUMN_NAME_PROFILE_PIC, BitmapUtil.getBitmapAsByteArray(newPic));
        long id = database.insert(TABLE_NAME_USER, null, newUser);

        if (id == -1) {
            return ERROR_ID;
        } else {
            return id;
        }
    }

    /* Read a user's table ID from the database */
    public long getUserID(String username) {
        Cursor c = database.query(TABLE_NAME_USER,
                null, COLUMN_NAME_USERNAME + "='" + username + "'", null, null, null, null);

        if ((c != null) && (c.getCount() > 0)) {
            c.moveToFirst();
            long id = c.getLong(c.getColumnIndexOrThrow(COLUMN_NAME_ID));
            c.close();
            return id;
        } else {
            return ERROR_ID;
        }
    }

    /* Get a list of all user from the database contents */
    public ArrayList<String> getAllUsers() {
        Cursor c = database.query(TABLE_NAME_USER,
                null, "1=1", null, null, null, null);

        ArrayList<String> allUsers = new ArrayList<>();
        if ((c != null) && (c.getCount() > 0)) {
            c.moveToFirst();
            do {
                String name = c.getString(c.getColumnIndexOrThrow(COLUMN_NAME_USERNAME));
                allUsers.add(name);
            } while (c.moveToNext());
            c.close();
        }
        return allUsers;
    }

    /* Create a user from the database contents */
    public User getUser(String username) {
        Cursor c = database.query(TABLE_NAME_USER,
                null, COLUMN_NAME_USERNAME + "='" + username + "'", null, null, null, null);

        if ((c != null) && (c.getCount() > 0)) {
            // Read the columns of interest
            c.moveToFirst();
            long id = c.getLong(c.getColumnIndexOrThrow(COLUMN_NAME_ID));
            byte[] profilePic = c.getBlob(c.getColumnIndexOrThrow(COLUMN_NAME_PROFILE_PIC));
            c.close();

            // Get an array list of all the trips
            TripDatabaseHelper t = new TripDatabaseHelper(database);
            ArrayList<Trip> tripsOld = t.getAllTrips(0, id);
            ArrayList<Trip> tripsNew = t.getAllTrips(1, id);
            ArrayList<Trip> trips = new ArrayList<>();
            trips.addAll(tripsNew);
            trips.addAll(tripsOld);

            // Create an array list of all the user's friend's names
            FriendDatabaseHelper friendDB = new FriendDatabaseHelper(database);
            ArrayList<String> friends = friendDB.getAllFriends(id);

            // Create a User object and return it
            User u = new User(username);
            u.setProfilePicture(BitmapUtil.getByteArrayAsBitmap(profilePic));
            u.setAllFriends(friends);
            u.setAllTrips(trips);
            return u;
        } else {
            // User does not exist
            return null;
        }
    }

    /* Method for deleting user from database */
    public void deleteUser(String username) {
        database.delete(TABLE_NAME_USER, COLUMN_NAME_USERNAME + "='" + username + "'", null);
    }
}
