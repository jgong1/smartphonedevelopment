package com.travelogue.travelogue;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.travelogue.travelogue.DBLayout.LocalDatabaseConnector;
import com.travelogue.travelogue.ui.DisplayUtil;
import com.travelogue.travelogue.utils.HttpHelper;
import com.travelogue.travelogue.ws.remote.RemoteCallsImpl;

import org.json.JSONException;
import org.json.JSONObject;

public class Login extends Activity {
    private static final String URL = ""; // TODO, URL for database
    private EditText usernameEditText;
    private EditText passwordEditText;
    private Button loginButton;
    private Button cancelButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        usernameEditText = (EditText) findViewById(R.id.usernameEditText);
        passwordEditText = (EditText) findViewById(R.id.passwordEditText);

        loginButton = (Button) findViewById(R.id.loginButton);
        loginButton.setOnClickListener(loginButtonClicked);

        cancelButton = (Button) findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(cancelButtonClicked);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    OnClickListener loginButtonClicked = new OnClickListener() {
        @Override
        public void onClick(View v) {
            String usernameStr = usernameEditText.getText().toString();
            String passwordStr = passwordEditText.getText().toString();
            TravelogueApp appState = ((TravelogueApp)getApplicationContext());

            // Do a basic form check
            if (!passBasicFormValidate(usernameStr, passwordStr)) {
                return;
            }

            // Remote database query: Check credentials, get user
            boolean result = false;
            try {
                result = (boolean)new RemoteCallsImpl().execute("/checkUserCredentials",
                        usernameStr, passwordStr).get();
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (!result) {
                DisplayUtil.showOkAlertDialog(Login.this, R.string.wrong_credentials_alert_msg);
                return;
            }

            // We need to query the database to get the trip state
            LocalDatabaseConnector ldb = new LocalDatabaseConnector(Login.this);
            if (!ldb.doesUserExist(usernameStr)) {
                DisplayUtil.showOkAlertDialog(Login.this, R.string.cannot_login);
                return;
            }

            // Update application state with the current user
            appState.setCurrentUser(usernameStr);
            appState.setOngoingTrip(ldb.getOngoingTripName(usernameStr));

            // Start the main activity
            Intent main = new Intent(Login.this, Main.class);
            startActivity(main);
            finish();
        }
    };

    OnClickListener cancelButtonClicked = new OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };

    private class ValidateUser extends AsyncTask<JSONObject, String, JSONObject> {
        @Override
        protected JSONObject doInBackground(JSONObject... params) {
            HttpHelper httpHelper = new HttpHelper();
            String result = httpHelper.postRequest(URL, params[0]);
            JSONObject response = null;
            try {
                response = new JSONObject(result);
            } catch (JSONException e) {
                Log.e("Error","Error when construct JSON with response");
                e.printStackTrace();
            }
            return response;
        }
    }

    private boolean passBasicFormValidate(String username, String password) {
        // Check if all fields are filled
        if (username.isEmpty() || password.isEmpty()) {
            DisplayUtil.showOkAlertDialog(Login.this, getString(R.string.missing_field_alert_msg));
        // Make sure username is not too long
        } else if (username.length() > 10) {
            DisplayUtil.showOkAlertDialog(Login.this, R.string.max_username_length);
        // Make sure password is not too long
        } else if (password.length() > 10) {
            DisplayUtil.showOkAlertDialog(Login.this, R.string.max_password_length);
        // Successful basic form validation
        } else {
            return true;
        }
        return false;
    }
}
