package com.travelogue.travelogue.DBLayout;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.travelogue.travelogue.entities.Note;
import com.travelogue.travelogue.entities.Place;
import com.travelogue.travelogue.entities.Trip;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Class for working with Trips objects in the database.
 */
public class TripDatabaseHelper implements DatabaseVariables {
    /* Private field */
    private SQLiteDatabase database; // open, writable database object

    /* Public constructor */
    public TripDatabaseHelper(SQLiteDatabase database) {
        this.database = database;
    }

    /* Create a new trip item in the database. Return the ID */
    public long insertTrip(String tripName, int state, long ownerID) {
        if (doesTripExist(tripName, ownerID)) {
            return ERROR_ID;
        }

        ContentValues newTrip = new ContentValues();
        newTrip.put(COLUMN_NAME_TRIP_NAME, tripName);
        newTrip.put(COLUMN_NAME_TRIP_STATE, state);
        newTrip.put(COLUMN_NAME_OWNER_ID, ownerID);
        return database.insert(TABLE_NAME_TRIP, null, newTrip);
    }

    /* Return a trip ID of a username's Trip from the database */
    public long getTripID(String tripName, long ownerID) {
        // Use the owner ID to get all the trip names
        Cursor c = database.query(TABLE_NAME_TRIP, null,
                COLUMN_NAME_TRIP_NAME + "='" + tripName + "' AND " +
                COLUMN_NAME_OWNER_ID + "=" + Long.toString(ownerID), null, null, null, null);

        if ((c != null) && (c.getCount() > 0)) {
            c.moveToFirst();
            long id = c.getLong(c.getColumnIndexOrThrow(COLUMN_NAME_ID));
            c.close();
            return id;
        }
        return ERROR_ID;
    }

    public Trip getTrip(String tripName, long ownerID) {
        // Use the owner ID and trip name to get one trip
        Cursor c = database.query(TABLE_NAME_TRIP,
                null, COLUMN_NAME_OWNER_ID + "=" + Long.toString(ownerID) + " AND " +
                COLUMN_NAME_TRIP_NAME + "='" + tripName + "'",
                null, null, null, null);

        if ((c != null) && (c.getCount() > 0)) {
            c.moveToFirst();
            long id = c.getLong(c.getColumnIndexOrThrow(COLUMN_NAME_ID));
            int state = c.getInt(c.getColumnIndexOrThrow(COLUMN_NAME_TRIP_STATE));
            String name = c.getString(c.getColumnIndexOrThrow(COLUMN_NAME_TRIP_NAME));
            c.close();

            // Get a HashMap of all the Places
            PlaceDatabaseHelper p = new PlaceDatabaseHelper(database);
            HashMap<LatLng, Place> places = p.getAllPlaces(id);

            // Get an array list of all the comments
            CommentDatabaseHelper com = new CommentDatabaseHelper(database);
            ArrayList<Note> comments = com.getAllComments(id);

            Trip t = new Trip(name);
            t.setAllComments(comments);
            t.setAllPlaces(places);
            t.setState(state);
            return t;
        }
        return null;
    }

    /* Return all of username's Trips from the database where the state = state */
    public ArrayList<Trip> getAllTrips(int state, long ownerID) {
        // Use the owner ID to get all the trip names
        Cursor c = database.query(TABLE_NAME_TRIP,
                null, COLUMN_NAME_OWNER_ID + "=" + Long.toString(ownerID) + " AND " +
                COLUMN_NAME_TRIP_STATE + "=" + Integer.toString(state),
                null, null, null, null);

        // Create an array list to fill with trips
        ArrayList<Trip> trips = new ArrayList<>();
        if ((c != null) && (c.getCount() > 0)) {
            c.moveToFirst();
            do {
                long id = c.getLong(c.getColumnIndexOrThrow(COLUMN_NAME_ID));
                int s = c.getInt(c.getColumnIndexOrThrow(COLUMN_NAME_TRIP_STATE));
                String name = c.getString(c.getColumnIndexOrThrow(COLUMN_NAME_TRIP_NAME));

                // Get a HashMap of all the Places
                PlaceDatabaseHelper p = new PlaceDatabaseHelper(database);
                HashMap<LatLng, Place> places = p.getAllPlaces(id);

                // Get an array list of all the comments
                CommentDatabaseHelper com = new CommentDatabaseHelper(database);
                ArrayList<Note> comments = com.getAllComments(id);

                Trip t = new Trip(name);
                t.setAllComments(comments);
                t.setAllPlaces(places);
                t.setState(s);
                trips.add(t);
            } while (c.moveToNext());
            c.close();
        }
        return trips;
    }

    /* Method for deleting trip from database */
    public void deleteTrip(String tripName, long ownerID) {
        database.delete(TABLE_NAME_TRIP, COLUMN_NAME_TRIP_NAME + "='" + tripName + "' AND " +
                COLUMN_NAME_OWNER_ID + "=" + ownerID, null);
    }

    /* Returns whether trip exists for this user */
    public boolean doesTripExist(String tripName, long ownerID) {
        // Use the owner ID to get the trip
        Cursor c = database.query(TABLE_NAME_TRIP, null,
                COLUMN_NAME_OWNER_ID + "=" + Long.toString(ownerID) + " AND " +
                        COLUMN_NAME_TRIP_NAME + "='" + tripName + "'",
                null, null, null, null);

        // If we get results back, return that the trip exists
        if ((c != null) && (c.getCount() > 0)) {
            c.close();
            return true;
        } else if ((c != null) && (c.getCount() == 0)) {
            c.close();
        }
        return false;
    }

    public void updateTripState(String tripName, int state, long ownerID) {
        ContentValues update = new ContentValues();
        update.put(COLUMN_NAME_TRIP_STATE, state);
        database.update(TABLE_NAME_TRIP, update,
                COLUMN_NAME_TRIP_NAME + "='" + tripName + "' AND " +
                COLUMN_NAME_OWNER_ID + "=" + Long.toString(ownerID),
                null);
    }

    /* Returns whether trip is ongoing for this user */
    public String ongoingTrip(long ownerID) {
        // Use the owner ID to get the trip
        Cursor c = database.query(TABLE_NAME_TRIP, null,
                COLUMN_NAME_OWNER_ID + "=" + Long.toString(ownerID),
                null, null, null, null);

        if ((c != null) && (c.getCount() > 0)) {
            c.moveToFirst();
            do {
                int state = c.getInt(c.getColumnIndexOrThrow(COLUMN_NAME_TRIP_STATE));
                if (state != 0) {
                    return c.getString(c.getColumnIndexOrThrow(COLUMN_NAME_TRIP_NAME));
                }
            } while (c.moveToNext());
            c.close();
        }
        return null;
    }
}
