package com.travelogue.travelogue.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.InputType;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.travelogue.travelogue.R;

/**
 * Methods for displaying stuff in activities
 */
public class DisplayUtil {
    /**
     * @param context Application context
     * @param msg Message to display
     */
    public static void showToast(Context context, int msg) {
        int duration = Toast.LENGTH_LONG;
        Toast toast = Toast.makeText(context, context.getString(msg), duration);
        toast.show();
    }

    /**
     * One button alert dialog with input
     *
     * @param context Application context
     * @param msg Message to display (R.string id)
     * @param et EditText to get input from
     */
    public static void showInputAlertDialog(Context context, int msg, EditText et,
                                            final DialogInterface.OnClickListener okMethod) {
        AlertDialog.Builder alertBuilder =
                new AlertDialog.Builder(context, AlertDialog.THEME_HOLO_DARK);
        alertBuilder
                .setView(et)
                .setCancelable(false)
                .setMessage(msg)
                .setPositiveButton(context.getString(R.string.ok), okMethod);
        AlertDialog alert = alertBuilder.create();
        alert.getWindow().setLayout(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        alert.show();
    }

    /**
     * One button alert dialog
     *
     * @param context Application context
     * @param msg Message to display (R.string id)
     */
    public static void showOkAlertDialog(Context context, int msg) {
        showOkAlertDialog(context, context.getString(msg));
    }

    /**
     * One button alert dialog
     *
     * @param context Application context
     * @param msg Message to display
     */
    public static void showOkAlertDialog(Context context, String msg) {
        AlertDialog.Builder alertBuilder =
                new AlertDialog.Builder(context, AlertDialog.THEME_HOLO_DARK);
        alertBuilder
                .setCancelable(false)
                .setMessage(msg)
                .setPositiveButton(context.getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = alertBuilder.create();
        alert.getWindow().setLayout(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        alert.show();
    }

    /**
     * Shows an alert with yes and no buttons.
     *
     * @param context Application context
     * @param msg Message to display (R.string id)
     * @param yesMethod Yes method
     * @param noMethod No method
     */
    public static void showYesNoAlertDialog(Context context, int msg,
                                            final DialogInterface.OnClickListener yesMethod,
                                            final DialogInterface.OnClickListener noMethod) {
        showYesNoAlertDialog(context, context.getString(msg), yesMethod, noMethod);

    }

    /**
     * Shows an alert with yes and no buttons.
     *
     * @param context Application context
     * @param msg Message to display
     * @param yesMethod Yes method
     * @param noMethod No method
     */
    public static void showYesNoAlertDialog(Context context, String msg,
                                            final DialogInterface.OnClickListener yesMethod,
                                            final DialogInterface.OnClickListener noMethod) {
        AlertDialog.Builder alertBuilder =
                new AlertDialog.Builder(context, AlertDialog.THEME_HOLO_DARK);
        alertBuilder
                .setCancelable(false)
                .setMessage(msg)
                .setPositiveButton(context.getString(R.string.yes), yesMethod)
                .setNegativeButton(context.getString(R.string.no), noMethod);
        AlertDialog alert = alertBuilder.create();
        alert.getWindow().setLayout(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        alert.show();
    }
}
