package com.travelogue.travelogue.ws.local;

import android.media.Image;

/**
 * Created by elliotttan on 11/13/15.
 */
public interface CameraServiceInterface {

    /* Open the phone's camera */
    public void openCamera();

    /* Get the image from the camera */
    public Image getImageFromCamera();

}
