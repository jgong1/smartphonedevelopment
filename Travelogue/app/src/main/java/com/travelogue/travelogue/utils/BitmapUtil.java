package com.travelogue.travelogue.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.ByteArrayOutputStream;

/**
 * Used for Bitmap <-> byte[] objects
 * Created by anitazha on 12/8/15.
 */
public class BitmapUtil {
    /* Helper function for getting byte array from Bitmap image */
    public static byte[] getBitmapAsByteArray(Bitmap bitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, outputStream);
        return outputStream.toByteArray();
    }

    /* Helper function for getting Bitmap image from byte array */
    public static Bitmap getByteArrayAsBitmap(byte[] data) {
        Bitmap bmp;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inMutable = true;
        bmp = BitmapFactory.decodeByteArray(data, 0, data.length, options);
        return bmp;
    }
}
