package com.travelogue.travelogue.entities;

import java.util.ArrayList;
import java.util.HashMap;
import com.google.android.gms.maps.model.LatLng;
import com.travelogue.travelogue.exceptions.EntityException;

public class Trip {
    private String name;
    private int state;
    private HashMap<LatLng, Place> places;
    private ArrayList<Note> comments;

    /**
     * @param name the name of the trip
     */
    public Trip(String name) {
        this.name = name;
        this.places = new HashMap<LatLng, Place>();
        this.comments = new ArrayList<Note>();
        this.state = 0;
    }

    /**
     * @return Trip name
     */
    public String getName() {
        return this.name;
    }

    /**
     * @param name Set the name of the trip
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return A place object with corresponding user data. Null if not found
     */
    public Place getPlace(LatLng loc) {
        Place place = this.places.get(loc);
        return place;
    }

    /**
     * @return HashMap with all of the places in this trip
     */
    public HashMap<LatLng, Place> getAllPlaces() {
        return this.places;
    }

    /**
     * Adds a Place to the HashMap
     *
     * @param loc   The latitude and longitude of the Place to add
     * @param place The Place object to add to the trip
     */
    public void setPlace(LatLng loc, Place place) throws EntityException {
        Place p = this.places.get(loc);
        if (p != null) {
            throw new EntityException("Trip, setPlace: Place already exists in this trip", 2);
        } else {
            this.places.put(loc, place);
        }
    }

    /**
     * Replaces the place object with the one provided
     *
     * @param places The HashMap of places to add
     */
    public void setAllPlaces(HashMap<LatLng, Place> places) {
        this.places = places;
    }

    /**
     * @param loc The latitude and longitude of the Place to remove from the list
     */
    public void deletePlace(LatLng loc) {
        this.places.remove(loc);
    }

    /**
     * @param idx The index of the comment to retrieve
     */
    public Note getComment(int idx) throws EntityException {
        if ((idx >= 0) && (idx < this.comments.size())) {
            return this.comments.get(idx);
        } else {
            throw new EntityException("Comment access out of bounds", 1);
        }
    }

    /**
     * @return The list of comments for this trip
     */
    public ArrayList<Note> getAllComments() {
        return this.comments;
    }

    /**
     * @param comment The comment to add to this trip
     */
    public void setComment(Note comment) {
        this.comments.add(comment);
    }

    /**
     * @param comment The comment array list to set for this trip
     */
    public void setAllComments(ArrayList<Note> comment) {
        this.comments = comment;
    }

    /**
     * @return state Whether this trip is ongoing or not
     */
    public int getState() {
        return this.state;
    }

    /**
     * @param state 0 if this state is not ongoing, nonzero if it is
     */
    public void setState(int state) {
        this.state = state;
    }
}