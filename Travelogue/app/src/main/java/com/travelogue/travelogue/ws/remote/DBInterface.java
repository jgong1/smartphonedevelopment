package com.travelogue.travelogue.ws.remote;

import com.travelogue.travelogue.entities.Trip;
import com.travelogue.travelogue.entities.User;

import java.util.List;

/**
 * Created by elliotttan on 11/13/15.
 * Interface for interacting with a remote database.
 */
public interface DBInterface {

    /* Open connection to database. */
    public void open();

    /* Close connection to database. */
    public void close();

    /* Create an entry in the database for a new User */
    public void createUser(User user);

    /* Get a User row from the database for a specified User */
    public User getUser(String username);

    /* Update a User row in the database for a specified User */
    public void updateUser(String username, User user);

    /* Delete a User row in the database for a specified User */
    public void deleteUser(String username);

    /* Create an entry in the database for a new Trip */
    public void createTrip(Trip trip);

    /* Get a Trip row from the database for the specified Trip */
    public Trip getTripById(int id);

    /* Get a list of the trips for a specified User */
    public List<Trip> getTripsByUser(String username);

    /* Update a Trip row in the database for a specified Trip */
    public void updateTrip(int id, Trip trip);

    /* Delete a Trip row in the database */
    public void deleteTrip(int id);

}
