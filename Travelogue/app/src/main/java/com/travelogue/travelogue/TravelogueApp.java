package com.travelogue.travelogue;

import android.app.Application;

/**
 * Holds the login state of a user
 * Created by anitazha on 11/24/15.
 */
public class TravelogueApp extends Application {
    private String currentUser;
    private String ongoingTrip;

    public String getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(String currentUser) {
        this.currentUser = currentUser;
    }

    public String getOngoingTrip() {
        return ongoingTrip;
    }

    public void setOngoingTrip(String name) {
        ongoingTrip = name;
    }
}
