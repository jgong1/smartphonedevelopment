package com.travelogue.travelogue;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.maps.model.LatLng;
import com.travelogue.travelogue.DBLayout.LocalDatabaseConnector;
import com.travelogue.travelogue.entities.Note;
import com.travelogue.travelogue.ui.DisplayUtil;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class AddNote extends Activity {

    private Button saveNoteButton;
    private Button cancelNoteButton;
    private EditText noteEditText;
    private boolean isComment;
    private String receiverName;
    private String tripName;

    private LocationManager locationManager;
    private double currLatitude;
    private double currLongitude;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_note);

        // If this is a comment, we need additional data from the intent to store in the database
        Intent intent = getIntent();
        isComment = intent.getBooleanExtra("isComment", false);
        if (isComment) {
            receiverName = intent.getStringExtra("username");
            tripName = intent.getStringExtra("tripName");
        }

        noteEditText = (EditText) findViewById(R.id.note);

        saveNoteButton = (Button) findViewById(R.id.save_note_button);
        saveNoteButton.setOnClickListener(saveNoteButtonClicked);

        cancelNoteButton = (Button) findViewById(R.id.cancel_note_button);
        cancelNoteButton.setOnClickListener(cancelNoteButtonClicked);

        getLocation();
    }

    /**
     * Save note to the local and remote database
     */
    OnClickListener saveNoteButtonClicked = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (noteEditText.getText().toString().isEmpty()) {
                DisplayUtil.showToast(AddNote.this, R.string.missing_field_alert_msg);
                return;
            }

            LocalDatabaseConnector db = new LocalDatabaseConnector(AddNote.this);
            TravelogueApp appState = (TravelogueApp) getApplicationContext();
            Note note = new Note(appState.getCurrentUser(), noteEditText.getText().toString());

            if (isComment) {
                // TODO insert to remote DB
                db.insertComment(receiverName, tripName, note);
            } else {
                // Insert the note into the local database
                LatLng coord = new LatLng(currLatitude, currLongitude);
                Intent back = getIntent();
                currLatitude = roundDouble(currLatitude, 5);
                currLongitude = roundDouble(currLongitude, 5);
                back.putExtra("Latitude", currLatitude);
                back.putExtra("Longitude", currLongitude);
                setResult(RESULT_OK, back);
                db.insertNote(appState.getOngoingTrip(), coord, note);
            }
            finish();
        }
    };

    /**
     * Shows an alert confirming that the user wants to cancel their note
     */
    OnClickListener cancelNoteButtonClicked = new OnClickListener() {
        @Override
        public void onClick(View v) {
            DisplayUtil.showYesNoAlertDialog(AddNote.this, R.string.cancel_prompt,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    },
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
        }
    };

    /* Helper function for getting location */
    private void getLocation() {
        locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_COARSE);

        try {
            locationManager.requestLocationUpdates(locationManager.getBestProvider(criteria, true),
                    50, 1, new LocationListener() {
                        @Override
                        public void onLocationChanged(Location location) {
                            currLatitude = location.getLatitude();
                            currLongitude = location.getLongitude();
                            Log.e("Current Location", String.valueOf(currLatitude) + ", " + String.valueOf(currLongitude));
                        }
                        /* No changes needed */
                        @Override
                        public void onStatusChanged(String provider, int status, Bundle extras) {}

                        @Override
                        public void onProviderEnabled(String provider) {}

                        @Override
                        public void onProviderDisabled(String provider) {}
                    });
        } catch(Exception e) {
            e.printStackTrace();
            Log.e("Get Location", "Cannot get location " + e.getMessage());
        }
    }

    private double roundDouble(double input, int digits) {
        if (digits < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(input);
        bd = bd.setScale(digits, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
