package com.travelogue.travelogue;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.app.Activity;

import com.travelogue.travelogue.DBLayout.LocalDatabaseConnector;
import com.travelogue.travelogue.entities.Trip;

import java.util.ArrayList;

public class ViewOldTrips extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_string_list);

        // Get the old trips
        TravelogueApp appState = (TravelogueApp) getApplicationContext();
        LocalDatabaseConnector db = new LocalDatabaseConnector(appState);
        ArrayList<Trip> trips = db.getAllOldTrips(appState.getCurrentUser());
        ArrayList<String> mItems = new ArrayList<>();
        for (Trip t : trips) {
            mItems.add(t.getName());
        }

        // Set up the fragment
        ViewOldTripsFragment newFragment = new ViewOldTripsFragment();
        Bundle args = new Bundle();
        args.putStringArrayList(newFragment.getParam1Name(), mItems);
        newFragment.setArguments(args);

        // Start the ViewOldTripsFragment
        FragmentManager manager = getFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(R.id.fragment, newFragment);
        transaction.commit();
    }
}
