package com.travelogue.travelogue;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;

import java.util.ArrayList;

public class ViewOldTripsFragment extends Fragment implements AbsListView.OnItemClickListener {
    private static final String ARG_PARAM1 = "Items Array";
    private AbsListView mListView;
    private ListAdapter mAdapter;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ViewOldTripsFragment() {
    }

    public String getParam1Name() {
        return ARG_PARAM1;
    }

    public static ViewOldTripsFragment newInstance(ArrayList<String> param1) {
        ViewOldTripsFragment fragment = new ViewOldTripsFragment();
        Bundle args = new Bundle();
        args.putStringArrayList(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ArrayList<String> mItems;
        if (getArguments() != null) {
            mItems = getArguments().getStringArrayList(ARG_PARAM1);
        } else {
            return;
        }

        mAdapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_list_item_1, android.R.id.text1, mItems);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_viewoldtrips, container, false);

        // Set the adapter
        mListView = (AbsListView) view.findViewById(android.R.id.list);
        mListView.setAdapter(mAdapter);

        // Set OnItemClickListener so we can be notified on item clicks
        mListView.setOnItemClickListener(this);

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent myIntent = new Intent(getActivity(), ViewArchivedTrip.class);
        TravelogueApp app = (TravelogueApp) getActivity().getApplicationContext();
        myIntent.putExtra("tripName", (String) mAdapter.getItem(position));
        myIntent.putExtra("username", app.getCurrentUser());
        getActivity().startActivity(myIntent);
    }
}
