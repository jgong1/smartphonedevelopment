package com.travelogue.travelogue.DBLayout;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

/**
 * Class for working with friends relations in the database.
 */
public class FriendDatabaseHelper implements DatabaseVariables {
    /* Private field */
    private SQLiteDatabase database; // open, writable database object

    /* Public constructor */
    public FriendDatabaseHelper(SQLiteDatabase database) {
        this.database = database;
    }

    /* Create a new friend item in the database. Return the ID */
    public long insertFriend(String username, long ownerID) {
        // If they are already friends, don't add a duplicate
        if (isFriend(username, ownerID)) {
            return ERROR_ID;
        }

        ContentValues newFriend = new ContentValues();
        newFriend.put(COLUMN_NAME_USERNAME, username);
        newFriend.put(COLUMN_NAME_OWNER_ID, ownerID);
        return database.insert(TABLE_NAME_FRIEND, null, newFriend);
    }

    /* Return all of a user's friends from the database */
    public ArrayList<String> getAllFriends(long ownerID) {
        // Use the owner ID to get all the friends
        Cursor c = database.query(TABLE_NAME_FRIEND,
                null, COLUMN_NAME_OWNER_ID + "=" + Long.toString(ownerID), null, null, null, null);

        // Create an array list to return and fill with friends
        ArrayList<String> friends = new ArrayList<>();
        if ((c != null) && (c.getCount() > 0)) {
            c.moveToFirst();
            do {
                String name = c.getString(c.getColumnIndexOrThrow(COLUMN_NAME_USERNAME));
                friends.add(name);
            } while (c.moveToNext());
            c.close();
        }
        return friends;
    }

    /* Method for deleting a friend relation from the database */
    public void deleteFriend(String username, long ownerID) {
        database.delete(TABLE_NAME_FRIEND, COLUMN_NAME_USERNAME + "='" + username + "' AND " +
                COLUMN_NAME_OWNER_ID + "=" + Long.toString(ownerID), null);
    }

    /* Returns whether username follows friendName (is a friend of) */
    public boolean isFriend(String username, long ownerID) {
        // Use the owner ID to get all the friends
        Cursor c = database.query(TABLE_NAME_FRIEND, null,
                COLUMN_NAME_OWNER_ID + "=" + Long.toString(ownerID) + " AND " +
                COLUMN_NAME_USERNAME + "='" + username + "'",
                null, null, null, null);

        // If we get results back, return that they are friends
        if ((c != null) && (c.getCount() > 0)) {
            return true;
        } else {
            return false;
        }
    }
}
