package com.travelogue.travelogue.exceptions;

import android.util.Log;

public abstract class GeneralException extends Exception {
    private int errno;

    /**
     * @param msg   A message for this exception
     * @param errno The errno of this exception
     */
    public GeneralException(String msg, int errno) {
        super(msg);
        this.errno = errno;
    }

    /**
     * @return The error number of the current exception.
     */
    public int getErrno() {
        return this.errno;
    }

    /**
     * @param errno The error number to set for this exception.
     */
    public void setErrno(int errno) {
        this.errno = errno;
    }

    /**
     * Logs an exception's message to the console
     *
     * @param exceptionName The name of the exception to show when logging
     *                      the message in the console.
     */
    public void log(String exceptionName) {
        Log.d(exceptionName, getMessage());
    }

    /**
     * Generic/default fix method for each exception.
     */
    public abstract void fix();
}
