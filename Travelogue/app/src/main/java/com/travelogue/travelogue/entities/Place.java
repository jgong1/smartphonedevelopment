package com.travelogue.travelogue.entities;

import android.graphics.Bitmap;

import com.google.android.gms.maps.model.LatLng;
import com.travelogue.travelogue.exceptions.EntityException;

import java.util.ArrayList;

public class Place {
    private LatLng loc;
    private ArrayList<Bitmap> photos;
    private ArrayList<Note> notes;

    /**
     * @param loc The latitude and longitude of this Place.
     */
    public Place(LatLng loc) {
        this.loc = loc;
        this.photos = new ArrayList<Bitmap>();
        this.notes = new ArrayList<Note>();
    }

    /**
     * @return The latitude and longitude of this Place
     */
    public LatLng getLoc() {
        return this.loc;
    }

    /**
     * @param loc The latitude and longitude to set
     */
    public void setLoc(LatLng loc) {
        this.loc = loc;
    }

    /**
     * @return A list of all the photos
     */
    public Bitmap getPhoto(int idx) throws EntityException {
        if ((idx >= 0) && (idx < this.photos.size())) {
            return this.photos.get(idx);
        } else {
            throw new EntityException("Access to photos is out of bounds", 1);
        }
    }

    /**
     * @return A list of all the photos
     */
    public ArrayList<Bitmap> getAllPhotos() {
        return this.photos;
    }

    /**
     * @param photo A Bitmap of the photo to add to the list
     */
    public void setPhoto(Bitmap photo) {
        this.photos.add(photo);
    }

    /**
     * @param photo A Bitmap array list to set this Place with
     */
    public void setAllPhotos(ArrayList<Bitmap> photo) {
        this.photos = photo;
    }

    /**
     * @param idx The index of the photo in the ArrayList to remove
     */
    public void deletePhoto(int idx) {
        if ((idx >= 0) && (idx < this.photos.size())) {
            this.photos.remove(idx);
        }
    }

    /**
     * @param idx Index of the note to retrieve
     * @return The Note object
     */
    public Note getNote(int idx) throws EntityException {
        if ((idx >= 0) && (idx < this.notes.size())) {
            return this.notes.get(idx);
        } else {
            throw new EntityException("Access to notes is out of bounds", 2);
        }
    }

    /**
     * @return A list of all the Notes
     */
    public ArrayList<Note> getAllNotes() {
        return this.notes;
    }

    /**
     * @param note The note to add to the list
     */
    public void setNote(Note note) {
        this.notes.add(note);
    }

    /**
     * @param note The note array list to set for this Place
     */
    public void setAllNotes(ArrayList<Note> note) {
        this.notes = note;
    }

    /**
     * @param idx The index of the note to delete from the list
     */
    public void deleteNote(int idx) {
        if ((idx >= 0) && (idx < this.notes.size())) {
            this.notes.remove(idx);
        }
    }
}
