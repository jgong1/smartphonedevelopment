package com.travelogue.travelogue;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.travelogue.travelogue.DBLayout.LocalDatabaseConnector;
import com.travelogue.travelogue.entities.Note;
import com.travelogue.travelogue.entities.Trip;

import java.util.ArrayList;

public class ViewComment extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_string_list);

        if (findViewById(R.id.fragment) != null) {
            // Get the username and the trip name
            Intent intent = getIntent();
            String username = intent.getStringExtra("username");
            String tripName = intent.getStringExtra("tripName");

            // TODO get all comments from remote database
            TravelogueApp app = (TravelogueApp) getApplicationContext();
            LocalDatabaseConnector db = new LocalDatabaseConnector(ViewComment.this);
            Trip trip = db.getTrip(tripName, username);
            ArrayList<Note> comments = trip.getAllComments();
            ArrayList<String> formattedComments = formatComments(comments);
            for (String s: formattedComments) {
                Log.d("form", s);
            }

            // Add comments to the fragment
            ViewCommentFragment newFragment = new ViewCommentFragment();
            Bundle args = new Bundle();
            args.putStringArrayList(newFragment.getParam1Name(), formattedComments);
            newFragment.setArguments(args);

            // Add the fragment to the placeholder
            FragmentManager manager = getFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.add(R.id.fragment, newFragment);
            transaction.commit();
        }
    }

    private ArrayList<String> formatComments(ArrayList<Note> comments) {
        ArrayList<String> com = new ArrayList<>();
        for (Note n : comments) {
            com.add(n.getContent() + "\n-- " + n.getAuthor());
        }
        return com;
    }
}