package com.travelogue.travelogue;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.travelogue.travelogue.DBLayout.LocalDatabaseConnector;
import com.travelogue.travelogue.entities.Trip;

public class ViewArchivedTrip extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap map;
    private Button addCommentButton;
    private Button viewCommentButton;
    private Trip trip;
    private String username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_archived_trip);

        Intent intent = getIntent();
        String tripName = intent.getStringExtra("tripName");
        username = intent.getStringExtra("username");

        LocalDatabaseConnector db = new LocalDatabaseConnector(ViewArchivedTrip.this);
        trip = db.getTrip(tripName, username);

        addCommentButton = (Button) findViewById(R.id.add_comment_button);
        addCommentButton.setOnClickListener(addCommentButtonClicked);

        viewCommentButton = (Button) findViewById(R.id.view_comment_button);
        viewCommentButton.setOnClickListener(viewCommentButtonClicked);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setOnCameraChangeListener(cameraListener);
        map.setOnMarkerClickListener(markerListener);
    }

    GoogleMap.OnMarkerClickListener markerListener = new GoogleMap.OnMarkerClickListener() {
        @Override
        public boolean onMarkerClick(Marker marker) {
            LatLng coord = marker.getPosition();
            Intent intent = new Intent(ViewArchivedTrip.this, ViewPlaceContent.class);
            intent.putExtra("username", username);
            intent.putExtra("tripName", trip.getName());
            intent.putExtra("latitude", coord.latitude);
            intent.putExtra("longitude", coord.longitude);
            startActivity(intent);
            return false;
        }
    };

    GoogleMap.OnCameraChangeListener cameraListener = new GoogleMap.OnCameraChangeListener() {
        @Override
        public void onCameraChange(CameraPosition arg0) {
            // Load a list of coordinates from the Trip and center the map on all of them
            if (trip.getAllPlaces().keySet().size() > 0) {
                LatLngBounds.Builder bounds = new LatLngBounds.Builder();
                for (LatLng loc : trip.getAllPlaces().keySet()) {
                    map.addMarker(new MarkerOptions().position(loc));
                    bounds.include(loc);
                }
                map.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds.build(), 50));
            }
            map.setOnCameraChangeListener(null);
        }
    };

    OnClickListener addCommentButtonClicked = new OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(ViewArchivedTrip.this, AddNote.class);
            intent.putExtra("isComment", true);
            intent.putExtra("username", username);
            intent.putExtra("tripName", trip.getName());
            startActivity(intent);
        }
    };

    OnClickListener viewCommentButtonClicked = new OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(ViewArchivedTrip.this, ViewComment.class);
            intent.putExtra("username", username);
            intent.putExtra("tripName", trip.getName());
            startActivity(intent);
        }
    };
}
