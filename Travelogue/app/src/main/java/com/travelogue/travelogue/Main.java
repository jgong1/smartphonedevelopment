package com.travelogue.travelogue;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.travelogue.travelogue.DBLayout.LocalDatabaseConnector;
import com.travelogue.travelogue.entities.User;
import com.travelogue.travelogue.ui.DisplayUtil;

/**
 * User home page
 */
public class Main extends Activity {

    private Button startButton;
    private Button viewOldTripsButton;
    private Button friendsButton;
    private Button logoutButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        startButton = (Button) findViewById(R.id.startButton);
        viewOldTripsButton = (Button) findViewById(R.id.viewOldTripsButton);
        friendsButton = (Button) findViewById(R.id.friendsButton);
        logoutButton = (Button) findViewById(R.id.logoutButton);

        LocalDatabaseConnector ldb = new LocalDatabaseConnector(getApplicationContext());
        User user = ldb.getUser(((TravelogueApp)getApplicationContext()).getCurrentUser());
        if (user != null) {
            ImageView photoIV = (ImageView) findViewById(R.id.profile_iv);
            photoIV.setImageBitmap(user.getProfilePicture());
        }

        startButton.setOnClickListener(startButtonClicked);
        viewOldTripsButton.setOnClickListener(viewOldTripsButtonClicked);
        friendsButton.setOnClickListener(friendsButtonClicked);
        logoutButton.setOnClickListener(logoutButtonClicked);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onBackPressed() {
        DisplayUtil.showYesNoAlertDialog(Main.this,
                R.string.logout_prompt,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                },
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
    }

    OnClickListener startButtonClicked = new OnClickListener() {
        @Override
        public void onClick(View v) {
            // Check for ongoing trips
            TravelogueApp appState = ((TravelogueApp)getApplicationContext());
            if (appState.getOngoingTrip() == null) {
                // If there is not, show a dialog to create one
                final EditText et = new EditText(Main.this);
                et.setInputType(InputType.TYPE_CLASS_TEXT);
                DisplayUtil.showInputAlertDialog(Main.this, R.string.new_trip_name, et,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (!et.getText().toString().isEmpty()) {
                                    String tripName = et.getText().toString();
                                    TravelogueApp appState = ((TravelogueApp) getApplicationContext());
                                    LocalDatabaseConnector ldb = new LocalDatabaseConnector(Main.this);

                                    // Check for duplicate trip names
                                    if (ldb.doesTripExist(appState.getCurrentUser(), tripName)) {
                                        DisplayUtil.showToast(Main.this, R.string.duplicate_trip_name);
                                        et.setText("");
                                    // Insert trip into DB
                                    } else {
                                        ldb.insertTrip(tripName, 1, appState.getCurrentUser());
                                        appState.setOngoingTrip(tripName);
                                        Intent start = new Intent(Main.this, ViewTrip.class);
                                        startActivity(start);
                                    }
                                }
                            }
                        });
            } else {
                Intent start = new Intent(Main.this, ViewTrip.class);
                startActivity(start);
            }
        }
    };

    OnClickListener viewOldTripsButtonClicked = new OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent viewOldTrips = new Intent(Main.this, ViewOldTrips.class);
            startActivity(viewOldTrips);
        }
    };

    OnClickListener friendsButtonClicked = new OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent friends = new Intent(Main.this, Friends.class);
            startActivity(friends);
        }
    };

    OnClickListener logoutButtonClicked = new OnClickListener() {
        @Override
        public void onClick(View v) {
            onBackPressed();
        }
    };
}
