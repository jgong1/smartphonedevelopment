package com.travelogue.travelogue.DBLayout;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;

import com.travelogue.travelogue.entities.Note;
import com.travelogue.travelogue.utils.BitmapUtil;

import java.util.ArrayList;

/**
 * Class for working with photo objects in the database.
 */
public class PhotoDatabaseHelper implements DatabaseVariables {
    /* Private field */
    private SQLiteDatabase database; // open, writable database object

    /* Public constructor */
    public PhotoDatabaseHelper(SQLiteDatabase database) {
        this.database = database;
    }

    /* Create a new photo item in the database. Return the ID */
    public long insertPhoto(Bitmap photo, long placeID) {
        // Don't check for duplicates because adding a duplicate photo is ok.
        ContentValues newPhoto = new ContentValues();
        newPhoto.put(COLUMN_NAME_PHOTO, BitmapUtil.getBitmapAsByteArray(photo));
        newPhoto.put(COLUMN_NAME_PLACE_ID, placeID);
        return database.insert(TABLE_NAME_PHOTO, null, newPhoto);
    }

    /* Return all of a place's photos from the database */
    public ArrayList<Bitmap> getAllPhotos(long placeID) {
        // Use the place ID to get all the photos
        Cursor c = database.query(TABLE_NAME_PHOTO,
                null, COLUMN_NAME_PLACE_ID + "=" + Long.toString(placeID), null, null, null, null);

        // Create an array list to return and fill with photos
        ArrayList<Bitmap> photos = new ArrayList<>();
        if ((c != null) && (c.getCount() > 0)) {
            c.moveToFirst();
            do {
                byte[] photo = c.getBlob(c.getColumnIndexOrThrow(COLUMN_NAME_PHOTO));
                photos.add(BitmapUtil.getByteArrayAsBitmap(photo));
            } while (c.moveToNext());
            c.close();
        }
        return photos;
    }

    /* Method for deleting a photo from the database */
    public void deletePhoto(long placeID) {
        // TODO Photos are forever
    }
}
