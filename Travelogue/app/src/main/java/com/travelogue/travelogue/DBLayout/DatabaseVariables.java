package com.travelogue.travelogue.DBLayout;

/**
 * Database table and column names.
 * Created by anitazha on 12/8/15.
 */
public interface DatabaseVariables {
    int ERROR_ID = -1;
    int DATABASE_VERSION = 1;
    String DATABASE_NAME = "TravelogueDatabase";

    String TABLE_NAME_USER = "users";
    String TABLE_NAME_FRIEND = "friends";
    String TABLE_NAME_TRIP = "trips";
    String TABLE_NAME_PLACE = "places";
    String TABLE_NAME_COMMENT = "comments";
    String TABLE_NAME_NOTE = "notes";
    String TABLE_NAME_PHOTO = "photos";

    String COLUMN_NAME_ID = "id";
    String COLUMN_NAME_OWNER_ID = "ownerID";
    String COLUMN_NAME_TRIP_ID = "tripID";
    String COLUMN_NAME_PLACE_ID = "placeID";
    String COLUMN_NAME_PROFILE_PIC = "profilePicture";
    String COLUMN_NAME_USERNAME = "username";
    String COLUMN_NAME_TRIP_NAME = "tripName";
    String COLUMN_NAME_TRIP_STATE = "tripState";
    String COLUMN_NAME_LATITUDE = "latitude";
    String COLUMN_NAME_LONGITUDE = "longitude";
    String COLUMN_NAME_TEXT = "text";
    String COLUMN_NAME_PHOTO = "photo";
    String COLUMN_NAME_AUTHOR = "author";
}
