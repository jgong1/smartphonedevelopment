package com.travelogue.travelogue.DBLayout;

import android.graphics.Bitmap;

import com.google.android.gms.maps.model.LatLng;
import com.travelogue.travelogue.entities.Note;
import com.travelogue.travelogue.entities.Trip;
import com.travelogue.travelogue.entities.User;

import java.util.ArrayList;

/**
 * Contract for methods to be implemented for the LocalDatabaseConnector
 * Created by anitazha on 12/10/15.
 */
public interface LocalDatabaseInterface {
    /**
     * Returns a user object containing all of user's current data.
     * @param username The username of the user to get
     * @return User object of all the cached fields for this user. Null on error.
     */
    public User getUser(String username);

    /**
     * Returns a trip object containing all of a trip's current data.
     * @param tripName The trip to retrieve
     * @param username The user who owns this trip
     * @return A trip object of all the cached fields for this trip. Null on error.
     */
    public Trip getTrip(String tripName, String username);

    /**
     * Returns the name of the ongoing trip. Null if there is no ongoing trip.
     * @param username The use of interest
     * @return The name of the ongoing trip, if it exists. Null otherwise.
     */
    public String getOngoingTripName(String username);

    /**
     * @return A list of all the users in the local database
     */
    public ArrayList<String> getAllUsers();

    /**
     * Gets a list of all of a user's archived trips
     * @param username The user whose trips we are looking up
     * @return An array list containing all of this user's archived trips.
     */
    public ArrayList<Trip> getAllOldTrips(String username);

    /**
     * Get a list of all of a user's friends.
     * @param username The user of interest
     * @return An array list of all of this user's friends.
     */
    public ArrayList<String> getAllFriends(String username);

    /**
     * Inserts a new user into the local database
     * @param username The user's name
     * @param profilePic Bitmap of the user's profile picture
     * @return Row ID in the user table of the entry. ERROR_ID on error
     */
    public long insertUser(String username, Bitmap profilePic);

    /**
     * Inserts all the fields of a user object into the appropriate tables in the local database.
     * The user object of interest was obtained from the remote database.
     * Assumes that this user does not exist in the database.
     * @param user User object
     * @return Row ID in the user table of the entry. ERROR_ID on error
     */
    public long insertUserFromRemote(User user);

    /**
     * Inserts a new trip into the trip table.
     * @param tripName The name of the trip
     * @param state 0 if the trip is archived, nonzero if it is ongoing
     * @param username The user's name
     * @return Row ID in the trip table of the entry. ERROR_ID on error
     */
    public long insertTrip(String tripName, int state, String username);

    /**
     *
     * @param username Username of the person who owns the trip
     * @param tripName The trip name to add the comment to
     * @param note The note with contents
     * @return Row ID of the comments table tihs was added to. ERROR_ID on error.
     */
    public long insertComment(String username, String tripName, Note note);

    /**
     * Adds a user to your friends list
     * @param username Your username
     * @param friend The name of the friend to add
     * @return Row ID of the Friends table
     */
    public long insertFriend(String username, String friend);

    /**
     * Inserts a user note into the database. The username is the note's author
     * @param tripName The name of the trip this note belongs to
     * @param coord The coordinates this note was saved at
     * @param note The note to save
     * @return Row ID of the note table of the entry. ERROR_ID on error
     */
    public long insertNote(String tripName, LatLng coord, Note note);

    /**
     * Inserts a photo into the database
     * @param username Owner of this photo
     * @param tripName Trip name
     * @param coord Coordinates of the place
     * @param photo Photo to store
     * @return Row ID of the photo table of the entry. ERROR_ID on error.
     */
    public long insertPhoto(String username, String tripName, LatLng coord, Bitmap photo);

    /**
     * Updates the state of a trip in the database.
     * @param tripName The name of the trip to update.
     * @param state The new state of this trip.
     * @param username The user who owns this trip
     */
    public void updateTripState(String tripName, int state, String username);

    /**
     * Returns whether a user is in the local database.
     * @param username The user to look up
     * @return True if this user is in the local database, false if not.
     */
    public boolean doesUserExist(String username);

    /**
     * Returns whether a user has a trip of this name.
     * @param username The user of interest
     * @param tripName The name of the trip to look up
     * @return True if this user owns a trip by this name. False otherwise.
     */
    public boolean doesTripExist(String username, String tripName);

    /**
     * Returns whether you are a friend of friendName.
     * @param friendName Name of a friend
     * @param username Your username
     * @return Returns true if the 2 are friends, false otherwise
     */
    public boolean isFriend(String friendName, String username);

    /**
     * Clears the local database.
     */
    public void clearDatabase();
}
