package com.travelogue.travelogue.DBLayout;

import android.content.Context;
import android.database.sqlite.*;
import android.graphics.Bitmap;

import com.google.android.gms.maps.model.LatLng;
import com.travelogue.travelogue.entities.Note;
import com.travelogue.travelogue.entities.Place;
import com.travelogue.travelogue.entities.Trip;
import com.travelogue.travelogue.entities.User;

import java.util.ArrayList;

/**
 * Used to access user related objects in the database
 * Anita Zhang (anitazha)
 */
public class LocalDatabaseConnector implements DatabaseVariables, LocalDatabaseInterface {
    /* Private field */
    private SQLiteDatabase database;
    private DatabaseOpenHelper databaseOpenHelper;

    public LocalDatabaseConnector(Context context) {
        databaseOpenHelper = new DatabaseOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public long insertUser(String username, Bitmap profilePic) {
        database = databaseOpenHelper.open();
        UserDatabaseHelper userDB = new UserDatabaseHelper(database);
        long id = userDB.insertUser(username, profilePic);
        database.close();
        return id;
    }

    public long insertUserFromRemote(User user) {
        database = databaseOpenHelper.open();
        UserDatabaseHelper userDB = new UserDatabaseHelper(database);
        FriendDatabaseHelper friendDB = new FriendDatabaseHelper(database);
        TripDatabaseHelper tripDB = new TripDatabaseHelper(database);
        CommentDatabaseHelper comDB = new CommentDatabaseHelper(database);
        PlaceDatabaseHelper placeDB = new PlaceDatabaseHelper(database);
        PhotoDatabaseHelper photoDB = new PhotoDatabaseHelper(database);
        NoteDatabaseHelper noteDB = new NoteDatabaseHelper(database);
        long id = ERROR_ID;
        if (user != null) {
            // Insert the user first
            id = userDB.insertUser(user.getUsername(), user.getProfilePicture());
            for (String f : user.getAllFriends()) {
                friendDB.insertFriend(f, id);
            }
            // Use the user's ID to insert trips
            for (Trip t : user.getAllTrips()) {
                long tid = tripDB.insertTrip(t.getName(), t.getState(), id);

                // Use the trip ID to insert comments
                for (Note n : t.getAllComments()) {
                    comDB.insertComment(n.getContent(), n.getAuthor(), tid);
                }

                // Use the trip ID to insert places
                for (LatLng l : t.getAllPlaces().keySet()) {
                    long pid = placeDB.insertPlace(l.latitude, l.longitude, tid);
                    Place p = t.getPlace(l);
                    if (p != null) {
                        // Use the place ID to insert photos
                        for (Bitmap b : p.getAllPhotos()) {
                            photoDB.insertPhoto(b, pid);
                        }
                        // Use the place ID to insert notes
                        for (Note n : p.getAllNotes()) {
                            noteDB.insertNote(n.getContent(), n.getAuthor(), pid);
                        }
                    }
                }
            }
        }
        database.close();
        return id;
    }

    public long insertTrip(String tripName, int state, String username) {
        database = databaseOpenHelper.open();
        UserDatabaseHelper userDB = new UserDatabaseHelper(database);
        long ownerID = userDB.getUserID(username);
        long id;
        if (ownerID == ERROR_ID) {
            id = ERROR_ID;
        } else {
            TripDatabaseHelper tripDB = new TripDatabaseHelper(database);
            id = tripDB.insertTrip(tripName, state, ownerID);
        }
        database.close();
        return id;
    }

    @Override
    public long insertComment(String username, String tripName, Note note) {
        database = databaseOpenHelper.open();
        UserDatabaseHelper userDB = new UserDatabaseHelper(database);
        long ownerID = userDB.getUserID(username);
        long id;
        if (ownerID == ERROR_ID) {
            id = ERROR_ID;
        } else {
            TripDatabaseHelper tripDB = new TripDatabaseHelper(database);
            long tid = tripDB.getTripID(tripName, ownerID);
            CommentDatabaseHelper comDB = new CommentDatabaseHelper(database);
            id = comDB.insertComment(note.getContent(), note.getAuthor(), tid);
        }
        database.close();
        return id;
    }

    @Override
    public long insertFriend(String username, String friend) {
        database = databaseOpenHelper.open();
        UserDatabaseHelper userDB = new UserDatabaseHelper(database);
        long ownerID = userDB.getUserID(username);
        long id;
        if (ownerID == ERROR_ID) {
            id = ERROR_ID;
        } else {
            FriendDatabaseHelper fdb = new FriendDatabaseHelper(database);
            id = fdb.insertFriend(friend, ownerID);
        }
        database.close();
        return id;
    }

    public long insertPhoto(String username, String tripName, LatLng coord, Bitmap photo) {
        database = databaseOpenHelper.open();
        UserDatabaseHelper userDB = new UserDatabaseHelper(database);
        long ownerID = userDB.getUserID(username);
        long id = ERROR_ID;
        if (ownerID != ERROR_ID) {
            TripDatabaseHelper tripDB = new TripDatabaseHelper(database);
            long tid = tripDB.getTripID(tripName, ownerID);

            PlaceDatabaseHelper placeDB = new PlaceDatabaseHelper(database);
            long pid = placeDB.getPlaceID(coord.latitude, coord.longitude, tid);
            if (pid == ERROR_ID) {
                pid = placeDB.insertPlace(coord.latitude, coord.longitude, tid);
            }

            PhotoDatabaseHelper photoDB = new PhotoDatabaseHelper(database);
            id = photoDB.insertPhoto(photo, pid);
        }
        database.close();
        return id;
    }

    public long insertNote(String tripName, LatLng coord, Note note) {
        database = databaseOpenHelper.open();
        UserDatabaseHelper userDB = new UserDatabaseHelper(database);
        long ownerID = userDB.getUserID(note.getAuthor());
        long id = ERROR_ID;
        if (ownerID != ERROR_ID) {
            TripDatabaseHelper tripDB = new TripDatabaseHelper(database);
            long tid = tripDB.getTripID(tripName, ownerID);

            PlaceDatabaseHelper placeDB = new PlaceDatabaseHelper(database);
            long pid = placeDB.getPlaceID(coord.latitude, coord.longitude, tid);
            if (pid == ERROR_ID) {
                pid = placeDB.insertPlace(coord.latitude, coord.longitude, tid);
            }

            NoteDatabaseHelper noteDB = new NoteDatabaseHelper(database);
            id = noteDB.insertNote(note.getContent(), note.getAuthor(), pid);
        }
        database.close();
        return id;
    }

    public User getUser(String username) {
        database = databaseOpenHelper.open();
        UserDatabaseHelper userDB = new UserDatabaseHelper(database);
        User u = userDB.getUser(username);
        database.close();
        return u;
    }

    public ArrayList<String> getAllUsers() {
        database = databaseOpenHelper.open();
        UserDatabaseHelper userDB = new UserDatabaseHelper(database);
        ArrayList<String> u = userDB.getAllUsers();
        database.close();
        return u;
    }

    public Trip getTrip(String tripName, String username) {
        database = databaseOpenHelper.open();
        UserDatabaseHelper userDB = new UserDatabaseHelper(database);
        long ownerID = userDB.getUserID(username);
        Trip t;
        if (ownerID == ERROR_ID) {
            t = null;
        } else {
            TripDatabaseHelper tripDB = new TripDatabaseHelper(database);
            t = tripDB.getTrip(tripName, ownerID);
        }
        database.close();
        return t;
    }

    public ArrayList<Trip> getAllOldTrips(String username) {
        database = databaseOpenHelper.open();
        UserDatabaseHelper userDB = new UserDatabaseHelper(database);
        long ownerID = userDB.getUserID(username);
        ArrayList<Trip> trip;
        if (ownerID == ERROR_ID) {
            trip = null;
        } else {
            TripDatabaseHelper tripDB = new TripDatabaseHelper(database);
            trip = tripDB.getAllTrips(0, ownerID);
        }
        database.close();
        return trip;
    }

    public void updateTripState(String tripName, int state, String username) {
        database = databaseOpenHelper.open();
        UserDatabaseHelper userDB = new UserDatabaseHelper(database);
        long ownerID = userDB.getUserID(username);
        if (ownerID != ERROR_ID) {
            TripDatabaseHelper tripDB = new TripDatabaseHelper(database);
            tripDB.updateTripState(tripName, state, ownerID);
        }
        database.close();
    }

    public ArrayList<String> getAllFriends(String username) {
        database = databaseOpenHelper.open();
        UserDatabaseHelper userDB = new UserDatabaseHelper(database);
        FriendDatabaseHelper friendsDB = new FriendDatabaseHelper(database);
        ArrayList<String> friends = new ArrayList<>();
        long ownerID = userDB.getUserID(username);
        if (ownerID != ERROR_ID) {
            friends = friendsDB.getAllFriends(ownerID);
        }
        database.close();
        return friends;
    }

    public boolean doesUserExist(String username) {
        database = databaseOpenHelper.open();
        UserDatabaseHelper userDB = new UserDatabaseHelper(database);
        boolean ret = true;
        if (userDB.getUserID(username) == ERROR_ID) {
            ret = false;
        }
        database.close();
        return ret;
    }

    public String getOngoingTripName(String username) {
        database = databaseOpenHelper.open();
        UserDatabaseHelper userDB = new UserDatabaseHelper(database);
        long ownerID = userDB.getUserID(username);
        String name;
        if (ownerID == ERROR_ID) {
            name = null;
        } else {
            TripDatabaseHelper tripDB = new TripDatabaseHelper(database);
            name = tripDB.ongoingTrip(ownerID);
        }
        database.close();
        return name;
    }

    public boolean doesTripExist(String username, String tripName) {
        database = databaseOpenHelper.open();
        UserDatabaseHelper userDB = new UserDatabaseHelper(database);
        long ownerID = userDB.getUserID(username);
        boolean ret;
        if (ownerID == ERROR_ID) {
            ret = false;
        } else {
            TripDatabaseHelper tripDB = new TripDatabaseHelper(database);
            ret = tripDB.doesTripExist(tripName, ownerID);
        }
        database.close();
        return ret;
    }

    @Override
    public boolean isFriend(String friendName, String username) {
        database = databaseOpenHelper.open();
        UserDatabaseHelper userDB = new UserDatabaseHelper(database);
        long ownerID = userDB.getUserID(username);
        if (ownerID == ERROR_ID) {
            database.close();
            return false;
        } else {
            FriendDatabaseHelper fdb = new FriendDatabaseHelper(database);
            boolean retval = fdb.isFriend(friendName, ownerID);
            database.close();
            return retval;
        }
    }

    public void clearDatabase() {
        database = databaseOpenHelper.open();
        databaseOpenHelper.onUpgrade(database, DATABASE_VERSION, DATABASE_VERSION + 1);
        database.close();
    }

    /**
     * For opening, closing, upgrading, creating tables in the database
     */
    private class DatabaseOpenHelper extends SQLiteOpenHelper {
        public DatabaseOpenHelper(Context context, String name,
                                  SQLiteDatabase.CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        /* Open DatabaseConnection */
        public SQLiteDatabase open() {
            return databaseOpenHelper.getWritableDatabase();
        }

        @Override
        public void onOpen(SQLiteDatabase db) {
            super.onOpen(db);
            if (!db.isReadOnly()) {
                // Enable foreign key constraints
                db.execSQL("PRAGMA foreign_keys=ON;");
            }
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            String createUserQuery = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME_USER + " (" +
                    COLUMN_NAME_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                    COLUMN_NAME_USERNAME + " TEXT NOT NULL, " +
                    COLUMN_NAME_PROFILE_PIC + " BLOB NOT NULL" +
                    ");";

            String createFriendQuery = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME_FRIEND + " (" +
                    COLUMN_NAME_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                    COLUMN_NAME_USERNAME + " TEXT NOT NULL, " +
                    COLUMN_NAME_OWNER_ID + " INTEGER NOT NULL, " +
                    "FOREIGN KEY (" + COLUMN_NAME_OWNER_ID + ") REFERENCES " +
                    TABLE_NAME_USER + "(" + COLUMN_NAME_ID + ") ON DELETE CASCADE" +
                    ");";

            String createTripQuery = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME_TRIP + " (" +
                    COLUMN_NAME_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                    COLUMN_NAME_TRIP_NAME + " TEXT NOT NULL, " +
                    COLUMN_NAME_TRIP_STATE + " INTEGER NOT NULL, " +
                    COLUMN_NAME_OWNER_ID + " INTEGER NOT NULL, " +
                    "FOREIGN KEY (" + COLUMN_NAME_OWNER_ID + ") REFERENCES " +
                    TABLE_NAME_USER + "(" + COLUMN_NAME_ID + ") ON DELETE CASCADE" +
                    ");";

            String createPlaceQuery = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME_PLACE + " (" +
                    COLUMN_NAME_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                    COLUMN_NAME_LATITUDE + " REAL NOT NULL, " +
                    COLUMN_NAME_LONGITUDE + " REAL NOT NULL, " +
                    COLUMN_NAME_TRIP_ID + " INTEGER NOT NULL, " +
                    "FOREIGN KEY (" + COLUMN_NAME_TRIP_ID + ") REFERENCES " +
                    TABLE_NAME_TRIP + "(" + COLUMN_NAME_ID + ") ON DELETE CASCADE" +
                    ");";

            String createCommentQuery = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME_COMMENT + " (" +
                    COLUMN_NAME_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                    COLUMN_NAME_TEXT + " TEXT NOT NULL, " +
                    COLUMN_NAME_AUTHOR + " TEXT NOT NULL, " +
                    COLUMN_NAME_TRIP_ID + " INTEGER NOT NULL, " +
                    "FOREIGN KEY (" + COLUMN_NAME_TRIP_ID + ") REFERENCES " +
                    TABLE_NAME_TRIP + "(" + COLUMN_NAME_ID + ") ON DELETE CASCADE" +
                    ");";

            String createNoteQuery = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME_NOTE + " (" +
                    COLUMN_NAME_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                    COLUMN_NAME_TEXT + " TEXT NOT NULL, " +
                    COLUMN_NAME_AUTHOR + " TEXT NOT NULL, " +
                    COLUMN_NAME_PLACE_ID + " INTEGER NOT NULL, " +
                    "FOREIGN KEY (" + COLUMN_NAME_PLACE_ID + ") REFERENCES " +
                    TABLE_NAME_PLACE + "(" + COLUMN_NAME_ID + ") ON DELETE CASCADE" +
                    ");";

            String createPhotoQuery = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME_PHOTO + " (" +
                    COLUMN_NAME_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                    COLUMN_NAME_PHOTO + " BLOB NOT NULL, " +
                    COLUMN_NAME_PLACE_ID + " INTEGER NOT NULL, " +
                    "FOREIGN KEY (" + COLUMN_NAME_PLACE_ID + ") REFERENCES " +
                    TABLE_NAME_PLACE + "(" + COLUMN_NAME_ID + ") ON DELETE CASCADE" +
                    ");";

            db.execSQL(createUserQuery);
            db.execSQL(createFriendQuery);
            db.execSQL(createTripQuery);
            db.execSQL(createPlaceQuery);
            db.execSQL(createCommentQuery);
            db.execSQL(createNoteQuery);
            db.execSQL(createPhotoQuery);
        }

        /**
         * This database is only a cache for online data, so its upgrade policy is
         * to simply to discard the data and start over
         */
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // All the other tables cascade from the user table, so just deleting users is enough
            String sqlDeleteUser = "DROP TABLE IF EXISTS " + TABLE_NAME_USER;
            String sqlDeleteFriend = "DROP TABLE IF EXISTS " + TABLE_NAME_FRIEND;
            String sqlDeleteTrip = "DROP TABLE IF EXISTS " + TABLE_NAME_TRIP;
            String sqlDeletePlace = "DROP TABLE IF EXISTS " + TABLE_NAME_PLACE;
            String sqlDeleteComment = "DROP TABLE IF EXISTS " + TABLE_NAME_COMMENT;
            String sqlDeleteNote = "DROP TABLE IF EXISTS " + TABLE_NAME_NOTE;
            String sqlDeletePhoto = "DROP TABLE IF EXISTS " + TABLE_NAME_PHOTO;
            db.execSQL(sqlDeleteUser);
            db.execSQL(sqlDeleteFriend);
            db.execSQL(sqlDeleteTrip);
            db.execSQL(sqlDeletePlace);
            db.execSQL(sqlDeleteComment);
            db.execSQL(sqlDeleteNote);
            db.execSQL(sqlDeletePhoto);
            onCreate(db);
        }

        @Override
        public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            onUpgrade(db, oldVersion, newVersion);
        }
    }
}
