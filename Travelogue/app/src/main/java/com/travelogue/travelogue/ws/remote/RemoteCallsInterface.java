package com.travelogue.travelogue.ws.remote;

import android.graphics.Bitmap;

import com.travelogue.travelogue.entities.Note;
import com.travelogue.travelogue.entities.Trip;
import com.travelogue.travelogue.entities.User;

import java.util.List;

public interface RemoteCallsInterface {

    /**
     * Validates a login attempt.
     * @param username Username
     * @param password Password
     * @return true if the login credentials are valid, false otherwise.
     */
    public boolean checkUserCredentials(String username, String password);

    /**
     * Registers a new user in the remote database.
     * @param username Username
     * @param password Password
     * @param profilePic Profile picture as a Bitmap
     * @return true if registration was successful, false otherwise.
     */
    public boolean register(String username, String password, Bitmap profilePic);

    /**
     * Query the remote database for a User object
     * @param username Username
     * @return User object for the specified username.
     */
    public User getUser(String username);

    /**
     * Query the remote database for a list of comments for a given trip.
     * @param username Username
     * @param tripName Name of the trip
     * @return List of Notes objects for the specified trip.
     */
    public List<Note> getComments(String username, String tripName);

    /**
     * Create a new trip entry in the remote database.
     * @param username Username
     * @param trip Trip object to be saved
     */
    public void newTrip(String username, Trip trip);

    /**
     * Query the remote database for all the usernames.
     * @return List of all usernames as Strings.
     */
    public List<String> getAllUsers();

    /**
     * Add a friend for the specified user.
     * @param username Username of user initiating the action.
     * @param friend Username of person the user wants to follow.
     */
    public void addFriend(String username, String friend);

    /**
     * Create a new comment entry in the remote database.
     * @param note Note object to be stored.
     * @param tripName Name of the trip.
     * @param username Username of the user who created the trip.
     */
    public void addComment(String author, String content, String tripName, String username);
}