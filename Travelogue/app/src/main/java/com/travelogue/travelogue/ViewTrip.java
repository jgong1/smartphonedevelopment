package com.travelogue.travelogue;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.travelogue.travelogue.DBLayout.LocalDatabaseConnector;
import com.travelogue.travelogue.entities.Trip;
import com.travelogue.travelogue.utils.BitmapUtil;


public class ViewTrip extends FragmentActivity implements OnMapReadyCallback {
    private final static int TAKE_PHOTO_REQUEST = 1;
    private static final int UPDATE_MARKER = 2;
    private GoogleMap map;
    private Button addNoteButton;
    private Button addPhotoButton;
    private Button endTripButton;
    private Trip trip;
    LatLngBounds.Builder bounds;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_trip);

        TravelogueApp app = (TravelogueApp) getApplicationContext();
        LocalDatabaseConnector db = new LocalDatabaseConnector(ViewTrip.this);
        trip = db.getTrip(app.getOngoingTrip(), app.getCurrentUser());
        bounds = new LatLngBounds.Builder();

        addNoteButton = (Button) findViewById(R.id.add_note_button);
        addNoteButton.setOnClickListener(addNoteButtonClicked);

        addPhotoButton = (Button) findViewById(R.id.add_photo_button);
        addPhotoButton.setOnClickListener(addPhotoButtonClicked);

        endTripButton = (Button) findViewById(R.id.end_trip_button);
        endTripButton.setOnClickListener(endTripButtonClicked);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition arg0) {
                updateCamera();
            }
        });
    }

    private void updateCamera() {
        /* Update the trip object with new places */
        TravelogueApp app = (TravelogueApp) getApplicationContext();
        LocalDatabaseConnector db = new LocalDatabaseConnector(ViewTrip.this);
        trip = db.getTrip(app.getOngoingTrip(), app.getCurrentUser());

        // Load a list of coordinates from the Trip and center the map on all of them
        if (trip.getAllPlaces().keySet().size() > 0) {
            for (LatLng loc : trip.getAllPlaces().keySet()) {
                map.addMarker(new MarkerOptions().position(loc));
                bounds.include(loc);
            }
            map.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds.build(), 50));
        }
        map.setOnCameraChangeListener(null);
    }

    OnClickListener addNoteButtonClicked = new OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(ViewTrip.this, AddNote.class);
            startActivityForResult(intent, UPDATE_MARKER);
        }
    };

    OnClickListener addPhotoButtonClicked = new OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(ViewTrip.this, AddPhoto.class);
            startActivityForResult(intent, TAKE_PHOTO_REQUEST);
        }
    };

    OnClickListener endTripButtonClicked = new OnClickListener() {
        @Override
        public void onClick(View v) {
            TravelogueApp appState = (TravelogueApp) getApplicationContext();
            LocalDatabaseConnector db = new LocalDatabaseConnector(ViewTrip.this);
            db.updateTripState(appState.getOngoingTrip(), 0, appState.getCurrentUser());
            appState.setOngoingTrip(null);
            finish();
        }
    };

    /*
        Add marker to map with the results get from add photo activity
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            double latitude = data.getDoubleExtra("Latitude", 0.0);
            double longitude = data.getDoubleExtra("Longitude", 0.0);
            LatLng newAddedLatLng = new LatLng(latitude, longitude);

            if (requestCode == TAKE_PHOTO_REQUEST) {
                /* Get result data */
                /* Add marker to the map */
                if (data.hasExtra("Image")) {
                    byte[] picTook = data.getByteArrayExtra("Image");
                    Bitmap newAddedBitmap = BitmapUtil.getByteArrayAsBitmap(picTook);
                    map.addMarker(new MarkerOptions()
                            .position(newAddedLatLng).rotation(90)
                            .icon(BitmapDescriptorFactory.fromBitmap(newAddedBitmap)));
                } else {
                    map.addMarker(new MarkerOptions().position(newAddedLatLng));
                }
                Log.e("Latitude", String.valueOf(latitude));
                Log.e("Longitude", String.valueOf(longitude));



            } else if (requestCode == UPDATE_MARKER) {
                map.addMarker(new MarkerOptions().position(newAddedLatLng));
            }

            bounds.include(newAddedLatLng);
            map.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds.build(), 50));
        }
    }
}
