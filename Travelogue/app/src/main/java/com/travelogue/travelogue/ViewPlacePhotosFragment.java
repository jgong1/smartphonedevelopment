package com.travelogue.travelogue;

import android.app.Activity;
import android.app.Fragment;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;

import java.util.ArrayList;

public class ViewPlacePhotosFragment extends Fragment {
    private static final String ARG_PARAM1 = "Items Array";
    private AbsListView mListView;
    private ListAdapter mAdapter;
    private ArrayList<Bitmap> mItems;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ViewPlacePhotosFragment() {
    }

    public String getParam1Name() {
        return ARG_PARAM1;
    }

    public static ViewPlacePhotosFragment newInstance(ArrayList<Bitmap> param1) {
        ViewPlacePhotosFragment fragment = new ViewPlacePhotosFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mItems = getArguments().getParcelableArrayList(ARG_PARAM1);
        } else {
            return;
        }
        mAdapter = new ImageAdapter(mItems);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_viewphotos, container, false);
        // Set the adapter
        mListView = (AbsListView) view.findViewById(android.R.id.list);
        mListView.setAdapter(mAdapter);
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private class ImageAdapter extends ArrayAdapter<Bitmap> {
        public ImageAdapter(ArrayList<Bitmap> items) {
            super(getActivity(), android.R.layout.simple_list_item_1, items);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // if we weren't given a view, inflate one
            if (null == convertView) {
                convertView = getActivity().getLayoutInflater().inflate(R.layout.image_list_item, null);
            }

            ImageView iv = (ImageView) convertView.findViewById(R.id.imageview);
            iv.setImageBitmap(mItems.get(position));

            return convertView;
        }
    }
}
