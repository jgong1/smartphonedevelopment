package com.travelogue.travelogue.DBLayout;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.travelogue.travelogue.entities.Note;

import java.util.ArrayList;

/**
 * Class for working with Note objects in the database.
 */
public class NoteDatabaseHelper implements DatabaseVariables {
    /* Private field */
    private SQLiteDatabase database; // open, writable database object

    /* Public constructor */
    public NoteDatabaseHelper(SQLiteDatabase database) {
        this.database = database;
    }

    /* Create a new note item in the database. Return the ID */
    public long insertNote(String text, String author, long placeID) {
        // Don't check for duplicates because adding a duplicate comment is ok.
        ContentValues newNote = new ContentValues();
        newNote.put(COLUMN_NAME_TEXT, text);
        newNote.put(COLUMN_NAME_AUTHOR, author);
        newNote.put(COLUMN_NAME_PLACE_ID, placeID);
        return database.insert(TABLE_NAME_NOTE, null, newNote);
    }

    /* Return all of a place's notes from the database */
    public ArrayList<Note> getAllNotes(long placeID) {
        // Use the place ID to get all the notes
        Cursor c = database.query(TABLE_NAME_NOTE,
                null, COLUMN_NAME_PLACE_ID + "=" + Long.toString(placeID), null, null, null, null);

        // Create an array list to return and fill with notes
        ArrayList<Note> notes = new ArrayList<>();
        if ((c != null) && (c.getCount() > 0)) {
            c.moveToFirst();
            do {
                String content = c.getString(c.getColumnIndexOrThrow(COLUMN_NAME_TEXT));
                String author = c.getString(c.getColumnIndexOrThrow(COLUMN_NAME_AUTHOR));
                Note note = new Note(author, content);
                notes.add(note);
            } while (c.moveToNext());
            c.close();
        }
        return notes;
    }

    /* Method for deleting a note from the database */
    public void deleteNote(long placeID) {
        // TODO Notes are forever
    }
}
