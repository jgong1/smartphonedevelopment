package com.travelogue.travelogue;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.travelogue.travelogue.DBLayout.LocalDatabaseConnector;
import com.travelogue.travelogue.ui.DisplayUtil;
import com.travelogue.travelogue.ws.remote.RemoteCallsImpl;

import java.io.IOException;

/**
 * Registration page
 */
public class Register extends Activity {
    private static final int SELECT_PICTURE = 1;
    private EditText usernameEditText;
    private EditText passwordEditText;
    private EditText retypePasswordEditText;
    private Button photoButton;
    private Button registerButton;
    private Button cancelButton;
    private Bitmap profilePic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        usernameEditText = (EditText) findViewById(R.id.usernameEditText);
        passwordEditText = (EditText) findViewById(R.id.passwordEditText);
        retypePasswordEditText = (EditText) findViewById(R.id.retypePasswordEditText);

        photoButton = (Button) findViewById(R.id.choosePhotoButton);
        photoButton.setOnClickListener(photoButtonClicked);

        registerButton = (Button) findViewById(R.id.registerButton);
        registerButton.setOnClickListener(registerButtonClicked);

        cancelButton = (Button) findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(cancelButtonClicked);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {
                // Get the image as a bitmap
                Uri uri = data.getData();
                try {
                    profilePic = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                    profilePic = Bitmap.createScaledBitmap(profilePic, 500, 500, false);
                } catch (IOException e) {
                    DisplayUtil.showOkAlertDialog(this, R.string.image_error_alert_msg);
                }

                // Change the button so the user knows an image is selected
                photoButton.setText(R.string.photo_chosen);
            }
        }
    }

    OnClickListener photoButtonClicked = new OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("image/*");
            startActivityForResult(intent, SELECT_PICTURE);
        }
    };

    OnClickListener registerButtonClicked = new OnClickListener() {
        @Override
        public void onClick(View v) {
            String usernameStr = usernameEditText.getText().toString();
            String passwordStr1 = passwordEditText.getText().toString();
            String passwordStr2 = retypePasswordEditText.getText().toString();

            // Check for all filled form fields and matching passwords
            if (!passBasicFormValidate(usernameStr, passwordStr1, passwordStr2)) {
                return;
            }

            // Remote database query: Check if username exists and Insert user into remote database
            boolean result = false;
            try {
                result = (boolean)new RemoteCallsImpl().execute("/register",
                        usernameStr, passwordStr1, profilePic).get();
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (!result) {
                DisplayUtil.showToast(getApplicationContext(), R.string.user_exists_alert_msg);
                return;
            }

            // Cache user in the local database
            LocalDatabaseConnector db = new LocalDatabaseConnector(getApplicationContext());
            db.insertUser(usernameStr, profilePic);

            // Set as the current application user
            TravelogueApp appState = (TravelogueApp)getApplicationContext();
            appState.setCurrentUser(usernameStr);
            appState.setOngoingTrip(null);

            // Start the main activity
            Intent main = new Intent(Register.this, Main.class);
            startActivity(main);
            finish(); // We do not want the user to come back to register when they log out

        }
    };

    public void onBackPressed() {
        DisplayUtil.showYesNoAlertDialog(Register.this,
                R.string.cancel_prompt,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                },
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
    }

    OnClickListener cancelButtonClicked = new OnClickListener() {
        @Override
        public void onClick(View v) {
            onBackPressed();
        }
    };

    /**
     * Performs basic form validation
     *
     * @param username String of username edit text
     * @param password1 String of password edit text
     * @param password2 String of retypte password edit text
     * @return True if it passes basic validation, false otherwise
     */
    private boolean passBasicFormValidate(String username, String password1, String password2) {
        // Make sure all fields are filled (username, 2 passwords, image)
        if (username.isEmpty() || password1.isEmpty() || password2.isEmpty() ||
                (profilePic == null)) {
            DisplayUtil.showOkAlertDialog(Register.this, R.string.missing_field_alert_msg);
        // Make sure username is not too long
        } else if (username.length() > 10) {
            DisplayUtil.showOkAlertDialog(Register.this, R.string.max_username_length);
        // Make sure password is not too long
        } else if (password1.length() > 10) {
            DisplayUtil.showOkAlertDialog(Register.this, R.string.max_password_length);
        // Make sure passwords match
        } else if (!password1.equals(password2)) {
            DisplayUtil.showOkAlertDialog(Register.this, R.string.mismatch_password_alert_msg);
        } else {
            // Successful basic form validation
            return true;
        }
        return false;
    }
}
