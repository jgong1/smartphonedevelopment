package com.travelogue.travelogue.exceptions;

/**
 * Exceptions that can occur with remote database accesses package.
 */
public class RemoteException extends GeneralException {
    public RemoteException(String msg, int errno) {
        super(msg, errno);
    }

    @Override
    public void fix(){
        switch (getErrno()) {
            default:
                printStackTrace();
                break;
        }
    }
}
