package com.travelogue.travelogue.ws.remote;

import android.support.v4.app.Fragment;

import com.travelogue.travelogue.entities.Trip;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by elliotttan on 11/13/15.
 */
public interface GoogleMapsInterface {

    /* Query Google Maps for the current map location */
    public Fragment getCurrentMap(LatLng loc);

    /* Start a Google Maps trip */
    public void beginTrip();

    /* Ends a Google Maps tip */
    public void endTrip();

    /* Query Google Maps for the finished trip, with locations stored as pins */
    public Fragment getFinishedTrip(Trip trip);
}
