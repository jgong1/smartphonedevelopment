package com.travelogue.travelogue.utils;

import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by jiangong on 11/24/15.
 */
public class HttpHelper {

    /*
        Method for sending http post request
     */
    public String postRequest(String url, JSONObject postBody) {

        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(url);
        StringBuilder sb = new StringBuilder();

        try {
            StringEntity stringEntity = new StringEntity(postBody.toString());
            httpPost.setEntity(stringEntity);
            httpPost.setHeader("content-type", "application/json");
            HttpResponse httpResponse = httpClient.execute(httpPost);
            InputStream inputStream = httpResponse.getEntity().getContent();
            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
        } catch (Exception e) {
            Log.e("Error", "Error when send post request");
            e.printStackTrace();
        }

        return sb.toString();
    }
}
