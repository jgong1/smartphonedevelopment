package com.travelogue.travelogue.utils;

import android.content.Context;
import android.hardware.Camera;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;

/**
 * Created by jiangong on 12/9/15.
 */
public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback, Camera.PictureCallback{

    private SurfaceHolder mHolder;
    private Camera mCamera;

    /*
        Constructor
     */
    public CameraPreview(Context context, Camera camera) {
        super(context);
        mCamera = camera;
        mCamera.setDisplayOrientation(90);
        mHolder = getHolder();
        mHolder.addCallback(this);
    }

    /*
        Create surface and start camera preview
     */
    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        try {
            if (mCamera != null) {
                mCamera.setPreviewDisplay(holder);
                mCamera.startPreview();
            }
        } catch(IOException e) {
            Log.e("CameraPreview", "Error starting camera preview " + e.getMessage());
        }

    }

    /*
        Update camera preview
     */
    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        refreshCamera(mCamera);
    }

    public void refreshCamera(Camera camera) {
        if (mHolder.getSurface() == null){
            return;
        }

        try {
            mCamera.stopPreview();  // Stop preview for making changes
        } catch (Exception e){
            // ignore: tried to stop a non-existent preview
        }

        // set preview size and make any resize, rotate or
        // reformatting changes here
        mCamera = camera;
        try {   // start preview with new settings
            mCamera.setPreviewDisplay(mHolder);
            mCamera.startPreview();

        } catch (Exception e){
            Log.d("Camera Preview", "Error starting camera preview: " + e.getMessage());
        }
    }

    /*
        Releasing camera
     */
    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        if (mCamera != null) {
            mCamera.release();
            mCamera = null;
        }
    }

    @Override
    public void onPictureTaken(byte[] data, Camera camera) {

    }
}
