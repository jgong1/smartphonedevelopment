package com.travelogue.travelogue;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;

import com.travelogue.travelogue.DBLayout.LocalDatabaseConnector;
import com.travelogue.travelogue.entities.Trip;
import com.travelogue.travelogue.entities.User;

import java.util.ArrayList;

public class OthersMain extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_others_main);

        Intent intent = getIntent();
        String username = intent.getStringExtra("username");

        // TODO query remote database and get this friend's User object. Store in local DB.
        LocalDatabaseConnector ldb = new LocalDatabaseConnector(getApplicationContext());
        User user = ldb.getUser(username);
        if (user != null) {
            ImageView photoIV = (ImageView) findViewById(R.id.profile_pic);
            photoIV.setImageBitmap(user.getProfilePicture());
        }

        ArrayList<String> trip = new ArrayList<>();
        for (Trip t : user.getAllTrips()) {
            trip.add(t.getName());
        }

        // Add comments to the fragment
        OthersMainFragment newFragment = new OthersMainFragment();
        Bundle args = new Bundle();
        args.putString(newFragment.getUsernameParam(), username);
        args.putStringArrayList(newFragment.getArrayListTripsParam(), trip);
        newFragment.setArguments(args);

        // Add the fragment to the placeholder
        FragmentManager manager = getFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(R.id.fragment, newFragment);
        transaction.commit();
    }
}
