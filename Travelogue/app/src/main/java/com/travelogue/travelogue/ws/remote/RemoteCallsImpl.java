package com.travelogue.travelogue.ws.remote;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.JsonReader;
import android.util.JsonWriter;

import com.google.android.gms.maps.model.LatLng;
import com.travelogue.travelogue.entities.Note;
import com.travelogue.travelogue.entities.Place;
import com.travelogue.travelogue.entities.Trip;
import com.travelogue.travelogue.entities.User;
import com.travelogue.travelogue.utils.BitmapUtil;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by elliotttan on 12/11/15.
 */
public class RemoteCallsImpl extends AsyncTask<Object, String, Object> {

    private static final String backendURL = "http://travelogue-kh9sdmpdju.elasticbeanstalk.com";

    @Override
    protected Object doInBackground(Object... args) {
        String result = null;
        try {
            if (((String)args[0]).equals("/checkUserCredentials")) {
                if (checkUserCredentials((String) args[1], (String) args[2])) {
                    return true;
                } else {
                    return false;
                }
            } else if (((String)args[0]).equals("/register")) {
                if (register((String)args[1], (String)args[2], (Bitmap)args[3])) {
                    return true;
                } else {
                    return false;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }



    /**
     * Validates a login attempt.
     * @param username Username
     * @param password Password
     * @return true if the login credentials are valid, false otherwise.
     */
    public boolean checkUserCredentials(String username, String password) {
        boolean result = false;
        try {
            URL url = new URL(backendURL + "/checkUserCredentials?username=" + username +
                "&password=" + password);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));
            reader.beginObject();
            while (reader.hasNext()) {
                String name = reader.nextName();
                if (name.equals("result")) {
                    String response = reader.nextString();
                    if (response.equals("true")) {
                        result = true;
                    } else {
                        reader.skipValue();
                    }
                }
            }
            reader.endObject();
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Registers a new user in the remote database.
     * @param username Username
     * @param password Password
     * @param profilePic Profile picture as a Bitmap
     * @return true if registration was successful, false otherwise.
     */
    public boolean register(String username, String password, Bitmap profilePic) {
        boolean result = false;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(backendURL + "/register");
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Content-Type", "application/json");
            OutputStream out = new BufferedOutputStream(urlConnection.getOutputStream());
            JsonWriter writer = new JsonWriter(new OutputStreamWriter(out, "UTF-8"));
            writer.beginObject();
            writer.name("username").value(username);
            writer.name("password").value(password);
            String profilePicture = Base64.encodeToString(
                    BitmapUtil.getBitmapAsByteArray(profilePic), Base64.DEFAULT);
            writer.name("profilePicture").value(profilePicture);
            writer.endObject();
            writer.close();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));
            reader.beginObject();
            while (reader.hasNext()) {
                String name = reader.nextName();
                if (name.equals("result")) {
                    String response = reader.nextString();
                    if (response.equals("true")) {
                        result = true;
                    }
                }
            }
            reader.endObject();
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null)
                urlConnection.disconnect();
        }
        return result;
    }

    /**
     * Query the remote database for a User object
     * @param username Username
     * @return User object for the specified username.
     */
    public User getUser(String username) {
        boolean result = false;
        User user = null;
        try {
            URL url = new URL(backendURL + "/getUser?username=" + username);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();
            user = new User(username);
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));
            reader.beginObject();
            while (reader.hasNext()) {
                String name = reader.nextName();
                if (name.equals("profilePicture")) {
                    String profilePictureStr = reader.nextString();
                    byte[] bytes = Base64.decode(profilePictureStr, Base64.DEFAULT);
                    Bitmap profilePicture = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                } else if (name.equals("trips")) {
                    reader.beginArray();
                    while (reader.hasNext()) {
                        Trip trip = new Trip("");
                        reader.beginObject();
                        while (reader.hasNext()) {
                            String tripName = reader.nextName();
                            if (tripName.equals("name")) {
                                trip.setName(reader.nextString());
                            } else if (tripName.equals("state")) {
                                trip.setState(reader.nextInt());
                            } else if (tripName.equals("places")) {
                                reader.beginObject();
                                while (reader.hasNext()) {
                                    String locString = reader.nextName();
                                    String[] str = locString.split(",");
                                    double latitude = Double.valueOf(str[0]);
                                    double longitude = Double.valueOf(str[1]);
                                    LatLng latLng = new LatLng(latitude, longitude);
                                    Place place = new Place(latLng);
                                    reader.beginObject();
                                    while (reader.hasNext()) {
                                        String placeName = reader.nextName();
                                        if (placeName.equals("photos")) {
                                            reader.beginArray();
                                            while (reader.hasNext()) {
                                                String photoStr = reader.nextString();
                                                byte[] bytes = Base64.decode(photoStr, Base64.DEFAULT);
                                                Bitmap photo = BitmapFactory.decodeByteArray(bytes, 0,
                                                        bytes.length);
                                                place.setPhoto(photo);
                                            }
                                            reader.endArray();
                                        } else if (placeName.equals("notes")) {
                                            reader.beginObject();
                                            Note note = new Note("", "");
                                            while (reader.hasNext()) {
                                                String noteName = reader.nextName();
                                                if (noteName.equals("author")) {
                                                    note.setAuthor(reader.nextString());
                                                } else if (noteName.equals("content")) {
                                                    note.setContent(reader.nextString());
                                                }
                                            }
                                            reader.endObject();
                                            place.setNote(note);
                                        }
                                    }
                                    reader.endObject();
                                    trip.setPlace(latLng, place);
                                }
                                reader.endObject();
                            } else if (name.equals("comments")) {
                                reader.beginArray();
                                while (reader.hasNext()) {
                                    reader.beginObject();
                                    Note comment = new Note("", "");
                                    while (reader.hasNext()) {
                                        String commentName = reader.nextName();
                                        if (commentName.equals("author")) {
                                            comment.setAuthor(reader.nextString());
                                        } else if (commentName.equals("content")) {
                                            comment.setContent(reader.nextString());
                                        }
                                    }
                                    reader.endObject();
                                    trip.setComment(comment);
                                }
                                reader.endArray();
                            }
                        }
                        reader.endObject();
                        user.setTrip(trip);
                    }
                    reader.endArray();
                } else if (name.equals("friends")) {
                    reader.beginArray();
                    while (reader.hasNext()) {
                        user.setFriend(reader.nextString());
                    }
                    reader.endArray();
                } else {
                    reader.skipValue();
                }
            }
            reader.endObject();
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return user;
    }

    /**
     * Query the remote database for a list of comments for a given trip.
     * @param username Username
     * @param tripName Name of the trip
     * @return List of Notes objects for the specified trip.
     */
    public List<Note> getComments(String username, String tripName) {
        List<Note> comments = new ArrayList<Note>();
        try {
            URL url = new URL(backendURL + "/checkUserCredentials?username=" + username +
                "&tripName=" + tripName);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));
            reader.beginArray();
            while (reader.hasNext()) {
                reader.beginObject();
                Note note = new Note("","");
                while (reader.hasNext()) {
                    String name = reader.nextName();
                    if (name.equals("author")) {
                        note.setAuthor(reader.nextString());
                    } else if (name.equals("content")) {
                        note.setContent(reader.nextString());
                    }
                }
                reader.endObject();
                comments.add(note);
            }
            reader.endArray();
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return comments;
    }

    /**
     * Create a new trip entry in the remote database.
     * @param username Username
     * @param trip Trip object to be saved
     */
    public void newTrip(String username, Trip trip) {
        try {
            URL url = new URL(backendURL + "/newTrip");
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.connect();
            OutputStream out = new BufferedOutputStream(urlConnection.getOutputStream());
            JsonWriter writer = new JsonWriter(new OutputStreamWriter(out, "UTF-8"));
            writer.beginObject();
            writer.name("username").value(username);
            // Write Trip object as JSON
            writer.name("trip");
            writer.beginObject();
            writer.name("name").value(trip.getName());
            writer.name("state").value(trip.getState());
            // Write places map as JSON
            writer.name("places");
            writer.beginObject();
            Map<LatLng, Place> allPlaces = trip.getAllPlaces();
            for (Place place : allPlaces.values()) {
                String latLngStr = String.valueOf(place.getLoc().latitude) + "," +
                        String.valueOf(place.getLoc().longitude);
                writer.name(latLngStr);
                // Write Place object as JSON
                writer.beginObject();
                writer.name("latitude").value(place.getLoc().latitude);
                writer.name("longitude").value(place.getLoc().longitude);
                writer.name("photos");
                // Write list of Photos as JSON
                writer.beginArray();
                for (Bitmap bitmap : place.getAllPhotos()) {
                    String photo = Base64.encodeToString(
                            BitmapUtil.getBitmapAsByteArray(bitmap), Base64.DEFAULT);
                    writer.value(photo);
                }
                writer.endArray();
                writer.name("comments");
                // Write list of Notes as JSON
                writer.beginArray();
                for (Note note : place.getAllNotes()) {
                    writer.beginObject();
                    writer.name("author").value(note.getAuthor());
                    writer.name("content").value(note.getContent());
                    writer.endObject();
                }
                writer.endArray();
            }
            writer.endObject();
            // Write comments list as JSON
            writer.beginArray();
            for (Note comment : trip.getAllComments()) {
                writer.beginObject();
                writer.name("author").value(comment.getAuthor());
                writer.name("content").value(comment.getContent());
                writer.endObject();
            }
            writer.endArray();
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Query the remote database for all the usernames.
     * @return List of all usernames as Strings.
     */
    public List<String> getAllUsers() {
        List<String> users = new ArrayList<String>();
        try {
            URL url = new URL(backendURL + "/getUserList");
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));
            reader.beginArray();
            while (reader.hasNext()) {
                users.add(reader.nextString());
            }
            reader.endArray();
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return users;
    }

    /**
     * Add a friend for the specified user.
     * @param username Username of user initiating the action.
     * @param friend Username of person the user wants to follow.
     */
    public void addFriend(String username, String friend) {
        try {
            URL url = new URL(backendURL + "/addFriend");
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.connect();
            OutputStream out = new BufferedOutputStream(urlConnection.getOutputStream());
            JsonWriter writer = new JsonWriter(new OutputStreamWriter(out, "UTF-8"));
            writer.beginObject();
            writer.name("username").value(username);
            writer.name("friend").value(friend);
            writer.endObject();
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Create a new comment entry in the remote database.
     * @param author
     * @param content
     * @param tripName Name of the trip.
     * @param username Username of the user who created the trip.
     */
    public void addComment(String author, String content, String tripName, String username) {
        try {
            URL url = new URL(backendURL + "/addComment");
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.connect();
            OutputStream out = new BufferedOutputStream(urlConnection.getOutputStream());
            JsonWriter writer = new JsonWriter(new OutputStreamWriter(out, "UTF-8"));
            writer.beginObject();
            writer.name("author").value(author);
            writer.name("content").value(content);
            writer.name("tripName").value(tripName);
            writer.name("username").value(username);
            writer.endObject();
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
