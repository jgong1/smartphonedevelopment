package com.travelogue.travelogue.exceptions;

/**
 * Exceptions that can occur in the entity/model package.
 */
public class EntityException extends GeneralException {
    public EntityException(String msg, int errno) {
        super(msg, errno);
    }

    @Override
    public void fix(){
        switch (getErrno()) {
            // Access out of bounds, ignore request
            case 1:
                log("Access out of bounds");
                break;
            // No duplicates allowed, ignore request
            case 2:
                log("No Duplicates Allowed");
                break;
            default:
                break;
        }
    }
}
