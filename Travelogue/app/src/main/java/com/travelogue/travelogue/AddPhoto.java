package com.travelogue.travelogue;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Button;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.travelogue.travelogue.DBLayout.LocalDatabaseConnector;
import com.travelogue.travelogue.entities.Trip;
import com.travelogue.travelogue.utils.CameraPreview;
import com.travelogue.travelogue.utils.BitmapUtil;
import com.travelogue.travelogue.DBLayout.PhotoDatabaseHelper;
import com.travelogue.travelogue.DBLayout.PlaceDatabaseHelper;

import java.io.File;

import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AddPhoto extends Activity {

    private Button takePhotoButton;
    private Button cancelPhotoButton;

    private Camera mCamera;
    private RelativeLayout mCameraLayout;
    private CameraPreview mCameraPreview;

    private LocationManager locationManager;
    private double currLatitude;
    private double currLongitude;

    private byte[] picByteArr;
    private Bitmap picBitmap;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_photo);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        /* Initialize layouts */
        takePhotoButton = (Button) findViewById(R.id.take_photo_button);
        takePhotoButton.setOnClickListener(takePhotoButtonClicked);

        cancelPhotoButton = (Button) findViewById(R.id.cancel_photo_button);
        cancelPhotoButton.setOnClickListener(cancelPhotoButtonClicked);

        mCameraLayout = (RelativeLayout)findViewById(R.id.inner_relative_layout);

        /* Initialize camera */
        try {
            if (mCamera != null) {
                mCamera.release();
                mCamera = null;
            }
            mCamera = Camera.open(findBackFacingCamera());
        } catch(Exception e) {
            e.printStackTrace();
            Log.e("Initialize", "Cannot initialize camera " + e.getMessage());
        }

        /* Add camera preview to layout */
        if (mCamera != null) {
            mCameraPreview = new CameraPreview(this, mCamera);
            mCameraLayout.addView(mCameraPreview);
        }

        /* Get current location */
        getLocation();
    }

    OnClickListener takePhotoButtonClicked = new OnClickListener() {
        @Override
        public void onClick(View v) {
            // TODO: This should be camera and show alert dialog
            Log.e("ButtonText", takePhotoButton.getText().toString());
            Log.e("Equal", String.valueOf(takePhotoButton.getText().toString().trim().equals("Take")));
            if (takePhotoButton.getText().toString().trim().equals("Take")) {
                /* Take picture */
                mCamera.takePicture(null, null, getPictureCallback());
                takePhotoButton.setText("Save");
            } else {
                takePhotoButton.setText("Take");
                Toast picSavedToast = Toast.makeText(getApplicationContext(),
                        "Picture Saved", Toast.LENGTH_SHORT);
                picSavedToast.show();
                /* Resize Bitmap and back to viewtrip activity */
                Intent back = getIntent();
                if (picBitmap != null) {
                    Bitmap picBitmapResized = Bitmap.createScaledBitmap(picBitmap, 80, 60, false);
                    byte[] picByteArrResized = BitmapUtil.getBitmapAsByteArray(picBitmapResized);
                    back.putExtra("Image", picByteArrResized);
                }

                back.putExtra("Latitude", currLatitude);
                back.putExtra("Longitude", currLongitude);
                setResult(RESULT_OK, back);
                finish();
            }


        }
    };

    OnClickListener cancelPhotoButtonClicked = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (takePhotoButton.getText().toString().trim().equals("Take")) {
                finish();
            } else {
                cancelPhotoButton.setText("Retake");
                mCameraPreview.refreshCamera(mCamera);
            }
        }
    };

    /*
        Find front facing camera and return its ID
     */
    private int findFrontFacingCamera() {
        int cameraId = -1;
        /* Search for the front facing camera */
        int numberOfCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                cameraId = i;
                break;
            }
        }
        return cameraId;
    }

    /*
        Find back facing camera and return its ID
     */
    private int findBackFacingCamera() {
        int cameraId = -1;
        /* Search for the front facing camera */
        int numberOfCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                cameraId = i;

                break;
            }
        }
        return cameraId;
    }

    private Camera.PictureCallback getPictureCallback() {
        Camera.PictureCallback picture = new Camera.PictureCallback() {
            @Override
            public void onPictureTaken(byte[] data, Camera camera) {
                Log.e("Picture taken", "On Picture Taken");
                picByteArr = data;
                File pictureFile = getOutputMediaFile();
                if (pictureFile == null) { return; }
                try {
                    FileOutputStream fos = new FileOutputStream(pictureFile);
                    fos.write(data);
                    fos.close();
                } catch(Exception e) {
                    Log.e("AddPhoto", "Error get picture callback");
                }


                /* Get place ID and store image to local database */
                picBitmap = BitmapUtil.getByteArrayAsBitmap(picByteArr);
                // TODO: store into local database
                currLatitude = roundDouble(currLatitude, 5);
                currLongitude = roundDouble(currLongitude, 5);

                LocalDatabaseConnector db = new LocalDatabaseConnector(AddPhoto.this);
                TravelogueApp appState = (TravelogueApp) getApplicationContext();
                db.insertPhoto(appState.getCurrentUser(), appState.getOngoingTrip(),
                        new LatLng(currLatitude, currLongitude), picBitmap);
            }
        };
        return picture;
    }

    private static File getOutputMediaFile() {
        File mediaStorageDir = new File("/sdcard/", "Travelogue");

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;

        mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg");
        return mediaFile;
    }

    /* Helper function for getting location */
    private void getLocation() {
        locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_COARSE);

        try {
            locationManager.requestLocationUpdates(locationManager.getBestProvider(criteria, true),
                    50, 1, new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    currLatitude = location.getLatitude();
                    currLongitude = location.getLongitude();
                    Log.e("Current Location", String.valueOf(currLatitude) + ", " + String.valueOf(currLongitude));
                }
                /* No changes needed */
                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {}

                @Override
                public void onProviderEnabled(String provider) {}

                @Override
                public void onProviderDisabled(String provider) {}
            });
        } catch(Exception e) {
            e.printStackTrace();
            Log.e("Get Location", "Cannot get location " + e.getMessage());
        }
    }

    private double roundDouble(double input, int digits) {
        if (digits < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(input);
        bd = bd.setScale(digits, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

}
