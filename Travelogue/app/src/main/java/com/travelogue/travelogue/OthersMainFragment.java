package com.travelogue.travelogue;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;

import java.util.ArrayList;

public class OthersMainFragment extends Fragment implements AbsListView.OnItemClickListener {
    private static final String ARG_PARAM1 = "Username";
    private static final String ARG_PARAM2 = "ArrayList of Trips";
    private AbsListView mListView;
    private ListAdapter mAdapter;
    private String username;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public OthersMainFragment() {
    }

    public static OthersMainFragment newInstance(String param1, ArrayList<String> param2) {
        OthersMainFragment fragment = new OthersMainFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putStringArrayList(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public String getUsernameParam() {
        return ARG_PARAM1;
    }

    public String getArrayListTripsParam() {
        return ARG_PARAM2;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ArrayList<String> mItems;
        if (getArguments() != null) {
            username = getArguments().getString(ARG_PARAM1);
            mItems = getArguments().getStringArrayList(ARG_PARAM2);
        } else {
            return;
        }

        mAdapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_list_item_1, android.R.id.text1, mItems);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_othersmain, container, false);

        // Set the adapter
        mListView = (AbsListView) view.findViewById(android.R.id.list);
        mListView.setAdapter(mAdapter);

        // Set OnItemClickListener so we can be notified on item clicks
        mListView.setOnItemClickListener(this);

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent myIntent = new Intent(getActivity(), ViewArchivedTrip.class);
        myIntent.putExtra("tripName", (String) mAdapter.getItem(position));
        myIntent.putExtra("username", username);
        getActivity().startActivity(myIntent);
    }
}
