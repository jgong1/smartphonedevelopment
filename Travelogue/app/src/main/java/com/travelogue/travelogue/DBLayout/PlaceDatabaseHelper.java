package com.travelogue.travelogue.DBLayout;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;

import com.google.android.gms.maps.model.LatLng;
import com.travelogue.travelogue.entities.Note;
import com.travelogue.travelogue.entities.Place;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Class for working with Comment objects in the database.
 */
public class PlaceDatabaseHelper implements DatabaseVariables {
    /* Private field */
    private SQLiteDatabase database; // open, writable database object

    /* Public constructor */
    public PlaceDatabaseHelper(SQLiteDatabase database) {
        this.database = database;
    }

    /* Create a new place item in the database. Return the ID */
    public long insertPlace(double latitude, double longitude, long tripID) {
        if (doesPlaceExist(latitude, longitude, tripID)) {
            return ERROR_ID;
        }

        ContentValues newPlace = new ContentValues();
        newPlace.put(COLUMN_NAME_LATITUDE, latitude);
        newPlace.put(COLUMN_NAME_LONGITUDE, longitude);
        newPlace.put(COLUMN_NAME_TRIP_ID, tripID);
        return database.insert(TABLE_NAME_PLACE, null, newPlace);
    }

    /* Returns the place ID of the trip with these coordinates */
    public long getPlaceID(double latitude, double longitude, long tripID) {
        // Use the trip ID to get the place
        Cursor c = database.query(TABLE_NAME_PLACE, null,
                COLUMN_NAME_TRIP_ID + "=" + Long.toString(tripID) + " AND " +
                COLUMN_NAME_LATITUDE + "=" + Double.toString(latitude) + " AND " +
                COLUMN_NAME_LONGITUDE + "=" + Double.toString(longitude),
                null, null, null, null);

        // If we get results back, return that the trip exists
        long id = ERROR_ID;
        if ((c != null) && (c.getCount() > 0)) {
            c.moveToFirst();
            id = c.getLong(c.getColumnIndexOrThrow(COLUMN_NAME_ID));
        }
        return id;
    }

    /* Return all of a trip's places from the database */
    public HashMap<LatLng, Place> getAllPlaces(long tripID) {
        // Use the trip ID to get all the comments
        Cursor c = database.query(TABLE_NAME_PLACE,
                null, COLUMN_NAME_TRIP_ID + "=" + Long.toString(tripID), null, null, null, null);

        // Create an array list to return and fill with comments
        HashMap<LatLng, Place> places = new HashMap<>();
        if ((c != null) && (c.getCount() > 0)) {
            c.moveToFirst();
            do {
                // Get the fields of interest
                long id = c.getLong(c.getColumnIndexOrThrow(COLUMN_NAME_ID));
                double latitude = c.getDouble(c.getColumnIndexOrThrow(COLUMN_NAME_LATITUDE));
                double longitude = c.getDouble(c.getColumnIndexOrThrow(COLUMN_NAME_LONGITUDE));

                // Get all the Bitmaps
                PhotoDatabaseHelper photo = new PhotoDatabaseHelper(database);
                ArrayList<Bitmap> p = photo.getAllPhotos(id);

                // Get all the Notes
                NoteDatabaseHelper note = new NoteDatabaseHelper(database);
                ArrayList<Note> n = note.getAllNotes(id);

                // Put these all in a Place object and the HashMap
                LatLng l = new LatLng(latitude, longitude);
                Place place = new Place(l);
                place.setAllNotes(n);
                place.setAllPhotos(p);
                places.put(l, place);
            } while (c.moveToNext());
            c.close();
        }
        return places;
    }

    /* Method for deleting a place from the database */
    public void deletePlace(double latitude, double longitude, long tripID) {
        database.delete(TABLE_NAME_PLACE,
                COLUMN_NAME_LATITUDE + "=" + Double.toString(latitude) + " AND " +
                COLUMN_NAME_LONGITUDE + "=" + Double.toString(longitude) + " AND " +
                COLUMN_NAME_TRIP_ID + "=" + Long.toString(tripID), null);
    }

    /* Returns whether this place exists for this trip */
    public boolean doesPlaceExist(double latitude, double longitude, long tripID) {
        // Use the trip ID to get the place
        Cursor c = database.query(TABLE_NAME_PLACE, null,
                COLUMN_NAME_TRIP_ID + "=" + Long.toString(tripID) + " AND " +
                COLUMN_NAME_LATITUDE + "=" + Double.toString(latitude) + " AND " +
                COLUMN_NAME_LONGITUDE + "=" + Double.toString(longitude),
                null, null, null, null);

        // If we get results back, return that the trip exists
        if ((c != null) && (c.getCount() > 0)) {
            return true;
        } else {
            return false;
        }
    }
}
