package com.travelogue.travelogue;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.travelogue.travelogue.DBLayout.LocalDatabaseConnector;

public class Home extends Activity {

    private Button loginButton;
    private Button registerButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        loginButton = (Button) findViewById(R.id.loginButton);
        loginButton.setOnClickListener(loginButtonClicked);

        registerButton = (Button) findViewById(R.id.registerButton);
        registerButton.setOnClickListener(registerButtonClicked);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    OnClickListener loginButtonClicked = new OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent loginUser = new Intent(Home.this, Login.class);
            startActivity(loginUser);
        }
    };

    OnClickListener registerButtonClicked = new OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent registerNewUser = new Intent(Home.this, Register.class);
            startActivity(registerNewUser);
        }
    };

}
