package com.travelogue.travelogue.entities;

import android.graphics.Bitmap;

import com.travelogue.travelogue.exceptions.EntityException;

import java.util.ArrayList;

public class User {
    private String username;
    private Bitmap profilePicture;
    private ArrayList<Trip> trips;
    private ArrayList<String> friends;

    /**
     * @param username The username of this user.
     */
    public User(String username) {
        this.username = username;
        this.trips = new ArrayList<Trip>();
        this.friends = new ArrayList<String>();
    }

    /**
     * @return User's username
     */
    public String getUsername() {
        return this.username;
    }

    /**
     * @param username New username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return This user's profile picture
     */
    public Bitmap getProfilePicture() {
        return this.profilePicture;
    }

    /**
     * @param pic The Bitmap to set the user's profile picture to
     */
    public void setProfilePicture(Bitmap pic) {
        this.profilePicture = pic;
    }

    /**
     * @param idx The index of the trip to retrieve
     * @return The Trip of interest. Exception if it wasn't found.
     */
    public Trip getTrip(int idx) throws EntityException {
        if ((idx >= 0) && (idx < this.trips.size())) {
            return this.trips.get(idx);
        } else {
            throw new EntityException("User, getTrip: Access to trips is out of bounds", 1);
        }
    }

    /**
     * @return List of all the user's trips
     */
    public ArrayList<Trip> getAllTrips() {
        return this.trips;
    }

    /**
     * Add a trip to the list
     * @param trip Trip object to add
     */
    public void setTrip(Trip trip) throws EntityException {
        for (Trip t : this.trips) {
            if (t.getName().equals(trip.getName())) {
                throw new EntityException("User, setTrip: Trip with this name already exists", 2);
            }
        }
        this.trips.add(trip);
    }

    /**
     * @param trips Replaces the trip list with this one.
     */
    public void setAllTrips(ArrayList<Trip> trips) {
        this.trips = trips;
    }

    /**
     * @param idx The index of the trip to remove
     */
    public void deleteTrip(int idx) {
        if ((idx >= 0) && (idx < this.trips.size())) {
            this.trips.remove(idx);
        }
        return;
    }

    /**
     * @return A List of all friends
     */
    public ArrayList<String> getAllFriends() {
        return this.friends;
    }

    /**
     * @param username User to add to the friend list
     */
    public void setFriend(String username) throws EntityException {
        for (String f : this.friends) {
            if (f.toLowerCase().equals(username.toLowerCase())) {
                throw new EntityException("User, setFriend: Already have this friend in the list", 2);
            }
        }
        this.friends.add(username.toLowerCase());
    }

    /**
     * @param friends Replaces the user's friend list with this one.
     */
    public void setAllFriends(ArrayList<String> friends) {
        this.friends = friends;
    }

    /**
     * @param username The username of the friend to remove from the list
     */
    public void deleteFriend(String username) {
        this.friends.remove(username.toLowerCase());
    }
}
