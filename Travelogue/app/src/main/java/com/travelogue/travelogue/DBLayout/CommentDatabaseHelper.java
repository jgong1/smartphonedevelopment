package com.travelogue.travelogue.DBLayout;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.travelogue.travelogue.entities.Note;

import java.util.ArrayList;

/**
 * Class for working with Comment objects in the database.
 */
public class CommentDatabaseHelper implements DatabaseVariables {
    /* Private field */
    private SQLiteDatabase database; // open, writable database object

    /* Public constructor */
    public CommentDatabaseHelper(SQLiteDatabase database) {
        this.database = database;
    }

    /* Create a new comment item in the database. Return the ID */
    public long insertComment(String text, String author, long tripID) {
        // Don't check for duplicates because adding a duplicate comment is ok.
        ContentValues newComment = new ContentValues();
        newComment.put(COLUMN_NAME_TEXT, text);
        newComment.put(COLUMN_NAME_AUTHOR, author);
        newComment.put(COLUMN_NAME_TRIP_ID, tripID);
        return database.insert(TABLE_NAME_COMMENT, null, newComment);
    }

    /* Return all of a trip's comments from the database */
    public ArrayList<Note> getAllComments(long tripID) {
        // Use the trip ID to get all the comments
        Cursor c = database.query(TABLE_NAME_COMMENT,
                null, COLUMN_NAME_TRIP_ID + "=" + Long.toString(tripID), null, null, null, null);

        // Create an array list to return and fill with comments
        ArrayList<Note> comments = new ArrayList<>();
        if ((c != null) && (c.getCount() > 0)) {
            c.moveToFirst();
            do {
                String content = c.getString(c.getColumnIndexOrThrow(COLUMN_NAME_TEXT));
                String author = c.getString(c.getColumnIndexOrThrow(COLUMN_NAME_AUTHOR));
                Note note = new Note(author, content);
                comments.add(note);
            } while (c.moveToNext());
            c.close();
        }
        return comments;
    }

    /* Method for deleting a comment from the database */
    public void deleteComment(long tripID) {
        // TODO Comments are forever
    }
}
